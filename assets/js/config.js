const url = "http://" + window.location.hostname + "/netrix.com/";
const url_api = "http://" + window.location.hostname + "/netrix-api.com/";
const uuid = "website";
const path = "/netrix.com/page/";
const direc = "/netrix-api.com";
const usr = localStorage.getItem("auth_name");
const pwd = localStorage.getItem("auth_password");
const lang = localStorage.getItem("bahasa");

const insurance_username = localStorage.getItem("auth_name");
const insurance_secret_key = localStorage.getItem("key_secret");
var signature = "";
var hash = "";

$(document).bind("ajaxSend", function(){
   $(".preloader").show();
 }).bind("ajaxComplete", function(){
   $(".preloader").hide();
 });

function NOW() {

    var date = new Date();
    var aaaa = date.getFullYear();
    var gg = date.getDate();
    var mm = (date.getMonth() + 1);

    if (gg < 10)
        gg = "0" + gg;

    if (mm < 10)
        mm = "0" + mm;

    var cur_day = aaaa + "-" + mm + "-" + gg;

    var hours = date.getHours()
    var minutes = date.getMinutes()
    var seconds = date.getSeconds();

    if (hours < 10)
        hours = "0" + hours;

    if (minutes < 10)
        minutes = "0" + minutes;

    if (seconds < 10)
        seconds = "0" + seconds;

    return cur_day + " " + hours + ":" + minutes + ":" + seconds;

}

function NOWTIME() {

    var date = new Date();
    var aaaa = date.getFullYear();
    var gg = date.getDate();
    var mm = (date.getMonth() + 1);

    if (gg < 10)
        gg = "0" + gg;

    if (mm < 10)
        mm = "0" + mm;

    var cur_day = aaaa + "" + mm + "" + gg;

    var hours = date.getHours()
    var minutes = date.getMinutes()
    var seconds = date.getSeconds();

    if (hours < 10)
        hours = "0" + hours;

    if (minutes < 10)
        minutes = "0" + minutes;

    if (seconds < 10)
        seconds = "0" + seconds;

    return cur_day + "" + hours + "" + minutes + "" + seconds;

}

const datetime = NOW();

if(insurance_secret_key != null){
  signature = CryptoJS.HmacSHA256(insurance_secret_key + insurance_username + datetime, insurance_secret_key);
  hash = CryptoJS.enc.Base64.stringify(signature);
}

function get_jsf_hash() {
    var returns = '';
    var urlHash = window.location.hash;
    var split = urlHash.split('?')
    if (typeof split[0] !== "undefined") {
        returns = split[0].replace("#", "");
    }
    return returns;
}

function ucwords (str) {

  return (str + '')
    .replace(/^(.)|\s+(.)/g, function ($1) {
      return $1.toUpperCase()
    })
}

function paging(val){
    var name_function = "table_get_" + val + "(data)";
    var this_page = "this_page_" + val;
    var total_page = "total_page_" + val;


    $(".change-num-records-" + val).change(function(e) {
      $(".this-page-get-" + val).val(1);
      var data = $('.' + val + '_form_filter').serialize();
      data += "&" + $('.paging_form_' + val).serialize();
      eval(name_function);
    });

    $(".link-first-page-" + val).click(function(e) {
      $(".this-page-get-" + val).val(1);
      var data = $('.' + val + '_form_filter').serialize();
      data += "&" + $('.paging_form_' + val).serialize();
      eval(name_function);
      this_page = 1;
    });

    $(".link-prev-page-" + val).click(function(e) {
      $(".this-page-get-" + val).val(eval(this_page) - 1);
      var data = $('.' + val + '_form_filter').serialize();
      data += "&" + $('.paging_form_' + val).serialize();
      eval(name_function);
	  this_page = parseInt(eval(this_page)) - 1;
    });

    $(".go-page-" + val).click(function(e) {
      $(".this-page-get-" + val).val();
      var data = $('.' + val + '_form_filter').serialize();
      data += "&" + $('.paging_form_' + val).serialize();
      eval(name_function);
    });

    $(".link-next-page-" + val).click(function(e) {
      $(".this-page-get-" + val).val(eval(this_page) + 1);
      var data = $('.' + val + '_form_filter').serialize();
      data += "&" + $('.paging_form_' + val).serialize();
      eval(name_function);
	  this_page = parseInt(eval(this_page)) + 1;
    });

    $(".link-last-page-" + val).click(function(e) {
      $(".this-page-get-" + val).val(eval(total_page));
      var data = $('.' + val + '_form_filter').serialize();
      data += "&" + $('.paging_form_' + val).serialize();
      eval(name_function);
      this_page = eval(total_page);
    });

    $(".btn-submit-filter-" + val).click(function(e) {
      var data = $('.' + val + '_form_filter').serialize();
      eval(name_function);
      $('#' + val + 'modalfilter').modal('toggle');
    });

    $(".btn-reset-filter-" + val).click(function(e) {
      $('.' + val + '_form_filter').trigger("reset");
      eval(name_function);
      $('#' + val + 'modalfilter').modal('toggle');
    });
}

function paging_ajax(val, res){
    var this_page = "this_page_" + val;
    var total_page = "total_page_" + val;

    var lang = localStorage.getItem("bahasa");
    if(lang == 1){
        var sub_total = "Total Data : ";
        var sub_page = " Page";
    }else{
        var sub_total = "Jumlah Data : ";
        var sub_page = " Halaman";
    }

    $('.total-data-get-' + val).text(sub_total + res.body.pagination.total_data);
    $('.total-page-get-' + val).text(" / " + res.body.pagination.total_page + sub_page);
    $('.this-page-get-' + val).val(res.body.pagination.this_page);
    $('.ikut-table-' + val).show();
    // $(".ikut-table-bulk").css("width", $("#table-bulk").width()).show();

    this_page = res.body.pagination.this_page;
    total_page = res.body.pagination.total_page;

    if(res.body.pagination.this_page == 1){
      $('.first-page-' + val).hide();
      $('.prev-page-' + val).hide();
    }else{
      $('.first-page-' + val).show();
      $('.prev-page-' + val).show();
    }

    if(res.body.pagination.this_page == res.body.pagination.total_page){
      $('.next-page-' + val).hide();
      $('.last-page-' + val).hide();
    }else{
      $('.next-page-' + val).show();
      $('.last-page-' + val).show();
    }
}

function refresh_data(page, form_filter){
    var name_function = "table_get_" + page + "()";
    eval(name_function);
    if(form_filter){
        $('.' + form_filter).trigger("reset");
    }
}
