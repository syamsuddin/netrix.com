var menu = new Array();
menu['commission'] = 'menu-two';
menu['tax'] = 'menu-two';

menu['bonuscalc'] = 'menu-three';
menu['bonusdist'] = 'menu-three';
menu['bonuspay'] = 'menu-three';
menu['bonustax'] = 'menu-three';
menu['bonushistory'] = 'menu-three';
menu['bonussum'] = 'menu-three';

menu['user'] = 'menu-four';
menu['role'] = 'menu-four';
menu['controllers'] = 'menu-four';

menu['insurance'] = 'menu-five';
menu['acm'] = 'menu-five';

menu['encrypt'] = 'menu-six';

$(document).ready(function() {

    // NEW BANGET //
    var topTabDetailDelete = JSON.parse(localStorage.getItem("topTabDetail"));
    var topTab = JSON.parse(localStorage.getItem("topTab"));
    if (topTabDetailDelete != null) {
        $.each(topTabDetailDelete, function(key, value) {
            delete topTab[key]
        })
        var ii = 1;
        $.each(topTab, function(key, value) {
            if (ii == 1) {
                topTab[key] = true;
            } else {
                topTab[key] = false;
            }
            ii++;
        })
        localStorage.setItem("topTab", JSON.stringify(topTab));
        localStorage.removeItem("topTabDetail");
    }
    // END NEW BANGET //


    $('.tab-render').css('width', ($('.page-content-render').width() - ($('.custom-tab-list').width() + 50)))

    renderTopTab(true);
    logout();
    load_side_menu();

    $(".prev-tab").click(function(e) {
        var topTab = localStorage.getItem("topTab");
        topTab = JSON.parse(topTab);

        var tab;
        $.each(topTab, function(key, value) {
            if (value) {
                return false
            } else {
                tab = key;
            }
        });
        $("[href='" + tab + "']").trigger("click");
    });

    $(".next-tab").click(function(e) {
        var topTab = localStorage.getItem("topTab");
        topTab = JSON.parse(topTab);

        var tab;
        var num = 0;
        $.each(topTab, function(key, value) {
            if (num == 1) {
                tab = key;
                return false
            }
            if (value) {
                num = 1;
            }
        });
        $("[href='" + tab + "']").trigger("click");
    });

});

function translate(page){
    var sub = new Array();
    sub["commission"] = "Komisi";
    sub["tax"] = "Pajak";
    sub["bonusdist"] = "Perhitungan Bonus";
    sub["bonushistory"] = "Riwayat Bonus";
    sub["user"] = "Pengguna";
    sub["insurance"] = "Inisiator Manajemen Kunci";

    if(lang == 2){
        $('.sub-' + page).text(sub[page]);
        $('.sub-create-new').text("Buat Baru");
        $('.sub-generate').text("Hitung");
        $('.sub-delete').text("Hapus");

        $('.sub-upload-csv').text("Unggah Berkas CSV");

        $('.sub-label-ins').text("Asuransi");
        $('.sub-label-file-upload').text("Unggah Berkas");
        $('.sub-label-file-period').text("Periode Berkas");
        $('.sub-label-period').text("Periode");

        $('.sub-filter').text("Saring");
        $('.sub-refresh').text("Muat Ulang");
        $('.sub-save').text("Simpan");
        $('.sub-reset').text("Atur Ulang");
        $('.sub-submit').text("Pilih");

        $('.sub-records').text("Data");
        $('.sub-go').text("Pergi");
    }
}

function renderTopTab(firstload, jsf, id_attr) {

    // $(".has-arrow").removeClass("active");
    // $(".first-level").removeClass("in");

    var topTab = localStorage.getItem("topTab");
    var topTabDetail = localStorage.getItem("topTabDetail");

    if (topTab != null) {
        topTab = JSON.parse(topTab);
    }

    if (topTabDetail != null) {
        topTabDetail = JSON.parse(topTabDetail);
    } else {
        topTabDetail = {};
    }

    $(".tab-render").html("");
    if (topTab) {
        $(".menu-to-del").removeClass("active");
        $.each(topTab, function(key, value) {
            var links = key;
            key = key.replace(".html", "");
            minkey = key;
            key = ucwords(key);
            var active = "";
            var dnone = 'style="display:none"';
            var title = $(".title-" + minkey).text();
            var id_data = "";

            if (links == jsf && typeof id_attr !== "undefined") {
                title = $(".title-" + minkey + "[attr-id=" + id_attr + "]").text();
                topTabDetail[jsf] = id_attr;
                localStorage.setItem("topTabDetail", JSON.stringify(topTabDetail));
                id_data = id_attr;
            }

            if (typeof topTabDetail[links] !== "undefined") {
                title = $(".title-" + minkey + "[attr-id=" + topTabDetail[links] + "]").text();
                id_data = topTabDetail[links];
            }

            if (value) {
                active = "active-tab";
                dnone = "";
                $("." + menu[minkey]).addClass("active");
                $("." + menu[minkey] + "-tree").addClass("in");
                $(".menu-" + minkey).addClass("active");
            }

            $(".tab-render").append(
                `<li class="nav-item tab-atas home-tab-atas tab-${minkey} ${active}" style="white-space: nowrap; height: 50px;">
                <a class="nav-link trigger-top-menu" href="${links}" href-key="${key}" style="height: 50px">
                  <span class="d-none d-md-block" style="height: 50px">${title} <i class="mdi mdi-close cltb fas fa-times" attr-link="${links}" attr-key="${key}"></i></span>
                </a>
            </li>`
            );

            // Append New Div Page
            if (links != "" && firstload) {
                $(".page-content-render").html("");

                var display = "";
                if (!value) {
                    display = "display: none"
                }

                $.get(path + links, function(data) {
                    $(".page-content-render").append(`<div style="min-height: calc(110vh - 180px);background-color: #ffffff;${display}" class="content-${key} content-displayn" ${dnone}>
                                                  ${data}
                                              </div>`);
                    window[key.toLowerCase() + "_ready"]();
                }).done(function() {
                    translate(links.replace(".html", ""));
                })
            }
            translate(minkey);
        });
    }
    triggerTopMenu();
    closeTab();
}

function triggerTopMenu() {

    $("a.trigger-top-menu").unbind("click");
    $("a.trigger-top-menu").click(function(e) {
        e.preventDefault();

        $(".menu-first-to-del").removeClass("active");
        $(".first-level").removeClass("in");
        $(".menu-to-del").removeClass("active");

        var key = $(this).attr("href-key");
        var jsf = $(this).attr("href");
        if (key != "") {
            var topTab = localStorage.getItem("topTab");
            if (topTab == null) {
                topTab = {};
            } else {
                topTab = JSON.parse(topTab);
            }
            $.each(topTab, function(key, value) {
                topTab[key] = false;
            });
            topTab[jsf] = true;
            localStorage.setItem("topTab", JSON.stringify(topTab));
            renderTopTab();
            $(".content-displayn").hide();
            $(".content-" + key).show();
            $(".menu-to-del").removeClass("active");
            $(".menu-" + key.toLowerCase()).addClass("active");
            // console.log(key);
        }
        return false;
    });
}

function closeTab() {
    $(".cltb").unbind("click");
    $(".cltb").click(function(e) {
        e.preventDefault();

        $(".home-tab-atas").removeClass("active-tab");
        $(".content-displayn").hide();

        var links = $(this).attr("attr-link");
        var key = $(this).attr("attr-key");
        var minkey = key.toLowerCase();
        var topTab = localStorage.getItem("topTab");
        topTab = JSON.parse(topTab);
        $(".content-" + key).remove();
        $(".tab-" + minkey).remove();
        delete topTab[links];

        var keys = Object.keys(topTab);
        if (keys.length > -1) {
            var name_link = keys[keys.length - 1];
            name_link = ucwords(name_link);
            name_link = name_link.replace(".html", "");

            $(".tab-" + name_link.toLowerCase()).addClass("active-tab");
            $(".content-" + name_link).show();

            $(".menu-first-to-del").removeClass("active");
            $(".first-level").removeClass("in");
            $(".menu-to-del").removeClass("active");

            $("." + menu[name_link.toLowerCase()]).addClass("active");
            $("." + menu[name_link.toLowerCase()] + "-tree").addClass("in");
            $(".menu-" + name_link.toLowerCase()).addClass("active");
        }

        localStorage.setItem("topTab", JSON.stringify(topTab));
    });
}

function load_side_menu() {
    $("a.trigger-side-menu").unbind('click');
    $("a.trigger-side-menu").click(function(e) {
        $("a.trigger-side-menu").removeClass("active-side");
        var atsm = $(this);
        e.preventDefault();
        var jsf = $(this).attr("href");
        var id_attr = $(this).attr("side-attr-id");
        var menu_active = $(this).attr("side-menu-active");
        var key = jsf.replace(".html", "");

        if(typeof menu_active == "undefined"){
            $(this).addClass("active-side");
        }else{
            $("a.trigger-side-menu[href='"+menu_active+"']").addClass("active-side");
        }

        $(".page-content-render").html("");

        $.get(path + jsf, function(data) {
            $(".page-content-render").append(`<div style="min-height: calc(110vh - 180px);background-color: #ffffff;" class="content-${key} content-displayn">
                                          ${data}
                                      </div>`);
        }).done(function() {
            if(typeof id_attr == "undefined"){
                window[key.toLowerCase() + "_ready"]();
            }else{
                window[key.toLowerCase() + "_ready"](id_attr);
            }

            translate(jsf.replace(".html", ""));
        })

        // if (jsf != "") {
        //     var loadnew = false;
        //     var topTab = localStorage.getItem("topTab");
        //     if (topTab == null) {
        //         topTab = {};
        //     } else {
        //         topTab = JSON.parse(topTab);
        //     }
        //     if (typeof topTab[jsf] === "undefined") {
        //         $(".content-displayn").hide();
        //         var key = jsf.replace(".html", "");
        //         minkey = key;
        //         key = ucwords(key);
        //         $.get(path + jsf, function(data) {
        //             $(".page-content-render").append(`
        //                 <div style="min-height: calc(110vh - 180px);background-color: #ffffff;" class="content-${key} content-displayn">
        //                     ${data}
        //                 </div>`);
        //
        //             var side_attr_id = '';
        //             var side_attr_type = '';
        //             if (typeof atsm.attr('side-attr-id') !== "undefined") {
        //                 side_attr_id = atsm.attr('side-attr-id');
        //             }
        //             if (typeof atsm.attr('side-attr-type') !== "undefined") {
        //                 side_attr_type = atsm.attr('side-attr-type');
        //             }
        //             window[minkey + "_ready"](side_attr_id, side_attr_type);
        //         }).done(function() {
        //             translate(jsf.replace(".html", ""));
        //         })
        //     } else {
        //         var side_attr_id = '';
        //         var side_attr_type = '';
        //         if (typeof atsm.attr('side-attr-id') !== "undefined") {
        //             side_attr_id = atsm.attr('side-attr-id');
        //         }
        //         if (typeof atsm.attr('side-attr-type') !== "undefined") {
        //             side_attr_type = atsm.attr('side-attr-type');
        //         }
        //         window[minkey + "_ready"](side_attr_id, side_attr_type);
        //
        //         var key = jsf.replace(".html", "");
        //         key = ucwords(key);
        //         $(".content-displayn").hide();
        //         $(".content-" + key).show();
        //     }
        //     $.each(topTab, function(key, value) {
        //         topTab[key] = false;
        //     });
        //     topTab[jsf] = true;
        //     localStorage.setItem("topTab", JSON.stringify(topTab));
        //     renderTopTab(false, jsf, id_attr);
        // }
        return false;
    });
}

function logout() {
    $(".btn-logout").unbind("click");
    $(".btn-logout").click(function(e) {
        e.preventDefault();
        $.ajax({
            url: url_api + 'auth/session',
            type: 'DELETE',
            dataType: 'json',
            beforeSend: function(xhr) {
                xhr.setRequestHeader("Authorization", "Basic " + btoa(usr + ":" + pwd));
                xhr.setRequestHeader("commandid", "Log Out");
                xhr.setRequestHeader("requestid", "logout" + Date.now());
                xhr.setRequestHeader("requestdt", datetime);
                xhr.setRequestHeader("clientid", "insurance");
                xhr.setRequestHeader("signature", signature);
                xhr.setRequestHeader("token", localStorage.getItem('token'));
                xhr.setRequestHeader("uuid", uuid);
            },
            success: function(data, textStatus, xhr) {
                localStorage.clear();
                window.location.href = url;
            },
            error: function(xhr, textStatus, errorThrown) {
                $('.dbody').show();
            }
        });
    });
}
