<?php
class Page extends CI_Controller
{
	public function encrypt()
	{
			$this->load->view('page\encrypt');
	}

	public function beforedetailfile()
	{
			$this->load->view('page\beforedetailfile');
	}

	public function detailfiletax()
	{
			$this->load->view('page\detailfiletax');
	}

	public function detailfile()
	{
			$this->load->view('page\detailfile');
	}

	public function insurance()
	{
			$this->load->view('page\insurance');
	}

	public function detailcommission()
	{
			$this->load->view('page\detailcommission');
	}

	public function commission()
	{
			$this->load->view('page\commission');
	}

	public function acm()
	{
			$this->load->view('page\acm');
	}

	public function detailhistory()
	{
			$this->load->view('page\detailhistory');
	}

	public function detailtax()
	{
			$this->load->view('page\detailtax');
	}

	public function detailsummary()
	{
			$this->load->view('page\detailsummary');
	}

	public function detailcalc()
	{
			$this->load->view('page\detailcalc');
	}

	public function tax()
    {
        $this->load->view('page\tax');
    }

    public function bulk()
    {
        $this->load->view("page\bulk");
    }

    public function bonuscalc()
    {
        $this->load->view("page\bonuscalc");
    }

	public function bonusdist()
    {
        $this->load->view("page\bonusdist");
    }

	public function bonustax()
    {
        $this->load->view("page\bonustax");
    }

	public function detailpayment()
    {
        $this->load->view("page\detailpayment");
    }

	public function bonuspay()
    {
        $this->load->view("page\bonuspay");
    }

	public function bonushistory()
    {
        $this->load->view("page\bonushistory");
    }

    public function bonussum()
    {
        $this->load->view("page\bonussum");
    }

    public function authentification()
    {
        $this->load->view("page\authentification");
    }
    public function controllers()
    {
        $this->load->view("page\controllers");
    }

    public function password()
    {
        $this->load->view("page\password");
    }
    public function profile()
    {
        $this->load->view("page\profile");
    }

    public function role()
    {
        $this->load->view("page\\role");
    }
    public function sample()
    {
        $this->load->view("page\sample");
    }

    public function user()
    {
        $this->load->view("page\user");
    }

	public function userdetail()
    {
        $this->load->view("page\userdetail");
    }
}
