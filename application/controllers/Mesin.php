<?php
class Mesin extends CI_Controller
{
    public function cek()
    {
        getcwd();
        chdir("../");
        chdir("netrix-api.com/uploads/temporary/");
        $dir = getcwd();
        chdir("../");
        chdir("success/");
        $success = getcwd();
        chdir("../");
        chdir("failed/");
        $failed = getcwd();

        $arr_column = array(
            "Policy Number", //0
            "Policy Header Number", //1
            "Product Code", //2
            "Due Date", //3
            "Frequency", //4
            "Premium", //5
            "Bonus Personal", //6
            "Percentage", //7
            "Currency", //8
            "K-Link Member ID", //9
            "Agent Code", //10
            "Agent Name", //11
            "Commision Period", //12
            "Issued Date", //13
            "Remarks", //14
            "Assured Name", //15
            "Premium Peroid (Year)", //16
            "Premium Peroid (Month)", //17
        );

        foreach (glob($dir.'/*.*') as $file) {
            if (($handle = fopen($file, "r")) !== false) {
                $row = 1;
                while (($data = fgetcsv($handle, 1000, ";")) !== false) {
                    $error = 0;
                    $msg_error = array();
                    foreach ($data as $k=>$v) {
                        if (empty($v)) {
                            $col = $arr_column[$k];
                            array_push($msg_error, "Row " . $row . " Column '" . $col  . "' is empty");
                            $error = 1;
                        }
                    }
                    $row++;
                }
            }
            fclose($handle);

            $file_name = basename($file);

            if ($error == 0) {
                rename($file, $success . "\\" . $file_name);
                echo "oke";
            } else {
                rename($file, $failed . "\\" . $file_name);
                foreach ($msg_error as $v) {
                    echo $v;
                }
            }
        }
    }
}
