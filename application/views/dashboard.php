<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <meta name="description" content="" />
  <meta name="author" content="" />
  <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url(); ?>assets/images/K-TRACS-04-02.png" />
  <title>K-TRACS</title>
  <link href="<?php echo base_url(); ?>assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.css" rel="stylesheet" />
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/libs/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css" />
  <link href="<?php echo base_url(); ?>assets/dist/css/style.min.css" rel="stylesheet" />
  <link href="<?php echo base_url(); ?>assets/libs/toastr/build/toastr.min.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets/libs/daterangepicker/daterangepicker.css" rel="stylesheet">
  <style>
    #myTab {
      display: none;
    }

    .active-tab {
      background: rgba(0, 0, 0, 0.05);
    }
    ::-webkit-scrollbar {
        width: 1px;
    }

    ::-webkit-scrollbar-track {
      background: #f1f1f1;
    }

    ::-webkit-scrollbar-thumb {
      background: #888;
    }

    ::-webkit-scrollbar-thumb:hover {
      background: #555;
    }
    .circular--landscape {
      display: inline-block;
      position: relative;
      width: 60px;
      height: 60px;
      overflow: hidden;
      border-radius: 50%;
    }

    .circular--landscape img {
      width: auto;
      height: 100%;
      margin-left: -25px;
    }

    .active-side {
      background: #00dc12ab;
    }
  </style>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/3.1.2/rollups/hmac-sha256.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/3.1.2/components/enc-base64-min.js"></script>
  <script src="<?php echo base_url(); ?>assets/libs/jquery/dist/jquery.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/js/config.js"></script>
  <script src="<?php echo base_url(); ?>assets/js/bahasa.js"></script>
  <script>
    $.ajax({
	  url: url_api + 'auth/session',
      type: 'GET',
      dataType: 'json',
      beforeSend: function(xhr) {
          xhr.setRequestHeader("Authorization", "Basic " + btoa(usr + ":" + pwd));
          xhr.setRequestHeader("commandid", "Cek Login");
          xhr.setRequestHeader("requestid", "ceklogin" + Date.now());
          xhr.setRequestHeader("requestdt", datetime);
          xhr.setRequestHeader("clientid", insurance_username);
          xhr.setRequestHeader("signature", signature);
          xhr.setRequestHeader("token", localStorage.getItem('token'));
          xhr.setRequestHeader("uuid", uuid);
      },
      success: function(data, textStatus, xhr) {
    		if(typeof data.body.data.token==='undefined'){
    			  window.location.href=url;
    		}
            data.body.data.picture = url_api + data.body.data.picture;
            $('.sess-username').text(data.body.data.username);
            $('.sess-fullname').text(data.body.data.fullname);
            $('.dashboard-picture').prepend(`<img src="${data.body.data.picture}" alt="user" class="rounded-circle" width="31" />`);
            $('.popup-picture').prepend(`<img src="${data.body.data.picture}" alt="user" class="rounded-circle" width="60" />`);

            var access = localStorage.getItem('access');
            access = JSON.parse(access);
            access.map(function(value) {
                value = value.toLowerCase();
                value = value.replace(/ /g, "-")
                $('.menu-access-' + value).show();
            })
      },
      error: function(xhr, textStatus, errorThrown) {
        window.location.href = url
      }
    });
  </script>
</head>

<body>
  <div class="preloader" style="opacity: 0.6">
    <div class="lds-ripple">
      <div class="lds-pos"></div>
      <div class="lds-pos"></div>
    </div>
  </div>
  <div id="main-wrapper">
    <header class="topbar">
        <nav class="navbar top-navbar navbar-expand-md navbar-dark" style="background-color:#007C40;  background-image: linear-gradient(to right, #007C40 , #158004);">
            <div class="navbar-header" style="background-color:#007C40;">
                <a class="nav-toggler waves-effect waves-light d-block d-md-none" href="javascript:void(0)"><i class="ti-menu ti-close"></i></a>
                <a class="navbar-brand sidebartoggler" href="javascript:void(1);">
            <img src="<?php echo base_url(); ?>assets/images/logo-icon.png" alt="homepage" class="dark-logo" />
            <img src="<?php echo base_url(); ?>assets/images/K-TRACS-04-02.png" alt="homepage" class="light-logo" style="max-height: 45px;" />
            <span class="logo-text">
              <!-- <img src="<?php echo base_url(); ?>assets/images/logo-text.png" alt="homepage" class="dark-logo" /> -->
              <img style="margin-left: 15px;" src="<?php echo base_url(); ?>assets/images/logo-light-text.png" class="light-logo" alt="homepage" />
            </span>
          </a>
            <a class="topbartoggler d-block d-md-none waves-effect waves-light" href="javascript:void(0)" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><i
              class="ti-more"></i></a>
            </div>
            <div class="navbar-collapse collapse float-right" id="navbarSupportedContent">
                <!-- <ul class="navbar-nav float-left">
                    <li class="nav-item d-none d-md-block prev-tab">
                        <a class="nav-link waves-effect waves-light" data-sidebartype="mini-sidebar"><i class="fa fa-angle-left"></i></a>
                    </li>
                </ul>
                <ul class="navbar-nav float-left mr-auto tab-render" style="width: 830px;overflow-x: scroll;scrollbar-width:thin">

                </ul> -->
                <div class="col-md-6">
                </div>
                <div class="col-md-6">
                    <ul class="navbar-nav float-right custom-tab-list">
                        <!-- <li class="nav-item d-none d-md-block next-tab">
                            <a class="nav-link waves-effect waves-light" data-sidebartype="mini-sidebar"><i class="fa fa-angle-right"></i></a>
                        </li> -->
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle flag-active" href="#" id="navbarDropdown2" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="flag-icon flag-icon-us"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right animated bounceInDown" aria-labelledby="navbarDropdown2">
                                <a onclick="change_bahasa('eng')" class="dropdown-item" href="javascript:void(0)"><i class="flag-icon flag-icon-us"></i> English</a>
                                <a onclick="change_bahasa('ind')" class="dropdown-item" href="javascript:void(0)"><i class="flag-icon flag-icon-id"></i> Indonesia</a>
                            </div>
                        </li>
                        <li class="nav-item dropdown" style="padding-top: 5px;">
                            <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark pro-pic dashboard-picture circular--landscape" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"
                            style="height: 40px;width: 40px;line-height: 0px;"
                            ></a>
                            <div class="dropdown-menu dropdown-menu-right user-dd animated flipInY">
                                <span class="with-arrow"><span class="bg-primary"></span></span>
                                <div class="d-flex no-block align-items-center p-15 bg-primary text-white m-b-10">
                                    <div class="popup-picture circular--landscape">
                                    </div>
                                    <div class="m-l-10">
                                        <h4 class="m-b-0 sess-username"></h4>
                                        <p class=" m-b-0 sess-fullname"></p>
                                    </div>
                                </div>
                                <a href="profile.html" class="dropdown-item sidebar-link trigger-side-menu menu-to-del menu-profile"><i class="ti-user m-r-5 m-l-5"></i><span class="hide-menu title-profile sm-myprofile"> My Profile</span></a>
                                <a href="password.html" class="dropdown-item sidebar-link trigger-side-menu menu-to-del menu-password"><i class="ti-key m-r-5 m-l-5"><span class="hide-menu title-password sm-password"> Change Password</span></i></a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item btn-logout sm-logout" href="javascript:void(0)"><i class="fa fa-power-off m-r-5 m-l-5"></i> Logout</a>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </header>
    <aside class="left-sidebar">
        <div class="scroll-sidebar">
            <nav class="sidebar-nav">
                <ul id="sidebarnav">

                    <li class="sidebar-item">
                        <a href="dashboard.html" class="sidebar-link trigger-side-menu menu-to-del menu-dashboard"><i class="mdi mdi-view-dashboard"></i><span class="hide-menu title-dashboard title-sb-dashboard"> Dashboard </span></a>
                    </li>

                    <li class="sidebar-item">
                        <a class="sidebar-link has-arrow waves-effect waves-dark menu-two menu-first-to-del" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-ungroup"></i><span class="hide-menu title-sb-group-filemanagement">File Management</span></a>
                        <ul aria-expanded="false" class="collapse first-level menu-two-tree">
                            <li class="sidebar-item">
                                <a href="commission.html" class="sidebar-link trigger-side-menu menu-to-del menu-commission menu-access-commission" style="display: none;"><i class="mdi mdi-file-document"></i><span class="hide-menu title-commission title-sb-commission"> Commission</span></a>
                            </li>
                            <li class="sidebar-item">
                                <a href="tax.html" class="sidebar-link trigger-side-menu menu-to-del menu-tax menu-access-tax" style="display: none;"><i class="mdi mdi-file-document"></i><span class="hide-menu title-tax title-sb-tax"> Tax</span></a>
                            </li>
                        </ul>
                    </li>

                    <li class="sidebar-item">
                        <a class="sidebar-link has-arrow waves-effect waves-dark menu-three menu-first-to-del" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-ungroup"></i><span class="hide-menu title-sb-group-bonusdistribution">Bonus Distribution</span></a>
                        <ul aria-expanded="false" class="collapse first-level menu-three-tree">
                            <li class="sidebar-item">
                                <a href="bonusdist.html" class="sidebar-link trigger-side-menu menu-to-del menu-bonusdist menu-access-bonus-calculation" style="display: none;"><i class="mdi mdi-calculator"></i><span class="hide-menu title-bonusdist title-sb-bonusdist"> Bonus Calculation</span></a>
                            </li>
                            <li class="sidebar-item">
                                <a href="bonuspay.html" class="sidebar-link trigger-side-menu menu-to-del menu-bonuspay menu-access-bonus-payment" style="display: none;"><i class="mdi mdi-calculator"></i><span class="hide-menu title-bonuspay title-sb-bonuspay"> Bonus Payment</span></a>
                            </li>
                            <li class="sidebar-item">
                                <a href="bonushistory.html" class="sidebar-link trigger-side-menu menu-to-del menu-bonushistory menu-access-bonus-history" style="display: none;"><i class="mdi mdi-calculator"></i><span class="hide-menu title-bonushistory  title-sb-bonus-history"> Bonus History</span></a>
                            </li>
                        </ul>
                    </li>

                    <li class="sidebar-item">
                        <a class="sidebar-link has-arrow waves-effect waves-dark menu-four menu-first-to-del" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-account-circle"></i><span class="hide-menu title-sb-group-usermanagement">User Management</span></a>
                        <ul aria-expanded="false" class="collapse first-level menu-four-tree">
                            <li class="sidebar-item">
                                <a href="user.html" class="sidebar-link trigger-side-menu menu-to-del menu-user menu-access-user" style="display: none;"><i class="mdi mdi-account-card-details"></i><span class="hide-menu title-user title-sb-um-user"> User </span></a>
                            </li>
                            <li class="sidebar-item">
                                <a href="role.html" class="sidebar-link trigger-side-menu menu-to-del menu-role menu-access-role" style="display: none;"><i class="mdi mdi-account-key"></i><span class="hide-menu title-role title-sb-um-role"> Role</span></a>
                            </li>
                            <li class="sidebar-item">
                                <a href="controllers.html" class="sidebar-link trigger-side-menu menu-to-del menu-controllers menu-access-controllers" style="display: none;"><i class="mdi mdi-flag"></i><span class="hide-menu title-controllers title-sb-um-controllers"> Controllers</span></a>
                            </li>
                        </ul>
                    </li>

                    <li class="sidebar-item">
                        <a class="sidebar-link has-arrow waves-effect waves-dark menu-five menu-first-to-del" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-account-check"></i><span class="hide-menu title-sb-group-apicallmanagement">API Caller Management</span></a>
                        <ul aria-expanded="false" class="collapse first-level menu-five-tree">
                            <li class="sidebar-item">
                                <a href="insurance.html" class="sidebar-link trigger-side-menu menu-to-del menu-insurance menu-access-initiator-key-management" style="display: none;"><i class="mdi mdi-account-multiple"></i><span class="hide-menu title-insurance title-sb-acm-ikm"> Initiator Key Management</span></a>
                            </li>
                            <li class="sidebar-item">
                                <a href="acm.html" class="sidebar-link trigger-side-menu menu-to-del menu-acm menu-access-reconciliaion-history" style="display: none;"><i class="mdi mdi-file"></i><span class="hide-menu title-acm title-sb-acm-rh"> Reconciliation History</span></a>
                            </li>
                        </ul>
                    </li>

                    <li class="sidebar-item">
                        <a class="sidebar-link has-arrow waves-effect waves-dark menu-six menu-first-to-del" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-file"></i><span class="hide-menu title-sb-tools">Tools</span></a>
                        <ul aria-expanded="false" class="collapse first-level menu-six-tree">
                            <li class="sidebar-item">
                                <a href="encrypt.html" class="sidebar-link trigger-side-menu menu-to-del menu-encrypt menu-access-encrypt" style="display: none;"><i class="mdi mdi-file"></i><span class="hide-menu title-encrypt title-sb-tools-encrypt"> Encrypt & Descrypt</span></a>
                            </li>
                        </ul>
                    </li>

                    <!-- <li class="sidebar-item">
                        <a class="sidebar-link has-arrow waves-effect waves-dark menu-six menu-first-to-del" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-chart-bar"></i><span class="hide-menu title-sb-group-report">Report</span></a>

                        <ul aria-expanded="false" class="collapse first-level menu-six-tree">
                            <li class="sidebar-item">
                                <a href="bonussum.html" class="sidebar-link trigger-side-menu menu-to-del menu-bonussum"><i class="mdi mdi-sitemap"></i><span class="hide-menu title-bonussum title-sb-report-ktrr"> K-Trak Revenue Report </span></a>
                            </li>
                            <li class="sidebar-item">
                                <a href="bonussum.html" class="sidebar-link trigger-side-menu menu-to-del menu-bonussum"><i class="mdi mdi-sitemap"></i><span class="hide-menu title-bonussum title-sb-report-mp"> Member Performance </span></a>
                            </li>

                        </ul>
                    </li> -->

                </ul>
            </nav>
        </div>
    </aside>
    <div class="page-wrapper">
        <div class="container-fluid page-content-render" style="background-image: url(assets/images/backgrounds/home.jpg);background-size: cover;padding: 0px;"></div>
        <footer class="footer text-center" style="background-color: white">
            All Rights Reserved by K-TRACS. Designed and Developed by DG Solution.
        </footer>
    </div>
</div>
  <script src="<?php echo base_url(); ?>assets/js/jquery.form.js"></script>
  <script src="<?php echo base_url(); ?>assets/libs/popper.js/dist/umd/popper.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/libs/bootstrap/dist/js/bootstrap.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/dist/js/app.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/dist/js/app.init.boxed.js"></script>
  <script src="<?php echo base_url(); ?>assets/dist/js/app-style-switcher.js"></script>
  <script src="<?php echo base_url(); ?>assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/libs/sparkline/sparkline.js"></script>
  <script src="<?php echo base_url(); ?>assets/dist/js/waves.js"></script>
  <script src="<?php echo base_url(); ?>assets/dist/js/sidebarmenu.js"></script>
  <script src="<?php echo base_url(); ?>assets/dist/js/custom.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/libs/prism/prism.js"></script>
  <script src="<?php echo base_url(); ?>assets/libs/bootstrap-switch/dist/js/bootstrap-switch.min.js"></script>
  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/libs/toastr/build/toastr.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/libs/daterangepicker/moment.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/libs/daterangepicker/daterangepicker.js"></script>
  <script src="<?php echo base_url(); ?>assets/js/dashboard.js"></script>
  <script>
	$('.title-sb-dashboard').html(bahasa_translate.sb_dashboard)
	$('.title-sb-group-filemanagement').html(bahasa_translate.sb_filemanagement)
	$('.title-sb-insurancefile').html(bahasa_translate.sb_insurancefile)
	$('.title-sb-commission').html(bahasa_translate.sb_commission)
	$('.title-sb-tax').html(bahasa_translate.sb_tax)
	$('.title-sb-group-bonusdistribution').html(bahasa_translate.sb_bonusdistribution)
	$('.title-sb-bonusdist').html(bahasa_translate.sb_bonuscalculation)
	$('.title-sb-bonus-tax').html(bahasa_translate.sb_bonustax)
	$('.title-sb-bonus-history').html(bahasa_translate.sb_bonushistory)
	$('.title-sb-group-usermanagement').html(bahasa_translate.sb_group_usermanagement)
	$('.title-sb-um-user').html(bahasa_translate.sb_um_user)
	$('.title-sb-um-role').html(bahasa_translate.sb_um_role)
	$('.title-sb-um-controllers').html(bahasa_translate.sb_um_controllers)
	$('.title-sb-group-apicallmanagement').html(bahasa_translate.sb_group_apicallmanagement)
	$('.title-sb-acm-ikm').html(bahasa_translate.sb_acm_ikm)
	$('.title-sb-acm-rh').html(bahasa_translate.sb_acm_rh)
	$('.title-sb-group-report').html(bahasa_translate.sb_group_report)
	$('.title-sb-report-ktrr').html(bahasa_translate.sb_report_ktrr)
	$('.title-sb-report-mp').html(bahasa_translate.sb_report_mp)
	$('.title-sb-tools').html(bahasa_translate.sb_tools)
	$('.sm-myprofile').html(' '+bahasa_translate.sm_myprofile)
	$('.sm-password').html(' '+bahasa_translate.sm_password)
	$('.sm-logout').html('<i class="fa fa-power-off m-r-5 m-l-5"></i>'+bahasa_translate.sm_logout)

	if(localStorage.getItem("bahasa")==1){
		$('.flag-active').html('<i class="flag-icon flag-icon-us"></i>');
	}else{
		$('.flag-active').html('<i class="flag-icon flag-icon-id"></i>');
	}

	function change_bahasa(data_bahasa){
		if(data_bahasa=='eng'){
			localStorage.setItem('bahasa', '1');
		}else{
			localStorage.setItem('bahasa', '2');
		}
		window.location.href=url+"dashboard";
	}
  </script>
</body>

</html>
