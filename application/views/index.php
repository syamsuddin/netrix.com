<!DOCTYPE html>
<html dir="ltr">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url(); ?>assets/images/K-TRACS-04-02.png" />
	<title>K-TRACS</title>
    <!-- Custom CSS -->
    <link href="<?php echo base_url(); ?>assets/dist/css/style.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/libs/toastr/build/toastr.min.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body class="dbody" style="display:none;">
    <div class="main-wrapper">
        <!-- ============================================================== -->
        <!-- Preloader - style you can find in spinners.css -->
        <!-- ============================================================== -->
        <div class="preloader">
            <div class="lds-ripple">
                <div class="lds-pos"></div>
                <div class="lds-pos"></div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- Preloader - style you can find in spinners.css -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Login box.scss -->
        <!-- ============================================================== -->
        <div class="auth-wrapper d-flex no-block justify-content-center align-items-center" style="background:url(<?php echo base_url(); ?>assets/images/backgrounds/login.jpg); background-size: cover">
            <div class="auth-box">
                <div id="loginform">
                    <div class="logo">
                        <span class="db"><img src="<?php echo base_url(); ?>assets/images/K-Link.jpg" alt="logo" style="max-height: 100px;"/></span>
						<span class="db"><img src="<?php echo base_url(); ?>assets/images/K-TRACS-04-01.png" alt="logo" style="max-height: 100px;"/></span>
                    </div>
                    <!-- Form -->
                    <div class="row">
                        <div class="col-12">
                            <form class="form-horizontal m-t-20" id="formlogin" onsubmit="login(event)">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="basic-addon1"><i class="ti-user"></i></span>
                                    </div>
                                    <input type="text" class="form-control form-control-lg" placeholder="Username" name="username" aria-label="Username" aria-describedby="basic-addon1">
                                </div>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="basic-addon2"><i class="ti-lock"></i></span>
                                    </div>
                                    <input type="password" name="password" class="form-control form-control-lg box-pwd" placeholder="Password" aria-label="Password" aria-describedby="basic-addon1">
                                    <div class="input-group-prepend">
                                        <button class="btn btn-show-pwd" type="button"><i class="fas fa-eye"></i></button>
                                        <button class="btn btn-hide-pwd" type="button"><i class="fas fa-eye-slash"></i></button>
                                    </div>
                                </div>
                                <div class="form-group text-center">
                                    <div class="col-xs-12 p-b-20">
                                        <button class="btn btn-block btn-lg btn-info" type="submit">Log In</button>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
                <div id="recoverform">
                    <div class="logo">
                        <span class="db"><img src="<?php echo base_url(); ?>assets/images/logo-icon.png" alt="logo" /></span>
                        <h5 class="font-medium m-b-20">Recover Password</h5>
                        <span>Enter your Email and instructions will be sent to you!</span>
                    </div>
                    <div class="row m-t-20">
                        <!-- Form -->
                        <form class="col-12" action="index.html">
                            <!-- email -->
                            <div class="form-group row">
                                <div class="col-12">
                                    <input class="form-control form-control-lg" type="email" required="" placeholder="Username">
                                </div>
                            </div>
                            <!-- pwd -->
                            <div class="row m-t-20">
                                <div class="col-12">
                                    <button class="btn btn-block btn-lg btn-danger" type="submit" name="action">Reset</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- Login box.scss -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper scss in scafholding.scss -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper scss in scafholding.scss -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Right Sidebar -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Right Sidebar -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- Config -->
    <!-- ============================================================== -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/3.1.2/rollups/hmac-sha256.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/3.1.2/components/enc-base64-min.js"></script>
    <script src="<?php echo base_url(); ?>assets/libs/jquery/dist/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery.form.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/config.js"></script>
	  <script src="<?php echo base_url(); ?>assets/js/bahasa.js"></script>
    <script src="<?php echo base_url(); ?>assets/libs/toastr/build/toastr.min.js"></script>
    <!-- All Required js -->
    <!-- ============================================================== -->

    <!-- Bootstrap tether Core JavaScript -->
    <script src="<?php echo base_url(); ?>assets/libs/popper.js/dist/umd/popper.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/libs/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- ============================================================== -->
    <!-- This page plugin js -->
    <!-- ============================================================== -->
    <script>
    $('[data-toggle="tooltip"]').tooltip();
    $(".preloader").fadeOut();
    // ==============================================================
    // Login and Recover Password
    // ==============================================================
    $('#to-recover').on("click", function() {
        $("#loginform").slideUp();
        $("#recoverform").fadeIn();
    });

    $(document).ready(function() {
        if(localStorage.getItem("token") !== null){
          $.ajax({
              url: url_api+'auth/session',
              type: 'GET',
              dataType: 'json',
              beforeSend: function(xhr) {
                  xhr.setRequestHeader("Authorization", "Basic " + btoa(usr + ":" + pwd));
                  xhr.setRequestHeader("commandid", "Cek Session");
                  xhr.setRequestHeader("requestid", "ceksession" + Date.now());
                  xhr.setRequestHeader("requestdt", datetime);
                  xhr.setRequestHeader("clientid", insurance_username);
                  xhr.setRequestHeader("signature", signature);
  				        xhr.setRequestHeader("uuid", uuid)
                  xhr.setRequestHeader("token", localStorage.getItem('token'))
              },
              success: function (data, textStatus, xhr) {
                 if(data.body.data.username !== undefined){
                     window.location.href=url+"dashboard";
                 }else{
  			             $('.dbody').show()
                 }
              },
              error: function (xhr, textStatus, errorThrown) {
                $('.dbody').show()
              }
          });
        }else{
          $('.dbody').show()
        }

        $(".btn-hide-pwd").hide();
        $(".btn-show-pwd").click(function(e) {
          $(".box-pwd").get(0).type = "text";
          $(".btn-show-pwd").hide();
          $(".btn-hide-pwd").show();
        });

        $(".btn-hide-pwd").click(function(e) {
          $(".box-pwd").get(0).type = "password";
          $(".btn-show-pwd").show();
          $(".btn-hide-pwd").hide();
        });
    });

    function login(e){

        e.preventDefault();
        var data = $('#formlogin').serialize();
        $.ajax({
            url: url_api+'auth',
            type: 'POST',
            dataType: 'json',
            data:data,
            beforeSend: function(xhr) {
                xhr.setRequestHeader("Authorization", "Basic " + btoa(usr + ":" + pwd));
                xhr.setRequestHeader("commandid", "Cek Login");
                xhr.setRequestHeader("requestid", "ceklogin" + Date.now());
                xhr.setRequestHeader("requestdt", datetime);
                xhr.setRequestHeader("clientid", insurance_username);
                // xhr.setRequestHeader("signature", signature);
                xhr.setRequestHeader("uuid", 'website');
            },
            success: function (data, textStatus, xhr) {
                if(data.body.token !== undefined){
                    localStorage.setItem("token",data.body.token);
                    localStorage.setItem("bahasa",'1');
                    localStorage.setItem("auth_name",data.body.auth_name);
                    localStorage.setItem("auth_password",data.body.auth_password);
                    localStorage.setItem("key_secret",data.body.key_secret);
                    localStorage.setItem("access",JSON.stringify(data.body.access));
                    window.location.href=url+"dashboard"
                }else{
                  toastr.error(data.message, 'Error!');
                }
            },
            error: function (xhr, textStatus, errorThrown) {

            }
        });
    }
    </script>
</body>

</html>
