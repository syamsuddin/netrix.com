<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="tab-content tabcontent-border p-20" id="myTabContent">
                    <div role="tabpanel" class="tab-pane fade show active" id="home5" aria-labelledby="home-tab">
                        <div class="row">
                          <div class="col-5 align-self-center">
                              <div class="d-flex align-items-center">
                                  <nav aria-label="breadcrumb">
                                      <ol class="breadcrumb">
                                          <li class="breadcrumb-item">Commission Detail</li>
                                      </ol>
                                  </nav>
                              </div>
                          </div>
                        </div>
                        <hr />
                        <!-- Row -->
                        <div class="row">
                            <div class="col-md-12" style="text-align: right;">
                                <button type="button" class="btn btn-warning detailpayment-btn-filter" data-toggle="modal" data-target="#detailpaymentmodalfilter">
                                    Filter
                                </button>
                            </div>

                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table style="margin-top:7px;" class="table table-bordered nowrap display table-striped">
                                        <thead>
                                            <tr>
                                                <th><strong>K-Link Member ID</strong></th>
                                                <th><strong>Total Premium</strong></th>
                                                <th><strong>Bonus Personal</strong></th>
                                                <th><strong>Activation Bonus</strong></th>
                                                <th><strong>Network Bonus</strong></th>
                                                <th><strong>Total Gross Bonus</strong></th>
                                            </tr>
                                        </thead>
                                        <tbody class="detailpayment-api-list">

                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <div class="col-md-12" style="margin-top:7px;">
                                <form class="paging_form_detailpayment" method="GET">
                                    <div class="ikut-table-detailpayment row" style="display: none;">
                                        <div class="col-md-3">
                                            <div class="total-data-get-detailpayment form-control"></div>
                                        </div>

                                        <div class="col-md-9">
                                            <ul class="pagination float-right">
                                                <li>
                                                    <select class="form-control change-num-records-detailpayment" name="item_per_page">
                                                        <option value="10">10</option>
                                                        <option value="20">20</option>
                                                        <option value="50">50</option>
                                                        <option value="100">100</option>
                                                    </select>
                                                </li>
                                                <li style="margin-right: 15px;">
                                                    <div style="padding: .375rem .75rem;">
                                                        Records
                                                    </div>
                                                </li>
                                                <li class="footable-page-arrow page-item first-page-detailpayment">
                                                    <a data-page="first" class="page-link link-first-page-detailpayment">«</a>
                                                </li>
                                                <li class="footable-page-arrow page-item prev-page-detailpayment" style="padding-right: .75rem;">
                                                    <a data-page="prev" class="page-link link-prev-page-detailpayment">‹</a>
                                                </li>
                                                <li>
                                                    <input type="text" class="form-control this-page-get-detailpayment" name="page" style="width: 40px;">
                                                </li>
                                                <li>
                                                    <div class="total-page-get-detailpayment" style="padding: .375rem .75rem;"></div>
                                                </li>
                                                <li>
                                                    <btn type="button" class="btn btn-success go-page-detailpayment">Go</btn>
                                                </li>
                                                <li class="footable-page-arrow page-item next-page-detailpayment" style="padding-left: .75rem;">
                                                    <a data-page="next" class="page-link link-next-page-detailpayment">›</a>
                                                </li>
                                                <li class="footable-page-arrow page-item last-page-detailpayment">
                                                    <a data-page="last" class="page-link link-last-page-detailpayment">»</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                        <!--Filter Modal -->
                        <div class="modal fade" id="detailpaymentmodalfilter" tabindex="-1" role="dialog" aria-labelledby="filterModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <form class="detailpayment_form_filter" method="POST">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="filterModalLabel">
                                              <i class="m-r-10"></i> Filter
                                            </h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" style="min-width: 125px;">Type</span>
                                                </div>
                                                <select class="form-control" name="type">
                                                    <option value="1">ALL</option>
                                                    <option value="2">SUCCESS</option>
                                                    <option value="3">FAILED</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <div class="col-md-6">
                                                <button class="btn btn-warning detailpayment-btn-reset-filter">
                                                    <i class="ti-loop"></i> Reset
                                                </button>
                                            </div>
                                            <div class="col-md-6" align="right">
                                                <button type="submit" class="btn btn-success detailpayment-btn-filter-exe">
                                                    <i class="ti-save"></i> Submit
                                                </button>
                                            </div>
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
      var this_page_detailpayment = 1;
      var total_page_detailpayment = 1;
      var id_db_detailpayment = "";

      function detailpayment_ready(id) {
          var data;
          id_db_detailpayment = id;
          table_get_detailpayment(data,id_db_detailpayment);
          detailpayment_filter();
          paging("detailpayment");
      }

      function table_get_detailpayment(data,id) {
          if(id == undefined){
              id = id_db_detailpayment;
          }

          $('.detailpayment-api-list').html('')
          $.ajax({
              url: url_api+'bonuspay/detail/'+id,
              type: 'GET',
              data: data,
              dataType: 'json',
              beforeSend: function(xhr) {
                xhr.setRequestHeader("Authorization", "Basic " + btoa(usr + ":" + pwd));
                xhr.setRequestHeader("commandid", "detailpayment");
                xhr.setRequestHeader("requestid", "detailpayment" + Date.now());
                xhr.setRequestHeader("requestdt", datetime);
                xhr.setRequestHeader("clientid", insurance_username);
                xhr.setRequestHeader("signature", signature);
              },
              success: function (res, textStatus, xhr) {
                  $('.detailpayment-api-list').html('')
                  res.body.data.map(function(value) {

                      $('.detailpayment-api-list').append(`<tr>
                                                      <td>${value.k_link_member_id}</td>
                                                      <td align="right">${Number(value.total_premium).toLocaleString('id')}</td>
                                                      <td align="right">${Number(value.total_bonus_personal).toLocaleString('id')}</td>
                                                      <td align="right">${Number(value.total_bonus_activation).toLocaleString('id')}</td>
                                                      <td align="right">${Number(value.total_bonus_network).toLocaleString('id')}</td>
                                                      <td align="right">${Number(value.total_bonus_gross).toLocaleString('id')}</td>
                                                  </tr>`)
                  });
                  total_page_detailpayment = res.body.pagination.total_page;
                  paging_ajax("detailpayment", res);
                  paging("detailpayment");
              },
              error: function (xhr, textStatus, errorThrown) {

              }
          });
      }

      function detailpayment_filter(){
          $('.detailpayment-btn-filter-exe').unbind('click');
          $('.detailpayment-btn-filter-exe').click(function(e){
              e.preventDefault();
              var data = $('.detailpayment_form_filter').serialize();
              table_get_detailpayment(data,id_db_detailpayment);
              $('#detailpaymentmodalfilter').modal('toggle');
              this_page_detailpayment = 1;
          });
      }
</script>
