<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="tab-content tabcontent-border p-20" id="myTabContent">
                    <div role="tabpanel" class="tab-pane fade show active" id="home5" aria-labelledby="home-tab">
                        <div class="row">
                          <div class="col-5 align-self-center">
                              <div class="d-flex align-items-center">
                                  <nav aria-label="breadcrumb">
                                      <ol class="breadcrumb">
                                          <li class="breadcrumb-item">History Detail</li>
                                      </ol>
                                  </nav>
                              </div>
                          </div>
                        </div>
                        <hr />
                        <!-- Row -->
                        <div class="row" id="print_history">
                            <div class="col-md-12">
								<h4>INVOICE #5669626</h4>
							</div>
                            <div class="col-md-12">
								<table style="margin-top:7px;" class="table nowrap display header-history">
									<thead>

									</thead>
									<tbody>
										<tr>
											<td>Admin</td>
											<td Align="right">To,</td>
										</tr>
										<tr>
											<td>E 104, Dharti-2,</td>
											<td Align="right">INDRIATI SE</td>
										</tr>
										<tr>
											<td>Testin 123,</td>
											<td Align="right">Testin 123,</td>
										</tr>
										<tr>
											<td>Indonesia,</td>
											<td Align="right">Indonesia,</td>
										</tr>
										<tr>
											<td>11820</td>
											<td Align="right">11820</td>
										</tr>
										<tr>
											<td></td>
											<td Align="right">Invoice Date :  23rd Jan 2018</td>
										</tr>
										<tr>
											<td></td>
											<td Align="right">Due Date :  25th Jan 2018</td>
										</tr>
									</tbody>
								</table>
							</div>
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table style="margin-top:7px;" class="table table-bordered nowrap display table-striped">
                                        <thead>
                                            <tr>
                                                <th><strong>k_link_member_id</strong></th>
                                                <th><strong>net_k_link_member_id</strong></th>
                                                <th><strong>net_k_link_member_id_tree</strong></th>
                                                <th><strong>net_k_link_member_id_level</strong></th>
                                                <th><strong>net_k_link_member_id_sub_bn</strong></th>
                                                <th><strong>k_link_member_id_sub_br</strong></th>
                                                <th><strong>k_link_member_id_rec</strong></th>
                                                <th><strong>k_link_member_id_rec_tree</strong></th>
                                                <th><strong>k_link_member_id_rec_level</strong></th>
                                            </tr>
                                        </thead>
                                        <tbody class="detailhistory-api-list">

                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <!-- <div class="col-md-12" style="margin-top:7px;">
                                <form class="paging_form_detailhistory" method="GET">
                                    <div class="ikut-table-detailhistory row" style="display: none;">
                                        <div class="col-md-3">
                                            <div class="total-data-get-detailhistory form-control"></div>
                                        </div>

                                        <div class="col-md-9">
                                            <ul class="pagination float-right">
                                                <li>
                                                    <select class="form-control change-num-records-detailhistory" name="item_per_page">
                                                        <option value="10">10</option>
                                                        <option value="20">20</option>
                                                        <option value="50">50</option>
                                                        <option value="100">100</option>
                                                    </select>
                                                </li>
                                                <li style="margin-right: 15px;">
                                                    <div style="padding: .375rem .75rem;">
                                                        Records
                                                    </div>
                                                </li>
                                                <li class="footable-page-arrow page-item first-page-detailhistory">
                                                    <a data-page="first" class="page-link link-first-page-detailhistory">«</a>
                                                </li>
                                                <li class="footable-page-arrow page-item prev-page-detailhistory" style="padding-right: .75rem;">
                                                    <a data-page="prev" class="page-link link-prev-page-detailhistory">‹</a>
                                                </li>
                                                <li>
                                                    <input type="text" class="form-control this-page-get-detailhistory" name="page" style="width: 40px;">
                                                </li>
                                                <li>
                                                    <div class="total-page-get-detailhistory" style="padding: .375rem .75rem;"></div>
                                                </li>
                                                <li>
                                                    <btn type="button" class="btn btn-success go-page-detailhistory">Go</btn>
                                                </li>
                                                <li class="footable-page-arrow page-item next-page-detailhistory" style="padding-left: .75rem;">
                                                    <a data-page="next" class="page-link link-next-page-detailhistory">›</a>
                                                </li>
                                                <li class="footable-page-arrow page-item last-page-detailhistory">
                                                    <a data-page="last" class="page-link link-last-page-detailhistory">»</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </form>
                            </div> -->
                        </div>

						<div class="row">
						<div class="col-md-12 text-right" style="margin-top:7px;">
							<button id="print" class="btn btn-default btn-outline" type="button" onclick="print_history()"> <span><i class="fa fa-print"></i> Print</span> </button>
						</div>
						</div>

                        <!--Filter Modal -->
                        <div class="modal fade" id="detailhistorymodalfilter" tabindex="-1" role="dialog" aria-labelledby="filterModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <form class="detailhistory1_form_filter" method="POST">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="filterModalLabel">
                                              <i class="fas fa-insurancefile-plus m-r-10"></i> Filter
                                            </h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" style="min-width: 125px;">Date Range</span>
                                                </div>
                                                <input type='text' class="form-control datetime-filter-insurancefile" name="date" value=""/>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <div class="col-md-6">
                                                <button class="btn btn-warning insurancefile-btn-reset-filter">
                                                    <i class="ti-loop"></i> Reset
                                                </button>
                                            </div>

                                            <div class="col-md-6" align="right">
                                                <button type="submit" class="btn btn-success insurancefile-btn-filter-exe">
                                                    <i class="ti-save"></i> Submit
                                                </button>
                                            </div>

                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
      var this_page_detailhistory = 1;
      var total_page_detailhistory = 1;
      var id_db_detailhistory = '';

      function detailhistory_ready(id) {
          var data;
          id_db_detailhistory = id;
          table_get_detailhistory(data,id_db_detailhistory);
          paging("detailhistory");
      }

      function table_get_detailhistory(datas,id,type) {
          if(id == undefined){
              id = id_db_detailhistory;
          }

          $('.detailhistory-api-list').html('')
          $.ajax({
              url: url_api+'bonushistory/detail/'+id,
              type: 'GET',
              data: datas,
              dataType: 'json',
              beforeSend: function(xhr) {
                xhr.setRequestHeader("Authorization", "Basic " + btoa(usr + ":" + pwd));
                xhr.setRequestHeader("commandid", "detailhistory");
                xhr.setRequestHeader("requestid", "detailhistory" + Date.now());
                xhr.setRequestHeader("requestdt", datetime);
                xhr.setRequestHeader("clientid", insurance_username);
                xhr.setRequestHeader("signature", signature);
              },
              success: function (res, textStatus, xhr) {
                  res.body.data.map(function(value) {

                      $('.detailhistory-api-list').append(`<tr>
                                                  <td>${value.k_link_member_id}</td>
                                                  <td>${value.net_k_link_member_id}</td>
                                                  <td>${value.net_k_link_member_id_tree}</td>
                                                  <td>${value.net_k_link_member_id_level}</td>
                                                  <td>${value.net_k_link_member_id_sub_bn}</td>
                                                  <td>${value.k_link_member_id_sub_br}</td>
                                                  <td>${value.k_link_member_id_rec}</td>
                                                  <td>${value.k_link_member_id_rec_tree}</td>
                                                  <td>${value.k_link_member_id_rec_level}</td>
                                                  </tr>`)
                  });
                  total_page_detailhistory = res.body.pagination.total_page;
                  paging_ajax("detailhistory", res);
              },
              error: function (xhr, textStatus, errorThrown) {

              }
          });
      }

	  function print_history(){
		$('.header-history').attr('width', 1080);
		var mywindow = window.open('', 'PRINT', 'height=400,width=600');

		mywindow.document.write('<html><head><title>' + document.title  + '</title>');
		mywindow.document.write('</head><body >');
		mywindow.document.write('<h1>' + document.title  + '</h1>');
		mywindow.document.write(document.getElementById('print_history').innerHTML);
		mywindow.document.write('</body></html>');

		mywindow.document.close(); // necessary for IE >= 10
		mywindow.focus(); // necessary for IE >= 10*/

		mywindow.print();
		mywindow.close();

		return true;
	  }
</script>
