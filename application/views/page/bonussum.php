<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="tab-content tabcontent-border p-20" id="myTabContent">
                    <div role="tabpanel" class="tab-pane fade show active" id="home5" aria-labelledby="home-tab">
                        <div class="row">
                            <div class="col-5 align-self-center">
                                <div class="d-flex align-items-center">
                                    <nav aria-label="breadcrumb">
                                        <ol class="breadcrumb">
                                            <li class="breadcrumb-item"><a href="#">Service</a></li>
                                            <li class="breadcrumb-item active" aria-current="page">Bonus Summary</li>
                                        </ol>
                                    </nav>
                                </div>
                            </div>
                            <div class="col-7 align-self-center">
                                <div class="d-flex no-block justify-content-end align-items-center">
                                    <div class="">
                                        <button type="submit" class="btn btn-warning float-right" data-toggle="modal" data-target="#bonussummodalfilter"> <i class="fas fa-filter"></i> Filter</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr />
                        <!-- Row -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive ">
                                    <button type="button" class="btn btn-success user-btn-new" data-toggle="modal" data-target="#bonussummodalcreate">
                                        <i class="fas fa-bonussum-plus"></i> Create New
                                    </button>

                                    <table style="margin-top:7px;" id="table-bonussum" class="table table-bordered nowrap display">
                                        <thead>
                                            <tr>
                                              <th><strong>Month</strong></th>
                                              <th><strong>Total Plan</strong></th>
                                              <th><strong>Total Amount (USD)</strong></th>
                                              <th><strong>Total Amount (IDR)</strong></th>
                                              <th><strong>Convertion Rate</strong></th>
                                              <th><strong>Status</strong></th>
                                              <th><strong>Task Type</strong></th>
                                              <th><strong>Task Start Time</strong></th>
                                              <th><strong>Task End Time</strong></th>
                                              <th><strong>Success Rate</strong></th>
                                              <th width="10%"><strong>Actions</strong></th>
                                            </tr>
                                        </thead>
                                        <tbody class="bonussum-api-list">

                                        </tbody>
                                    </table>

                                    <div class="ikut-table-bonussum" style="display: none;">
                                        <form class="paging_form_bonussum" method="GET">
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <div class="total-data-get-bonussum form-control"></div>
                                                </div>

                                                <div class="col-md-9">
                                                    <ul class="pagination float-right">
                                                        <li>
                                                            <select class="form-control change-num-records-bonussum" name="item_per_page">
                                                                <option value="10">10</option>
                                                                <option value="1">1</option>
                                                                <option value="20">20</option>
                                                                <option value="50">50</option>
                                                                <option value="100">100</option>
                                                            </select>
                                                        </li>
                                                        <li style="margin-right: 15px;">
                                                            <div style="padding: .375rem .75rem;">
                                                                Records
                                                            </div>
                                                        </li>
                                                        <li class="footable-page-arrow page-item first-page-bonussum">
                                                            <a data-page="first" class="page-link link-first-page-bonussum">«</a>
                                                        </li>
                                                        <li class="footable-page-arrow page-item prev-page-bonussum" style="padding-left: .75rem;">
                                                            <a data-page="prev" class="page-link link-prev-page-bonussum">‹</a>
                                                        </li>
                                                        <li>
                                                            <input type="text" class="form-control this-page-get-bonussum" name="page" style="width: 40px;">
                                                        </li>
                                                        <li>
                                                            <div class="total-page-get-bonussum" style="padding: .375rem .75rem;"></div>
                                                        </li>
                                                        <li>
                                                            <btn type="button" class="btn btn-success go-page-bonussum">Go</btn>
                                                        </li>
                                                        <li class="footable-page-arrow page-item next-page-bonussum" style="padding-left: .75rem;">
                                                            <a data-page="next" class="page-link link-next-page-bonussum">›</a>
                                                        </li>
                                                        <li class="footable-page-arrow page-item last-page-bonussum">
                                                            <a data-page="last" class="page-link link-last-page-bonussum">»</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!--Create New Modal -->
                        <div class="modal fade" id="bonussummodalcreate" tabindex="-1" role="dialog" aria-labelledby="createModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <form class="bonussum_form" method="POST">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="createModalLabel">
                      												<i class="fas fa-bonussum-plus m-r-10"></i> Create New User
                      											</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="input-group mb-3">
                                                <button type="button" class="btn btn-info">
                                                    <i class="ti-email text-white"></i>
                                                </button>
                                                <input type="text" class="form-control" placeholder="Enter Email" aria-label="name" name="bonussumname" />
                                            </div>
                                            <div class="input-group mb-3">
                                                <button type="button" class="btn btn-info">
                                                    <i class="ti-bonussum text-white"></i>
                                                </button>
                                                <input type="text" class="form-control" placeholder="Enter Full Name" aria-label="name" name="fullname" />
                                            </div>
                                            <div class="input-group mb-3">
                                                <button type="button" class="btn btn-info">
                                                    <i class="ti-mobile text-white"></i>
                                                </button>
                                                <input type="text" class="form-control" placeholder="Enter Phone" aria-label="name" name="phone" />
                                            </div>
                                            <div class="input-group mb-3">
                                                <button type="button" class="btn btn-info">
                                                    <i class="ti-shield text-white"></i>
                                                </button>
                                                <input type="password" class="form-control box-pwd" placeholder="Enter Password" aria-label="name" name="password" />
                                                <button class="btn btn-show-pwd" type="button"><i class="fas fa-eye"></i></button>
                                                <button class="btn btn-hide-pwd" type="button"><i class="fas fa-eye-slash"></i></button>
                                            </div>
                                            <div class="input-group mb-3">
                                                <button type="button" class="btn btn-info">
                                                    <i class="ti-key text-white"></i>
                                                </button>
                                                <select class="form-control bonussum-access-api-list" name="role_id">

                                                </select>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="submit" class="btn btn-success bonussum-btn-save">
                                                <i class="ti-save"></i> Save
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        <!--View Modal -->
                        <div class="modal fade" id="bonussummodaledit" tabindex="-1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document" style="max-width: 900px;">
                                <div class="modal-content">
                                    <form class="bonussum_form_update" method="POST">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="editModalLabel">
                      											  <i class="fas fa-bonussum-plus m-r-10 title-bonussum"></i>
                      											</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <h5>Basic Information</h5>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="input-group mb-3">
                                                        <button type="button" class="btn btn-info">
                                                            Plan Name
                                                        </button>
                                                        <input type="hidden" class="form-control id_edit" placeholder="Enter Email" aria-label="name" name="bonussum_id" />
                                                        <input type="text" class="form-control plan_name_view" aria-label="name" name="plan_name_view" />
                                                    </div>
                                                    <div class="input-group mb-3">
                                                        <button type="button" class="btn btn-info">
                                                            Created By
                                                        </button>
                                                        <input type="text" class="form-control created_by_view" aria-label="name" name="created_by_view" />
                                                    </div>
                                                    <div class="input-group mb-3">
                                                        <button type="button" class="btn btn-info">
                                                            Status
                                                        </button>
                                                        <input type="text" class="form-control status_view" aria-label="name" name="status_view" />
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="input-group mb-3">
                                                        <button type="button" class="btn btn-info">
                                                            bonussum Type
                                                        </button>
                                                        <input type="text" class="form-control bonussum_type_view" aria-label="name" name="bonussum_type_view" />
                                                    </div>
                                                    <div class="input-group mb-3">
                                                        <button type="button" class="btn btn-info">
                                                            bonussum Source
                                                        </button>
                                                        <input type="text" class="form-control bonussum_source_view" aria-label="name" name="bonussum_source_view" />
                                                    </div>
                                                    <div class="input-group mb-3">
                                                        <button type="button" class="btn btn-info">
                                                            Data File
                                                        </button>
                                                        <input type="text" class="form-control data_file_view" aria-label="name" name="data_file_view" />
                                                    </div>
                                                </div>
                                            </div>
                                            <h5>Basic Information</h5>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="input-group mb-3">
                                                        <button type="button" class="btn btn-info">
                                                            Start Time
                                                        </button>
                                                        <input type="text" class="form-control start_time_view" aria-label="name" name="start_time_view" />
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="input-group mb-3">
                                                        <button type="button" class="btn btn-info">
                                                            End Time
                                                        </button>
                                                        <input type="text" class="form-control end_time_view" aria-label="name" name="end_time_view" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="table-responsive">
                                                <table style="margin-top:7px;" class="table table-bordered nowrap display">
                                                    <thead>
                                                        <tr>
                                                            <th colspan="2"><strong>Success Items</strong></th>
                                                            <th colspan="2"><strong>Failed Items</strong></th>
                                                            <th><strong>Processing Items</strong></th>
                                                            <th><strong>Waiting Items</strong></th>
                                                            <th colspan="2"><strong>Details</strong></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td><strong>Success Items</strong></td>
                                                            <td><strong>Success Items</strong></td>
                                                            <td><strong>Failed Items</strong></td>
                                                            <td><strong>Failed Items</strong></td>
                                                            <td><strong>Processing Items</strong></td>
                                                            <td><strong>Waiting Items</strong></td>
                                                            <td><strong>Details</strong></td>
                                                            <td><strong>Details</strong></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="modal-footer">

                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        <!--Filter Modal -->
                        <div class="modal fade" id="bonussummodalfilter" tabindex="-1" role="dialog" aria-labelledby="filterModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <form class="bonussum_form_filter" method="GET">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="filterModalLabel"><i class="fas fa-filter m-r-10"></i> Choose the Filter</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                          <div class="row pt-3">
                                              <div class="col-md-6">
                                                  <div class="form-group">
                                                      <label class="control-label">Start Month</label>
                                                      <input type="text" class="form-control plan_name_filter" placeholder="Select Month Here" name="start_month_filter">
                                                  </div>
                                              </div>
                                              <div class="col-md-6">
                                                  <div class="form-group has-danger">
                                                      <label class="control-label">End Month</label>
                                                      <select class="form-control custom-select plan_type_filter" placeholder="Select Month Here" name="end_month_filter">
                                                          <option value="">Select Plan Type Here</option>
                                                          <option value="01">01</option>
                                                      </select>
                                                  </div>
                                              </div>
                                              <div class="col-md-6">
                                                  <div class="form-group">
                                                      <label class="control-label">Task Type</label>
                                                      <input type="text" class="form-control" placeholder="Select Type here" name="task_type_filter">
                                                  </div>
                                              </div>
                                              <div class="col-md-6">
                                                  <div class="form-group has-danger">
                                                      <label class="control-label">Status</label>
                                                      <select class="form-control custom-select" name="status_filter">
                                                          <option value="">Select Status Here</option>
                                                          <option value="INITIAL">INITIAL</option>
                                                          <option value="DUPLICATED">DUPLICATED</option>
                                                          <option value="CANCELLED">CANCELLED</option>
                                                          <option value="WAITING">WAITING</option>
                                                          <option value="PROCESSING">PROCESSING</option>
                                                          <option value="COMPLETED">COMPLETED</option>
                                                          <option value="FAILED">FAILED</option>
                                                      </select>
                                                  </div>
                                              </div>
                                          </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-success btn-submit-filter-bonussum"> <i class="fas fa-play"></i> Submit</button>
                                            <button type="button" class="btn btn-dark btn-reset-filter-bonussum"><i class="fas fa-undo"></i> Reset</button>
                                        </div>
                                    </form>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
	var this_page_bonussum = 1;
	var total_page_bonussum = 1;

  function bonussum_ready() {
      get_all_data_bonussum();
      save_data();
      paging("bonussum");
  }

	function table_get_bonussum(data){
		$('.bonussum-api-list').html('')
        $.ajax({
            url: url_api + 'bonussum',
            type: 'GET',
			      data: data,
            dataType: 'json',
            beforeSend: function(xhr) {
                xhr.setRequestHeader("Authorization", "Basic " + btoa(usr + ":" + pwd));
                xhr.setRequestHeader("commandid", "Cek Login");
                xhr.setRequestHeader("requestid", "ceklogin" + Date.now());
                xhr.setRequestHeader("requestdt", datetime);
                xhr.setRequestHeader("clientid", insurance_username);
                xhr.setRequestHeader("signature", signature);
            },
            success: function(res, textStatus, xhr) {
                res.body.data.map(function(value) {
                    $('.bonussum-api-list').append(`<tr>
                                                    <td>${value.GENERATE_MONTH}</td>
                                                    <td>${value.TOTAL_PLAN}</td>
                                                    <td>${value.plan_name}</td>
                                                    <td>${value.task_status}</td>
                                                    <td>${value.plan_channel}</td>
                                                    <td>${value.TASK_STATUS}</td>
                                                    <td>${value.task_start_date}</td>
                                                    <td>${value.task_end_date}</td>
                                                    <td>${value.task_end_date}</td>
                                                    <td>${value.plan_name}</td>
                                                    <td>
                                                        <button type="button" class="btn btn-info bonussum-btn-view" data-original-title="view" attr-id="${value.id}" data-toggle="modal" data-target="#bonussummodaledit"> <i class="fas fa-file-alt"></i></button>
                                                    </td>
                                                    </tr>`)
                });
        				$('.total-data-get-bonussum').text("Total Data : " + res.body.pagination.total_data);
        				$('.total-page-get-bonussum').text(" / " + res.body.pagination.total_page + " Page");
        				$('.this-page-get-bonussum').val(res.body.pagination.this_page);
        				$(".ikut-table-bonussum").show();
        				// $(".ikut-table-bonussum").css("width", $("#table-bonussum").width()).show();

        				this_page_bonussum = res.body.pagination.this_page;
        				total_page_bonussum = res.body.pagination.total_page;

        				if(res.body.pagination.this_page == 1){
        					$('.first-page-bonussum').hide();
        					$('.prev-page-bonussum').hide();
        				}else{
        					$('.first-page-bonussum').show();
        					$('.prev-page-bonussum').show();
        				}

        				if(res.body.pagination.this_page == res.body.pagination.total_page){
        					$('.next-page-bonussum').hide();
        					$('.last-page-bonussum').hide();
        				}else{
        					$('.next-page-bonussum').show();
        					$('.last-page-bonussum').show();
        				}

        				delete_data();
                edit_data();
            },
            error: function(xhr, textStatus, errorThrown) {

            }
        });
	}

    function get_all_data_bonussum() {
        table_get_bonussum();
    }

    function save_data() {
        $('.bonussum-btn-save').unbind('click');
        $('.bonussum_form').unbind('serialize');
        $('.bonussum-btn-save').click(function(e) {
            e.preventDefault();
            var data = $('.bonussum_form').serialize();
            $.ajax({
                url: url_api + 'bonussum',
                type: 'POST',
                dataType: 'json',
                data: data,
                beforeSend: function(xhr) {
                    xhr.setRequestHeader("uuid", uuid)
                    xhr.setRequestHeader("token", localStorage.getItem('token'))
                },
                success: function(data, textStatus, xhr) {
                    if (data.error == 0) {
                        $('#bonussummodalcreate').modal('toggle');
                        $('input[name="bonussum_name"]').val('')
                        get_all_data_bonussum();
                        toastr.success("Success Saved!", 'Success!');
                    } else {
                        var ret_message = "";
                        $.each(data.message, function(index, value) {
                            ret_message += "<br>" + index + ": " + value;
                        });
                        toastr.error(ret_message, 'Error!');
                    }
                },
                error: function(xhr, textStatus, errorThrown) {

                }
            });
        })
    }

    function delete_data() {
        $('.bonussum-btn-delete').unbind('click')
        $('.bonussum-btn-delete').click(function() {
            var id_param = $(this).attr('attr-id');
            swal({
                title: "Are you sure?",
                text: "Are you sure for delete this data ?",
                icon: "warning",
                buttons: ["No", "Yes"],
                dangerMode: true
            }).then(function(isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        url: url_api + 'bonussum/' + id_param,
                        type: 'DELETE',
                        dataType: 'json',
                        beforeSend: function(xhr) {
                            xhr.setRequestHeader("uuid", uuid)
                            xhr.setRequestHeader("token", localStorage.getItem('token'))
                        },
                        success: function(data, textStatus, xhr) {
                            if (data.error == 0) {
                                get_all_data_bonussum();
                            } else {
                                toastr.error(data.message, 'Error!');
                            }
                        },
                        error: function(xhr, textStatus, errorThrown) {

                        }
                    });
                }
            })
        })
    }

    function edit_data() {
        $('.bonussum-btn-view').unbind('click');
        $('.bonussum-btn-view').click(function() {
            $('.bonussum_id_edit').val('');
            $('.bonussum_name_edit').val('');
            var id_param = $(this).attr('attr-id');
            $.ajax({
                url: url_api + 'bonussum/' + id_param,
                type: 'GET',
                dataType: 'json',
                beforeSend: function(xhr) {
                    xhr.setRequestHeader("uuid", uuid)
                    xhr.setRequestHeader("token", localStorage.getItem('token'))
                },
                success: function(res, textStatus, xhr) {
                    $('.title-bonussum').text("View bonussum Detail - " + res.data[0].id);
                    $('.plan_name_view_bonussum').val(res.data[0].plan_name);
                    $('.bonussumname_edit_bonussum').val(res.data[0].bonussumname);
                    $('.fullname_edit_bonussum').val(res.data[0].fullname);
                    $('.phone_edit_bonussum').val(res.data[0].phone);
                    $('.role_id_edit_bonussum').val(res.data[0].role_id);
                    update_data();
                },
                error: function(xhr, textStatus, errorThrown) {

                }
            });
        })
    }

    function update_data() {
        $('.bonussum-btn-update').unbind('click');
        $('.bonussum_form_update').unbind('serialize');
        $('.bonussum-btn-update').click(function(e) {
            e.preventDefault();
            var data = $('.bonussum_form_update').serialize();
            $.ajax({
                url: url_api + 'bonussum/update',
                type: 'POST',
                dataType: 'json',
                data: data,
                beforeSend: function(xhr) {
                    xhr.setRequestHeader("uuid", uuid)
                    xhr.setRequestHeader("token", localStorage.getItem('token'))
                },
                success: function(data, textStatus, xhr) {
                    if (data.error == 0) {
                        $('#bonussummodaledit').modal('toggle');
                        get_all_data_bonussum();
                        toastr.success("Success Updated!", 'Success!');
                    } else {
                        var ret_message = "";
                        $.each(data.message, function(index, value) {
                            ret_message += "<br>" + index + ": " + value;
                        });
                        toastr.error(ret_message, 'Error!');
                    }
                },
                error: function(xhr, textStatus, errorThrown) {

                }
            });
        })
    }
</script>
