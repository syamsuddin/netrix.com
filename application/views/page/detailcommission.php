<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="tab-content tabcontent-border p-20" id="myTabContent">
                    <div role="tabpanel" class="tab-pane fade show active" id="home5" aria-labelledby="home-tab">
                        <div class="row">
                          <div class="col-5 align-self-center">
                              <div class="d-flex align-items-center">
                                  <nav aria-label="breadcrumb">
                                      <ol class="breadcrumb">
                                          <li class="breadcrumb-item">Commission Detail</li>
                                      </ol>
                                  </nav>
                              </div>
                          </div>
                        </div>
                        <hr />
                        <!-- Row -->
                        <div class="row">
                            <div class="col-md-12" style="text-align: right;">
                                <button type="button" class="btn btn-warning detailcommission-btn-filter" data-toggle="modal" data-target="#detailcommissionmodalfilter">
                                    Filter
                                </button>
                            </div>

                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table style="margin-top:7px;" class="table table-bordered nowrap display table-striped">
                                        <thead>
                                            <tr>
                                                <th><strong>Policy Number</strong></th>
                                                <th><strong>Policy Holder Name</strong></th>
                                                <th><strong>Product Code</strong></th>
                                                <th><strong>Due Date</strong></th>
                                                <th><strong>Frequency</strong></th>
                                                <th><strong>Premium</strong></th>
                                                <th><strong>Bonus Personal</strong></th>
                                                <th><strong>Percentage</strong></th>
                                                <th><strong>Currency</strong></th>
                                                <th><strong>K-Link Member ID</strong></th>
                                                <th><strong>Agent Code</strong></th>
                                                <th><strong>Agent Name/strong></th>
                                                <th><strong>Commission Period</strong></th>
                                                <th><strong>Issued Date</strong></th>
                                                <th><strong>Product Name</strong></th>
                                                <th><strong>Assured Name</strong></th>
                                                <th><strong>Premium Period (Years)</strong></th>
                                                <th><strong>Premium Period (Month)</strong></th>
                                                <th><strong>Gross Commission</strong></th>
                                                <th><strong>Gross Commission Percentage</strong></th>
                                                <th><strong>fci_status</strong></th>
                                                <th><strong>fci_error_id</strong></th>
                                                <th><strong>fci_error_message</strong></th>
                                                <th><strong>Activation Bonus</strong></th>
                                                <th><strong>Activation Bonus Percentage</strong></th>
                                                <th><strong>Network Bonus</strong></th>
                                                <th><strong>Network Bonus Percentage</strong></th>
                                                <th><strong>K-link Feebase</strong></th>
                                                <th><strong>K-link Feebase Percentage</strong></th>
                                                <th><strong>Extraction Status</strong></th>
                                                <th><strong>Extraction Period</strong></th>
                                            </tr>
                                        </thead>
                                        <tbody class="detailcommission-api-list">

                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <div class="col-md-12" style="margin-top:7px;">
                                <form class="paging_form_detailcommission" method="GET">
                                    <div class="ikut-table-detailcommission row" style="display: none;">
                                        <div class="col-md-3">
                                            <div class="total-data-get-detailcommission form-control"></div>
                                        </div>

                                        <div class="col-md-9">
                                            <ul class="pagination float-right">
                                                <li>
                                                    <select class="form-control change-num-records-detailcommission" name="item_per_page">
                                                        <option value="10">10</option>
                                                        <option value="20">20</option>
                                                        <option value="50">50</option>
                                                        <option value="100">100</option>
                                                    </select>
                                                </li>
                                                <li style="margin-right: 15px;">
                                                    <div style="padding: .375rem .75rem;">
                                                        Records
                                                    </div>
                                                </li>
                                                <li class="footable-page-arrow page-item first-page-detailcommission">
                                                    <a data-page="first" class="page-link link-first-page-detailcommission">«</a>
                                                </li>
                                                <li class="footable-page-arrow page-item prev-page-detailcommission" style="padding-right: .75rem;">
                                                    <a data-page="prev" class="page-link link-prev-page-detailcommission">‹</a>
                                                </li>
                                                <li>
                                                    <input type="text" class="form-control this-page-get-detailcommission" name="page" style="width: 40px;">
                                                </li>
                                                <li>
                                                    <div class="total-page-get-detailcommission" style="padding: .375rem .75rem;"></div>
                                                </li>
                                                <li>
                                                    <btn type="button" class="btn btn-success go-page-detailcommission">Go</btn>
                                                </li>
                                                <li class="footable-page-arrow page-item next-page-detailcommission" style="padding-left: .75rem;">
                                                    <a data-page="next" class="page-link link-next-page-detailcommission">›</a>
                                                </li>
                                                <li class="footable-page-arrow page-item last-page-detailcommission">
                                                    <a data-page="last" class="page-link link-last-page-detailcommission">»</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                        <!--Filter Modal -->
                        <div class="modal fade" id="detailcommissionmodalfilter" tabindex="-1" role="dialog" aria-labelledby="filterModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <form class="detailcommission_form_filter" method="POST">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="filterModalLabel">
                                              <i class="m-r-10"></i> Filter
                                            </h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" style="min-width: 125px;">Type</span>
                                                </div>
                                                <select class="form-control" name="type">
                                                    <option value="1">ALL</option>
                                                    <option value="2">SUCCESS</option>
                                                    <option value="3">FAILED</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
											<button class="btn btn-warning detailcommission-btn-reset-filter">
												Reset
											</button>
											<button type="submit" class="btn btn-success detailcommission-btn-filter-exe">
												Submit
											</button>
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
      var this_page_detailcommission = 1;
      var total_page_detailcommission = 1;
      var id_db_detailcommission = "";

      function detailcommission_ready(id) {
          var data;
          id_db_detailcommission = id;
          table_get_detailcommission(data,id_db_detailcommission);
          detailcommission_filter();
          paging("detailcommission");
      }

      function table_get_detailcommission(data,id) {
          if(id == undefined){
              id = id_db_detailcommission;
          }

          $('.detailcommission-api-list').html('')
          $.ajax({
              url: url_api+'insurancefile/detailall/'+id,
              type: 'GET',
              data: data,
              dataType: 'json',
              beforeSend: function(xhr) {
                xhr.setRequestHeader("Authorization", "Basic " + btoa(usr + ":" + pwd));
                xhr.setRequestHeader("commandid", "detailcommission");
                xhr.setRequestHeader("requestid", "detailcommission" + Date.now());
                xhr.setRequestHeader("requestdt", datetime);
                xhr.setRequestHeader("clientid", insurance_username);
                xhr.setRequestHeader("signature", signature);
              },
              success: function (res, textStatus, xhr) {
                  $('.detailcommission-api-list').html('')
                  res.body.data.map(function(value) {

                      $('.detailcommission-api-list').append(`<tr>
                                                      <td>${value.policy_number}</td>
                                                      <td>${value.policy_holder_name}</td>
                                                      <td>${value.product_code}</td>
                                                      <td>${value.due_date}</td>
                                                      <td>${value.frequency}</td>
                                                      <td align="right">${Number(value.premium).toLocaleString('id')}</td>
                                                      <td align="right">${Number(value.bonus_personal).toLocaleString('id')}</td>
                                                      <td align="right">${Number(value.bonus_personal_percentage).toLocaleString('id')}</td>
                                                      <td>${value.currency}</td>
                                                      <td>${value.k_link_member_id}</td>
                                                      <td>${value.agent_code}</td>
                                                      <td>${value.agent_name}</td>
                                                      <td>${value.commission_period}</td>
                                                      <td>${value.issued_date}</td>
                                                      <td>${value.product_name}</td>
                                                      <td>${value.assured_name}</td>
                                                      <td>${value.premium_period_years}</td>
                                                      <td>${value.premium_period_month}</td>
                                                      <td align="right">${Number(value.gross_commission).toLocaleString('id')}</td>
                                                      <td align="right">${Number(value.gross_commission_percentage).toLocaleString('id')}</td>
                                                      <td>${value.fci_status}</td>
                                                      <td>${value.fci_error_id}</td>
                                                      <td>${value.fci_error_message}</td>
                                                      <td>${value.sub_br}</td>
                                                      <td>${value.sub_br_percentage}</td>
                                                      <td>${value.sub_bn}</td>
                                                      <td>${value.sub_bn_percentage}</td>
                                                      <td>${value.commission}</td>
                                                      <td>${value.commission_percentage}</td>
                                                      <td>${value.extraction_status}</td>
                                                      <td>${value.extraction_period}</td>
                                                  </tr>`)
                  });
                  total_page_detailcommission = res.body.pagination.total_page;
                  paging_ajax("detailcommission", res);
                  paging("detailcommission");
              },
              error: function (xhr, textStatus, errorThrown) {

              }
          });
      }

      function detailcommission_filter(){
          $('.detailcommission-btn-filter-exe').unbind('click');
          $('.detailcommission-btn-filter-exe').click(function(e){
              e.preventDefault();
              var data = $('.detailcommission_form_filter').serialize();
              table_get_detailcommission(data,id_db_detailcommission);
              $('#detailcommissionmodalfilter').modal('toggle');
              this_page_detailcommission = 1;
          });
      }
</script>
