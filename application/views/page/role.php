<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="tab-content tabcontent-border p-20" id="myTabContent">
                    <div role="tabpanel" class="tab-pane fade show active" id="home5" aria-labelledby="home-tab">
                        <div class="row">
                          <div class="col-5 align-self-center">
                              <div class="d-flex align-items-center">
                                  <nav aria-label="breadcrumb">
                                      <ol class="breadcrumb">
                                          <li class="breadcrumb-item">Role</li>
                                      </ol>
                                  </nav>
                              </div>
                          </div>
                        </div>
                        <hr />
                        <!-- Row -->
                        <div class="row">
                            <div class="col-md-9">
                                <button type="button" class="btn btn-success role-btn-new" data-toggle="modal" data-target="#rolemodalcreate">
                                    Create New
                                </button>
                            </div>

                            <div class="col-md-3" align="right">
                                <button type="button" class="btn btn-warning role-btn-filter" data-toggle="modal" data-target="#rolemodalfilter">
                                    Filter
                                </button>
                                <button type="button" class="btn btn-info role-btn-refresh" onclick="refresh_data('role')">
                                    Refresh
                                </button>
                            </div>

                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table style="margin-top:7px;" id="file_export" class="table table-bordered nowrap display table-striped">
                                        <thead>
                                            <tr>
                                                <th><strong>Name</strong></th>
                                                <th width="20%"><strong>Actions</strong></th>
                                            </tr>
                                        </thead>
                                        <tbody class="role-api-list">

                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <div class="col-md-12" style="margin-top:7px;">
                                <form class="paging_form_role" method="GET">
                                    <div class="ikut-table-role row" style="display: none;">
                                        <div class="col-md-3">
                                            <div class="total-data-get-role form-control"></div>
                                        </div>

                                        <div class="col-md-9">
                                            <ul class="pagination float-right">
                                                <li>
                                                    <select class="form-control change-num-records-role" name="item_per_page">
                                                        <option value="10">10</option>
                                                        <option value="20">20</option>
                                                        <option value="50">50</option>
                                                        <option value="100">100</option>
                                                    </select>
                                                </li>
                                                <li style="margin-right: 15px;">
                                                    <div style="padding: .375rem .75rem;">
                                                        Records
                                                    </div>
                                                </li>
                                                <li class="footable-page-arrow page-item first-page-role">
                                                    <a data-page="first" class="page-link link-first-page-role">«</a>
                                                </li>
                                                <li class="footable-page-arrow page-item prev-page-role" style="padding-right: .75rem;">
                                                    <a data-page="prev" class="page-link link-prev-page-role">‹</a>
                                                </li>
                                                <li>
                                                    <input type="text" class="form-control this-page-get-role" name="page" style="width: 40px;">
                                                </li>
                                                <li>
                                                    <div class="total-page-get-role" style="padding: .375rem .75rem;"></div>
                                                </li>
                                                <li>
                                                    <btn type="button" class="btn btn-success go-page-role">Go</btn>
                                                </li>
                                                <li class="footable-page-arrow page-item next-page-role" style="padding-left: .75rem;">
                                                    <a data-page="next" class="page-link link-next-page-role">›</a>
                                                </li>
                                                <li class="footable-page-arrow page-item last-page-role">
                                                    <a data-page="last" class="page-link link-last-page-role">»</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                        <!--Create New Modal -->
                        <div class="modal fade" id="rolemodalcreate" tabindex="-1" role="dialog" aria-labelledby="createModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <form class="role_form" method="POST">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="createModalLabel">
                                              <i class="fas fa-user-plus m-r-10"></i> Create New Role
                                            </h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <label class="input-group-text" style="width: 90px;">Name</label>
                                                </div>
                                                <input type="text" class="form-control" name="role_name" placeholder="Enter Role Name">
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="submit" class="btn btn-success role-btn-save">
                                                <i class="ti-save"></i> Save
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        <!--Edit Modal -->
                        <div class="modal fade" id="rolemodaledit" tabindex="-1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <form class="role_form_update" method="POST">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="editModalLabel">
                                              <i class="fas fa-user-plus m-r-10"></i> Edit Role
                                            </h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <input type="hidden" name="role_id" class="role_id_edit" />
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <label class="input-group-text" style="width: 90px;">Name</label>
                                                </div>
                                                <input type="text" class="form-control role_name_edit" name="role_name">
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="submit" class="btn btn-success role-btn-update">
                                                <i class="ti-save"></i> Update
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        <!--Edit Controllers Modal -->
                        <div class="modal fade" id="rolemodaleditcontrollers" tabindex="-1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <form class="role_form_update_controllers" method="POST">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="editControllersModalLabel">
                                              <i class="fas fa-user-plus m-r-10"></i> Edit Access
                                            </h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                            <input type="hidden" name="role_id" class="role_id_edit" />
                                        </div>
                                        <div class="modal-body">
                                            <table style="margin-top:7px;" id="file_export" class="table table-bordered nowrap display table-striped">
                                                <thead>
                                                    <tr>
                                                        <th><strong>Name</strong></th>
                                                        <th width="10%"><strong>Access</strong></th>
                                                    </tr>
                                                </thead>
                                                <tbody class="role-api-list-controllers">

                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="submit" class="btn btn-success role-btn-update-controllers">
                                                <i class="ti-save"></i> Update
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        <!--Filter Modal -->
                        <div class="modal fade" id="rolemodalfilter" tabindex="-1" role="dialog" aria-labelledby="filterModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <form class="role_form_filter" method="POST">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="filterModalLabel">
                                              <i class="fas fa-insurancefile-plus m-r-10"></i> Filter
                                            </h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <label class="input-group-text" style="width: 90px;">Name</label>
                                                </div>
                                                <input type="text" class="form-control" name="name">
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <div class="col-md-6">
                                                <button class="btn btn-warning role-btn-reset-filter">
                                                    <i class="ti-loop"></i> Reset
                                                </button>
                                            </div>
                                            <div class="col-md-6" align="right">
                                                <button type="submit" class="btn btn-success role-btn-filter-exe">
                                                    <i class="ti-save"></i> Submit
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    var this_page_role = 1;
    var total_page_role = 1;

    function role_ready() {
        table_get_role();
        save_data_role();
        role_filter();
        paging("role");

        $('.role-btn-new').unbind('click')
        $('.role-btn-new').click(function() {
            $('input[name="role_name"]').val('')
        });

        $('.role-btn-reset-filter').unbind('click')
        $('.role-btn-reset-filter').click(function(e) {
            e.preventDefault();
            $('.role_form_filter').trigger("reset");
            $('#rolemodalfilter').modal('toggle');
            var data = $('.role_form_filter').serialize();
            table_get_role(data);
        });
    }

    function table_get_role(data) {
        $('.role-api-list').html('')
        $.ajax({
            url: url_api+'role',
            type: 'GET',
            data: data,
            dataType: 'json',
            beforeSend: function(xhr) {
              xhr.setRequestHeader("Authorization", "Basic " + btoa(usr + ":" + pwd));
              xhr.setRequestHeader("commandid", "Role");
              xhr.setRequestHeader("requestid", "role" + Date.now());
              xhr.setRequestHeader("requestdt", datetime);
              xhr.setRequestHeader("clientid", insurance_username);
              xhr.setRequestHeader("signature", signature);
            },
            success: function (res, textStatus, xhr) {
                res.body.data.map(function(value) {
                    $('.role-api-list').append(`<tr>
                                                <td>${value.name}</td>
                                                <td>
                                                    <button
                                                    type="button"
                                                    class="btn btn-warning role-btn-edit"
                                                    attr-id="${value.id}"
                                                    data-toggle="modal"
                                                    data-target="#rolemodaledit"
                                                    >
                                                    <i class="fas fa-pencil-alt"></i>
                                                    </button>
                                                    <button
                                                    type="button"
                                                    class="btn btn-success role-btn-edit-controllers"
                                                    attr-id="${value.id}"
                                                    data-toggle="modal"
                                                    data-target="#rolemodaleditcontrollers"
                                                    >
                                                    <i class="fas fa-list"></i>
                                                    </button>
                                                    <button
                                                    type="button"
                                                    class="btn btn-danger role-btn-delete"
                                                    attr-id="${value.id}"
                                                    >
                                                    <i class="fas fa-trash-alt"></i>
                                                    </button>
                                                </td>
                                            </tr>`)
                })
                delete_data_role();
                edit_data_role();
                get_list_controllers();
                paging_ajax("role", res);
            },
            error: function (xhr, textStatus, errorThrown) {

            }
        });
    }

    function save_data_role() {
        $('.role-btn-save').unbind('click');
        $('.role_form').unbind('serialize');
        $('.role-btn-save').click(function(e) {
            e.preventDefault();
            var data = $('.role_form').serialize();
            $.ajax({
                url: url_api+'role',
                type: 'POST',
                dataType: 'json',
                data:data,
                beforeSend: function(xhr) {
                  xhr.setRequestHeader("Authorization", "Basic " + btoa(usr + ":" + pwd));
                  xhr.setRequestHeader("commandid", "Role");
                  xhr.setRequestHeader("requestid", "role" + Date.now());
                  xhr.setRequestHeader("requestdt", datetime);
                  xhr.setRequestHeader("clientid", insurance_username);
                  xhr.setRequestHeader("signature", signature);
                },
                success: function (data, textStatus, xhr) {
                    if(data.body.isinsert){
                        $('#rolemodalcreate').modal('toggle');
                        $('input[name="role_name"]').val('')
                        table_get_role();
                        toastr.success("Success Saved!", 'Success!');
                    }else{
                        var ret_message = "";
                        $.each(data.body.message, function( index, value ) {
                            ret_message += "<br>" + index + ": " + value;
                        });
                        toastr.error(ret_message, 'Error!');
                    }
                },
                error: function (xhr, textStatus, errorThrown) {

                }
            });
        })
    }

    function delete_data_role() {
        $('.role-btn-delete').unbind('click')
        $('.role-btn-delete').click(function() {
            var id_param = $(this).attr('attr-id');
            swal({
                title: "Are you sure?",
                text: "Are you sure for delete this data ?",
                icon: "warning",
                buttons: ["No","Yes"],
                dangerMode: true
            }).then(function(isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        url: url_api+'role/' + id_param,
                        type: 'DELETE',
                        dataType: 'json',
                        beforeSend: function(xhr) {
                          xhr.setRequestHeader("Authorization", "Basic " + btoa(usr + ":" + pwd));
                          xhr.setRequestHeader("commandid", "Role");
                          xhr.setRequestHeader("requestid", "role" + Date.now());
                          xhr.setRequestHeader("requestdt", datetime);
                          xhr.setRequestHeader("clientid", insurance_username);
                          xhr.setRequestHeader("signature", signature);
                        },
                        success: function (data, textStatus, xhr) {
                            if(data.body.isdelete){
                                table_get_role();
                                toastr.success("Success Deleted!", 'Success!');
                            }else{
                              var ret_message = "";
                              $.each(data.body.message, function( index, value ) {
                                  ret_message += "<br>" + index + ": " + value;
                              });
                              toastr.error(ret_message, 'Error!');
                            }
                        },
                        error: function (xhr, textStatus, errorThrown) {

                        }
                    });
                }
            })
        })
    }

    function edit_data_role() {
        $('.role-btn-edit').unbind('click');
        $('.role-btn-edit').click(function() {
            $('.role_id_edit').val('');
            $('.role_name_edit').val('');
            var id_param = $(this).attr('attr-id');
            $.ajax({
            url: url_api+'role/' + id_param,
            type: 'GET',
            dataType: 'json',
            beforeSend: function(xhr) {
                xhr.setRequestHeader("Authorization", "Basic " + btoa(usr + ":" + pwd));
                xhr.setRequestHeader("commandid", "Role");
                xhr.setRequestHeader("requestid", "role" + Date.now());
                xhr.setRequestHeader("requestdt", datetime);
                xhr.setRequestHeader("clientid", insurance_username);
                xhr.setRequestHeader("signature", signature);
            },
            success: function (res, textStatus, xhr) {
                $('.role_id_edit').val(res.body.data[0].id);
                $('.role_name_edit').val(res.body.data[0].name);
                update_data_role();
            },
            error: function (xhr, textStatus, errorThrown) {

            }
        });
        })
    }

    function update_data_role() {
        $('.role-btn-update').unbind('click');
        $('.role_form_update').unbind('serialize');
        $('.role-btn-update').click(function(e) {
            e.preventDefault();
            var data = $('.role_form_update').serialize();
            $.ajax({
                url: url_api+'role/update',
                type: 'POST',
                dataType: 'json',
                data:data,
                beforeSend: function(xhr) {
                    xhr.setRequestHeader("Authorization", "Basic " + btoa(usr + ":" + pwd));
                    xhr.setRequestHeader("commandid", "Role");
                    xhr.setRequestHeader("requestid", "role" + Date.now());
                    xhr.setRequestHeader("requestdt", datetime);
                    xhr.setRequestHeader("clientid", insurance_username);
                    xhr.setRequestHeader("signature", signature);
                },
                success: function (data, textStatus, xhr) {
                    if(data.body.isupdate){
                        $('#rolemodaledit').modal('toggle');
                        table_get_role();
                        toastr.success("Success Updated!", 'Success!');
                    }else{
                        var ret_message = "";
                        $.each(data.body.message, function( index, value ) {
                            ret_message += "<br>" + index + ": " + value;
                        });
                        toastr.error(ret_message, 'Error!');
                    }
                },
                error: function (xhr, textStatus, errorThrown) {

                }
            });
        })
    }

    function get_list_controllers() {
        $('.role-btn-edit-controllers').unbind('click');
        $('.role-btn-edit-controllers').click(function(e) {
            var id_param = $(this).attr('attr-id');
            $('.role_id_edit').val(id_param);
            e.preventDefault();
            $.ajax({
                url: url_api+'role_access/'+id_param,
                type: 'GET',
                dataType: 'json',
                beforeSend: function(xhr) {
                    xhr.setRequestHeader("Authorization", "Basic " + btoa(usr + ":" + pwd));
                    xhr.setRequestHeader("commandid", "Role");
                    xhr.setRequestHeader("requestid", "role" + Date.now());
                    xhr.setRequestHeader("requestdt", datetime);
                    xhr.setRequestHeader("clientid", insurance_username);
                    xhr.setRequestHeader("signature", signature);
                },
                success: function (res, textStatus, xhr) {
                    $('.role-api-list-controllers').html('');
                    res.body.map(function(value) {
                        if(value.post == 1){
                          var check_post = "checked";
                        }

                        $('.role-api-list-controllers').append(`<tr>
                                                    <td>
                                                      ${value.name}
                                                      <input type="hidden" name="cntr_id[${value.id}]" value="${value.id}">
                                                    </td>
                                                    <td>
                                                      <div class="custom-control custom-checkbox">
                                                          <input type="checkbox" class="custom-control-input" id="post${value.id}" name="post[${value.id}]" value="1" ${check_post}>
                                                          <label class="custom-control-label" for="post${value.id}"></label>
                                                      </div>
                                                    </td>
                                                </tr>`);
                    })

                },
                error: function (xhr, textStatus, errorThrown) {

                }
            });
            update_data_role_controllers();
          });
    }

    function update_data_role_controllers() {
        $('.role-btn-update-controllers').unbind('click');
        $('.role-btn-update-controllers').click(function(e) {
            e.preventDefault();
            var data = $('.role_form_update_controllers').serialize();
            $.ajax({
                url: url_api+'role_access/post',
                type: 'POST',
                dataType: 'json',
                data:data,
                beforeSend: function(xhr) {
                    xhr.setRequestHeader("Authorization", "Basic " + btoa(usr + ":" + pwd));
                    xhr.setRequestHeader("commandid", "Role");
                    xhr.setRequestHeader("requestid", "role" + Date.now());
                    xhr.setRequestHeader("requestdt", datetime);
                    xhr.setRequestHeader("clientid", insurance_username);
                    xhr.setRequestHeader("signature", signature);
                },
                success: function (data, textStatus, xhr) {
                    if(data.body.isinsert){
                        $('#rolemodaleditcontrollers').modal('toggle');
                        table_get_role();
                        toastr.success("Success Saved!", 'Success!');
                    }else{
                        var ret_message = "";
                        $.each(data.body.message, function( index, value ) {
                            ret_message += "<br>" + index + ": " + value;
                        });
                        toastr.error(ret_message, 'Error!');
                    }
                },
                error: function (xhr, textStatus, errorThrown) {

                }
            });
        })
    }

    function role_filter(){
        $('.role-btn-filter-exe').unbind('click');
        $('.role-btn-filter-exe').click(function(e){
            e.preventDefault();
            var data = $('.role_form_filter').serialize();
            table_get_role(data);
            $('#rolemodalfilter').modal('toggle');
        });
    }
</script>
