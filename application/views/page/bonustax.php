<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="tab-content tabcontent-border p-20" id="myTabContent">
                    <div role="tabpanel" class="tab-pane fade show active" id="home5" aria-labelledby="home-tab">
                        <div class="row">
                          <div class="col-5 align-self-center">
                              <div class="d-flex align-items-center">
                                  <nav aria-label="breadcrumb">
                                      <ol class="breadcrumb">
                                          <li class="breadcrumb-item">Bonus Tax</li>
                                      </ol>
                                  </nav>
                              </div>
                          </div>
                        </div>
                        <hr />
                        <!-- Row -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <div class="row ikut-table-bonustax">
                                        <div class="col-md-9">
                                        </div>
                                        <div class="col-md-3" align="right">
                                            <button type="button" class="btn btn-warning bonustax-btn-filter" data-toggle="modal" data-target="#bonustaxmodalfilter">
                                                <i class="fas fa-filter"></i> Filter
                                            </button>
                                        </div>
                                    </div>

                                    <table style="margin-top:7px;" id="table-bonustax" class="table table-bordered nowrap display table-striped">
                                        <thead>
                                            <tr>
                                                <th><strong>Generate ID</strong></th>
                                                <th><strong>Insurance</strong></th>
                                                <th><strong>Period</strong></th>
                                                <th><strong>File</strong></th>
                                                <th><strong>Member</strong></th>
                                                <th><strong>Premium</strong></th>
                                                <th><strong>Personal</strong></th>
                                                <th><strong>Requiter</strong></th>
                                                <th><strong>Network</strong></th>
                                                <th><strong>Netrix</strong></th>
                                                <th><strong>Status</strong></th>
                                                <th><strong>Tax</strong></th>
                                                <th><strong>Actions</strong></th>
                                            </tr>
                                        </thead>
                                        <tbody class="bonustax-api-list">

                                        </tbody>
                                    </table>

                                    <div class="ikut-table-bonustax" style="display: none;">
                                        <form class="paging_form_bonustax" method="GET">
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <div class="total-data-get-bonustax form-control"></div>
                                                </div>

                                                <div class="col-md-9">
                                                    <ul class="pagination float-right">
                                                        <li>
                                                            <select class="form-control change-num-records-bonustax" name="item_per_page">
                                                                <option value="10">10</option>
                                                                <option value="20">20</option>
                                                                <option value="50">50</option>
                                                                <option value="100">100</option>
                                                            </select>
                                                        </li>
                                                        <li style="margin-right: 15px;">
                                                            <div style="padding: .375rem .75rem;">
                                                                Records
                                                            </div>
                                                        </li>
                                                        <li class="footable-page-arrow page-item first-page-bonustax">
                                                            <a data-page="first" class="page-link link-first-page-bonustax">«</a>
                                                        </li>
                                                        <li class="footable-page-arrow page-item prev-page-bonustax" style="padding-right: .75rem;">
                                                            <a data-page="prev" class="page-link link-prev-page-bonustax">‹</a>
                                                        </li>
                                                        <li>
                                                            <input type="text" class="form-control this-page-get-bonustax" name="page" style="width: 40px;">
                                                        </li>
                                                        <li>
                                                            <div class="total-page-get-bonustax" style="padding: .375rem .75rem;"></div>
                                                        </li>
                                                        <li>
                                                            <btn type="button" class="btn btn-success go-page-bonustax">Go</btn>
                                                        </li>
                                                        <li class="footable-page-arrow page-item next-page-bonustax" style="padding-left: .75rem;">
                                                            <a data-page="next" class="page-link link-next-page-bonustax">›</a>
                                                        </li>
                                                        <li class="footable-page-arrow page-item last-page-bonustax">
                                                            <a data-page="last" class="page-link link-last-page-bonustax">»</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!--Create New Modal -->
                        <div class="modal fade" id="bonustaxmodalupload" tabindex="-1" role="dialog" aria-labelledby="createModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <form class="bonustax_form" method="POST" enctype="multipart/form-data">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="createModalLabel">
                                              <i class="fas fa-bonustax-plus m-r-10"></i> Upload File CSV
                                            </h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" style="min-width: 125px;">File CSV</span>
                                                </div>
                                                <input type="file" class="form-control" placeholder="Select file CSV" aria-label="name" name="bonustax_file" id="bonustax-file" />
                                                <input type="hidden" class="form-control bonustax_id" name="bonustax_id" />
                                            </div>

                                        </div>
                                        <div class="modal-footer">
                                            <button type="submit" class="btn btn-success bonustax-btn-save">
                                                <i class="ti-save"></i> Save
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        <!--Filter Modal -->
                        <div class="modal fade" id="bonustaxmodalfilter" tabindex="-1" role="dialog" aria-labelledby="filterModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <form class="bonustax_form_filter" method="POST">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="filterModalLabel">
                                              <i class="fas fa-insurancefile-plus m-r-10"></i> Filter
                                            </h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="form-group mb-3">
                                                <label class="mr-sm-2" for="insurancefile_insurance">Insurance:</label>
                                                <select class="form-control insurance-api-list-bonustax" name="caller">
                                                    <option>ALL</option>
                                                </select>
                                            </div>
                                            <div class="form-group mb-3">
                                                <label class="mr-sm-2" for="insurancefile_datetime">Date Time Range:</label>
                                                <input class="form-control" type="month" id="start" name="start" min="2018-03" value="">
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <div class="col-md-6">
                                                <button class="btn btn-warning bonustax-btn-reset-filter">
                                                    <i class="ti-loop"></i> Reset
                                                </button>
                                            </div>

                                            <div class="col-md-6" align="right">
                                                <button type="submit" class="btn btn-success bonustax-btn-filter-exe">
                                                    <i class="ti-save"></i> Submit
                                                </button>
                                            </div>

                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    var this_page_bonustax = 1;
    var total_page_bonustax = 1;

    function bonustax_ready() {
        table_get_bonustax();
        save_data_bonustax();
        upload_bonustax();
        bonustax_filter();
        paging("bonustax");

        $('.bonustax-btn-reset-filter').unbind('click')
        $('.bonustax-btn-reset-filter').click(function(e) {
            e.preventDefault();
            $('.bonustax_form_filter').trigger("reset");
            $('#bonustaxmodalfilter').modal('toggle');
            var data = $('.bonustax_form_filter').serialize();
            table_get_bonustax(data);
        })

        $.ajax({
            url: url_api+'insurance',
            type: 'GET',
            dataType: 'json',
            beforeSend: function(xhr) {
              xhr.setRequestHeader("Authorization", "Basic " + btoa(usr + ":" + pwd));
              xhr.setRequestHeader("commandid", "insurance List");
              xhr.setRequestHeader("requestid", "insurancelist" + Date.now());
              xhr.setRequestHeader("requestdt", datetime);
              xhr.setRequestHeader("clientid", insurance_username);
              xhr.setRequestHeader("signature", signature);
            },
            success: function (res, textStatus, xhr) {
                res.body.data.map(function(value) {
                    $('.insurance-api-list-bonustax').append(`<option value="${value.id}">${value.name}</option>`);
                })
            },
            error: function (xhr, textStatus, errorThrown) {

            }
        });
    }

    function table_get_bonustax(data) {
        $('.bonustax-api-list').html('')
        $.ajax({
            url: url_api+'bonustax',
            type: 'GET',
			        data: data,
            dataType: 'json',
            beforeSend: function(xhr) {
              xhr.setRequestHeader("Authorization", "Basic " + btoa(usr + ":" + pwd));
              xhr.setRequestHeader("commandid", "bonustax");
              xhr.setRequestHeader("requestid", "bonustax" + Date.now());
              xhr.setRequestHeader("requestdt", datetime);
              xhr.setRequestHeader("clientid", insurance_username);
              xhr.setRequestHeader("signature", signature);
            },
            success: function (res, textStatus, xhr) {
                res.body.data.map(function(value) {
                    if(value.status == 5){
                        value.status = "Waiting";
                    }else if(value.status == 6){
                        value.status = "Failed";
                    }else if(value.status == 7){
                        value.status = "AUTH";
                    }else if(value.status == 2){
                        value.status = "INITIAL";
                    }

                    var button_upload = '';
                    if(value.insu_type == "EXTERNAL" && value.status == "INITIAL"){
                      button_upload = `<button type="button" class="btn btn-info bonustax-btn-upload" data-toggle="modal" data-target="#bonustaxmodalupload" attr-id="${value.id}">
                                              <i class="fas fa-upload"></i>
                                          </button>`;
                    }

                    var button_download = '';
                    if(value.status == "AUTH"){
                      button_download = `<a href="` + url_api + `uploads/tax_success/${value.id}.csv" target="_BLANK">
                        <button type="button" class="btn btn-success insurancefile-btn-download" attr-id="${value.serial_number}">
                          <i class="fas fa-download"></i>
                        </button>
                      </a>`;
                    }


                    $('.bonustax-api-list').append(`<tr>
                                                <td>${value.id}</td>
                                                <td>${value.plan_name}</td>
                                                <td>${value.generate_month}</td>
                                                <td>${value.total_plan}</td>
                                                <td>${value.total_sharing}</td>
                                                <td>${value.total_premium}</td>
                                                <td>${value.total_commission}</td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td>${value.status}</td>
                                                <td>${button_upload}</td>
                                                <td>
                                                    ${button_download}
                                                    <button
                                                    type="button"
                                                    class="btn btn-danger bonustax-btn-delete"
                                                    attr-id="${value.id}"
                                                    >
                                                    <i class="fas fa-trash-alt"></i>
                                                    </button>
                                                </td>
                                                </tr>`)
                });
                total_page_bonustax = res.body.pagination.total_page;
                paging_ajax("bonustax", res);
                delete_data_bonustax();
                upload_bonustax();

                var panjang = $("#table-bonustax").width();
                if(panjang == 0){
                  panjang = 1053;
                }
                $(".ikut-table-bonustax").css("width", panjang).show();
            },
            error: function (xhr, textStatus, errorThrown) {

            }
        });
    }

    function upload_bonustax() {
        $('.bonustax-btn-upload').unbind('click');
        $('.bonustax-btn-upload').click(function() {
            var id_param = $(this).attr('attr-id');
            $('.bonustax_id').val(id_param);
        })
    }

    function save_data_bonustax() {
        var options = {
            url: url_api+'bonustax',
            type: 'POST',
            beforeSend: function(xhr) {
                xhr.setRequestHeader("Authorization", "Basic " + btoa(usr + ":" + pwd));
                xhr.setRequestHeader("commandid", "Agent Bonus Tax File Details");
                xhr.setRequestHeader("requestid", "bonustax" + Date.now());
                xhr.setRequestHeader("requestdt", datetime);
                xhr.setRequestHeader("requesturl", "http://localhost/netrix.com/receiver.php");
                xhr.setRequestHeader("clientid", insurance_username);
                xhr.setRequestHeader("signature", signature);
            },
            success: function (data, textStatus, xhr) {
                if(data.body.isreceived){
                    $('#bonustaxmodalupload').modal('toggle');
                    table_get_bonustax();
                    toastr.success("Success Upload!", 'Success!');
                }else{
                    var ret_message = "";
                    $.each(data.body.message, function( index, value ) {
                        ret_message += "<br>" + index + ": " + value;
                    });
                    toastr.error(ret_message, 'Error!');
                }
            }
        };

        $('.bonustax_form').submit(function() {
            // inside event callbacks 'this' is the DOM element so we first
            // wrap it in a jQuery object and then invoke ajaxSubmit
            $(this).ajaxSubmit(options);

            // !!! Important !!!
            // always return false to prevent standard browser submit and page navigation
            return false;
        });
    }

    function delete_data_bonustax() {
        $('.bonustax-btn-delete').unbind('click')
        $('.bonustax-btn-delete').click(function() {
            var id_param = $(this).attr('attr-id');
            swal({
                title: "Are you sure?",
                text: "Are you sure for delete this data ?",
                icon: "warning",
                buttons: ["No","Yes"],
                dangerMode: true
            }).then(function(isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        url: url_api+'bonustax/' + id_param,
                        type: 'DELETE',
                        dataType: 'json',
                        beforeSend: function(xhr) {
                          xhr.setRequestHeader("Authorization", "Basic " + btoa(usr + ":" + pwd));
                          xhr.setRequestHeader("commandid", "bonustax");
                          xhr.setRequestHeader("requestid", "bonustax" + Date.now());
                          xhr.setRequestHeader("requestdt", datetime);
                          xhr.setRequestHeader("clientid", insurance_username);
                          xhr.setRequestHeader("signature", signature);
                        },
                        success: function (data, textStatus, xhr) {
                            if(data.body.isdelete){
                                table_get_bonustax();
                            }else{
                                var ret_message = "";
                                $.each(data.body.message, function( index, value ) {
                                    ret_message += "<br>" + index + ": " + value;
                                });
                                toastr.error(ret_message, 'Error!');
                            }
                        },
                        error: function (xhr, textStatus, errorThrown) {

                        }
                    });
                }
            })
        })
    }

    function edit_data_bonustax() {
        $('.bonustax-btn-edit').unbind('click');
        $('.bonustax-btn-edit').click(function() {
            $('.bonustax_id_edit').val('');
            $('.bonustax_name_edit').val('');
            var id_param = $(this).attr('attr-id');
            $.ajax({
            // url: url_api+'bonustax/' + id_param,
            url: url_api+'bonuscalc/',
            type: 'GET',
            dataType: 'json',
            beforeSend: function(xhr) {
              xhr.setRequestHeader("Authorization", "Basic " + btoa(usr + ":" + pwd));
              xhr.setRequestHeader("commandid", "bonustax");
              xhr.setRequestHeader("requestid", "bonustax" + Date.now());
              xhr.setRequestHeader("requestdt", datetime);
              xhr.setRequestHeader("clientid", insurance_username);
              xhr.setRequestHeader("signature", signature);
            },
            success: function (res, textStatus, xhr) {
                $('.bonustax_id_edit').val(res.body.data[0].id);
                $('.bonustax_name_edit').val(res.body.data[0].name);
                $('.bonustax_username_edit').val(res.body.data[0].username);
                $('.bonustax_secret_key_edit').val(res.body.data[0].secret_key);
                update_data_bonustax();
            },
            error: function (xhr, textStatus, errorThrown) {

            }
        });
        })
    }

    function update_data_bonustax() {
        $('.bonustax-btn-update').unbind('click');
        $('.bonustax_form_update').unbind('serialize');
        $('.bonustax-btn-update').click(function(e) {
            e.preventDefault();
            var data = $('.bonustax_form_update').serialize();
            $.ajax({
                url: url_api+'bonustax/update',
                type: 'POST',
                dataType: 'json',
                data:data,
                beforeSend: function(xhr) {
                  xhr.setRequestHeader("Authorization", "Basic " + btoa(usr + ":" + pwd));
                  xhr.setRequestHeader("commandid", "bonustax");
                  xhr.setRequestHeader("requestid", "bonustax" + Date.now());
                  xhr.setRequestHeader("requestdt", datetime);
                  xhr.setRequestHeader("clientid", insurance_username);
                  xhr.setRequestHeader("signature", signature);
                },
                success: function (data, textStatus, xhr) {
                    if(data.body.isupdate){
                        $('#bonustaxmodaledit').modal('toggle');
                        table_get_bonustax();
                        toastr.success("Success Updated!", 'Success!');
                    }else{
                        var ret_message = "";
                        $.each(data.body.message, function( index, value ) {
                            ret_message += "<br>" + index + ": " + value;
                        });
                        toastr.error(ret_message, 'Error!');
                    }
                },
                error: function (xhr, textStatus, errorThrown) {

                }
            });
        })
    }

    function bonustax_filter(){
        $('.bonustax-btn-filter-exe').unbind('click');
        $('.bonustax-btn-filter-exe').click(function(e){
            e.preventDefault();
            var data = $('.bonustax_form_filter').serialize();
            table_get_bonustax(data);
            $('#bonustaxmodalfilter').modal('toggle');
        });
    }
</script>
