<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="tab-content tabcontent-border p-20" id="myTabContent">
                    <div role="tabpanel" class="tab-pane fade show active" id="home5" aria-labelledby="home-tab">
                        <div class="row">
                          <div class="col-5 align-self-center">
                              <div class="d-flex align-items-center">
                                  <nav aria-label="breadcrumb">
                                      <ol class="breadcrumb">
                                          <li class="breadcrumb-item">User Detail</li>
                                      </ol>
                                  </nav>
                              </div>
                          </div>
                        </div>
                        <hr />
                        <!-- Row -->
                        <div class="row">
                            <div class="col-md-12">
                                <div role="tabpanel" class="tab-pane fade show active" id="profile5" aria-labelledby="profile-tab">
                                    <div class="row">
                                        <div class="col-lg-4 col-xlg-3 col-md-5">
                                            <div class="card">
                                                <div class="card-body">
                                                    <center class="m-t-30" align="center">
                                                        <div class="userdetail-picture circular--landscape" style="height: 120px; width: 120px;" data-toggle="modal" data-target="#pictureModal">
                                                        </div>
                                                    </center>
                                                    <center class="m-t-30">
                                                        <h4 class="card-title m-t-10 userdetail-fullname"></h4>
                                                        <h6 class="card-subtitle userdetail-role"></h6>
                                                    </center>
                                                </div>
                                                <div>
                                                    <hr>
                                                </div>
                                                <div class="card-body">
                                                    <small class="text-muted">Email address</small>
                                                    <h6 class="userdetail-email"></h6>
                                                    <small class="text-muted p-t-30 db">Phone</small>
                                                    <h6 class="userdetail-phone"></h6>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-8 col-xlg-9 col-md-7">
                                            <div class="card">
                                                <div class="tab-content" id="pills-tabContent">
                                                    <div class="tab-pane fade show active" id="previous-month" role="tabpanel" aria-labelledby="pills-setting-tab">
                                                        <div class="card-body">
                                                            <form class="form-horizontal form-material userdetail_form" method="POST" enctype="multipart/form-data">
                                                                <div class="form-group">
                                                                    <label class="col-md-12">Profile Picture</label>
                                                                    <div class="custom-file col-md-12">
                                                                        <input type="file" class="form-control form-control-line" id="userdetail-file" name="picture">
                                                                        <input type="hidden" class="form-control userdetail-id" name="user_id">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="col-md-12">Password</label>
                                                                    <div class="col-md-12">
                                                                        <input type="password" value="" class="form-control form-control-line" name="password">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="col-md-12">Confirmation Password</label>
                                                                    <div class="col-md-12">
                                                                        <input type="password" value="" class="form-control form-control-line" name="conf_password">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="col-md-12">Phone No</label>
                                                                    <div class="col-md-12">
                                                                        <input type="text" placeholder="Enter Mobile Number Here " name="phone" class="form-control form-control-line userdetail-box-phone">
                                                                    </div>
                                                                </div>

                                                                <div class="form-group">
                                                                    <label class="col-sm-12">Role</label>
                                                                    <div class="col-sm-12">
                                                                        <select class="form-control form-control-line userdetail-role-api-list" name="role_id">
                                                                        </select>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group">
                                                                    <label class="col-sm-12">Caller</label>
                                                                    <div class="col-sm-12">
                                                                        <select class="form-control form-control-line userdetail-caller-api-list" name="caller_id">
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <div class="col-sm-12">
                                                                        <button type="submit" class="btn btn-success float-right btn-submit-userdetail"><i class="ti-save"></i> Update Profile</button>
                                                                    </div>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div id="pictureModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-body modal-picture">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
      function userdetail_ready(id) {
          save_data_userdetail();

          $.ajax({
              url: url_api+'users/'+id,
              type: 'GET',
              dataType: 'json',
              beforeSend: function(xhr) {
                  xhr.setRequestHeader("Authorization", "Basic " + btoa(usr + ":" + pwd));
                  xhr.setRequestHeader("commandid", "userdetail List");
                  xhr.setRequestHeader("requestid", "userdetaillist" + Date.now());
                  xhr.setRequestHeader("requestdt", datetime);
                  xhr.setRequestHeader("clientid", insurance_username);
                  xhr.setRequestHeader("signature", signature);
              },
              success: function (res, textStatus, xhr) {
                  val = res.body.data[0];

                  if(val.picture === null){
                    val.picture = "/netrix-api.com/uploads/profile/def.jpg";
                  }else{
                    val.picture = "/netrix-api.com" + val.picture;
                  }

                  $('.userdetail-id').val(id);
                  $('.userdetail-fullname').text(val.fullname);
                  $('.userdetail-email').text(val.username);
                  $('.userdetail-phone').text(val.phone);
                  $('.userdetail-box-phone').val(val.phone);
                  $('.userdetail-role').text(val.role_name);
                  $('.userdetail-picture').prepend(`<img src="${val.picture}" class="rounded-circle " width="150">`);
                  $('.modal-picture').prepend(`<img src="${val.picture}" style="max-width: 470px;">`);

                  load_select(val.role_id, val.caller_id);

              },
              error: function (xhr, textStatus, errorThrown) {

              }
          });

      }

      function save_data_userdetail() {
          $('.btn-submit-userdetail').unbind('click');
          $('.userdetail_form').unbind('serialize');
          $('.btn-submit-userdetail').click(function(e) {
              var options = {
                  url: url_api+'users/update',
                  type: 'POST',
                  beforeSend: function(xhr) {
                      xhr.setRequestHeader("Authorization", "Basic " + btoa(usr + ":" + pwd));
                      xhr.setRequestHeader("commandid", "userdetails");
                      xhr.setRequestHeader("requestid", "userdetails" + Date.now());
                      xhr.setRequestHeader("requestdt", datetime);
                      xhr.setRequestHeader("clientid", "insurancefile");
                      xhr.setRequestHeader("signature", signature);
                  },
                  success: function (data, textStatus, xhr) {
                      if(data.body.isupdate){
                          toastr.success("Success Saved!", 'Success!');
                      }else{
                          var ret_message = "";
                          $.each(data.body.message, function( index, value ) {
                              ret_message += "<br>" + index + ": " + value;
                          });
                          toastr.error(ret_message, 'Error!');
                      }
                  }
              };

              $('.userdetail_form').submit(function() {
                  // inside event callbacks 'this' is the DOM element so we first
                  // wrap it in a jQuery object and then invoke ajaxSubmit
                  $(this).ajaxSubmit(options);
                  $('.btn-submit-userdetail').unbind('click');
                  $('.userdetail_form').unbind('serialize');
                  // !!! Important !!!
                  // always return false to prevent standard browser submit and page navigation
                  return false;
              });
          });
      }

      function load_select(role_id, caller_id){
          $.ajax({
              url: url_api+'role',
              type: 'GET',
              dataType: 'json',
              beforeSend: function(xhr) {
                  xhr.setRequestHeader("Authorization", "Basic " + btoa(usr + ":" + pwd));
                  xhr.setRequestHeader("commandid", "User List");
                  xhr.setRequestHeader("requestid", "userlist" + Date.now());
                  xhr.setRequestHeader("requestdt", datetime);
                  xhr.setRequestHeader("clientid", insurance_username);
                  xhr.setRequestHeader("signature", signature);
              },
              success: function (res, textStatus, xhr) {
                  res.body.data.map(function(value) {
                      $('.userdetail-role-api-list').append(`<option value="${value.id}">${value.name}</option>`);
                  })
                  $('.userdetail-role-api-list').val(role_id);
              },
              error: function (xhr, textStatus, errorThrown) {

              }
          });

          $.ajax({
              url: url_api+'insurance',
              type: 'GET',
              dataType: 'json',
              beforeSend: function(xhr) {
                  xhr.setRequestHeader("Authorization", "Basic " + btoa(usr + ":" + pwd));
                  xhr.setRequestHeader("commandid", "User List");
                  xhr.setRequestHeader("requestid", "userlist" + Date.now());
                  xhr.setRequestHeader("requestdt", datetime);
                  xhr.setRequestHeader("clientid", insurance_username);
                  xhr.setRequestHeader("signature", signature);
              },
              success: function (res, textStatus, xhr) {
                  res.body.data.map(function(value) {
                      $('.userdetail-caller-api-list').append(`<option value="${value.id}">${value.name}</option>`);
                  })
                  $('.userdetail-caller-api-list').val(caller_id);
              },
              error: function (xhr, textStatus, errorThrown) {

              }
          });
      }
</script>
