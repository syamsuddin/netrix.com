<div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-body">
              <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item">
                  <a
                    class="nav-link active"
                    id="home-tab"
                    data-toggle="tab"
                    href="#home5"
                    role="tab"
                    aria-controls="home5"
                    aria-expanded="true"
                    ><span class="hidden-sm-up"><i class="ti-home"></i></span>
                    <span class="hidden-xs-down">Authentication</span></a
                  >
                </li>
                <li class="nav-item">
                  <a
                    class="nav-link"
                    id="profile-tab"
                    data-toggle="tab"
                    href="#profile5"
                    role="tab"
                    aria-controls="profile"
                    ><span class="hidden-sm-up"><i class="ti-user"></i></span>
                    <span class="hidden-xs-down">Role Profile</span></a
                  >
                </li>
              </ul>
              <div class="tab-content tabcontent-border p-20" id="myTabContent">
                <div
                  role="tabpanel"
                  class="tab-pane fade show active"
                  id="home5"
                  aria-labelledby="home-tab"
                >
                  <!-- Row -->
                  <div class="row">
                    <div class="col-5 align-self-center">
                      <div class="d-flex align-items-center">
                        <nav aria-label="breadcrumb">
                          <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                              <a href="#">Management</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">
                              Authentication
                            </li>
                          </ol>
                        </nav>
                      </div>
                    </div>
                    <div class="col-7 align-self-center">
                      <div
                        class="d-flex no-block justify-content-end align-items-center"
                      >
                        <div class="">
                          <button
                            type="submit"
                            class="btn btn-warning float-right"
                            data-toggle="modal"
                            data-target="#filtermodel"
                          >
                            <i class="fas fa-filter"></i> Filter
                          </button>
                        </div>
                      </div>
                    </div>
                  </div>
                  <hr />
      
                  <!-- Row -->
                  <div class="row">
                    <div class="col-md-12">
                      <div class="card">
                        <div class="card-body">
                          <div class="table-responsive">
                            <button
                              type="button"
                              class="btn btn-success"
                              data-toggle="modal"
                              data-target="#createmodel"
                            >
                              <i class="fas fa-user-plus"></i> Create New
                            </button>
                            <button
                              type="submit"
                              class="btn btn-danger"
                              data-toggle="modal"
                              data-target="#deletemodel"
                            >
                              <i class="fas fa-trash-alt"></i> Delete
                            </button>
                            <table
                              style="margin-top:7px;"
                              id="file_export"
                              class="table table-bordered nowrap display"
                            >
                              <thead>
                                <tr>
                                  <th>
                                    <div class="custom-control custom-checkbox">
                                      <input
                                        type="checkbox"
                                        class="custom-control-input checkAll"
                                        value="1"
                                        id="checkbox1"
                                        style="display:none;"
                                      />
                                      <label
                                        class="custom-control-label"
                                        for="checkbox1"
                                      ></label>
                                    </div>
                                  </th>
                                  <th>Name</th>
                                  <th>Email</th>
                                  <th>Phone</th>
                                  <th>Role</th>
                                  <th>Status</th>
                                  <th>Action</th>
                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                  <td>
                                    <div class="custom-control custom-checkbox">
                                      <input
                                        type="checkbox"
                                        class="custom-control-input checkAll"
                                        id="customControlValidation2"
                                        required
                                      />
                                      <label
                                        class="custom-control-label"
                                        for="customControlValidation2"
                                      ></label>
                                    </div>
                                  </td>
                                  <td>
                                    <a href="page_mgt_authentication_member.html"
                                      ><img
                                        src="/assets/images/users/1.jpg"
                                        alt="user"
                                        class="rounded-circle"
                                        width="30"
                                      />
                                      Genelia Deshmukh</a
                                    >
                                  </td>
                                  <td>genelia@gmail.com</td>
                                  <td>+123 456 789</td>
                                  <td>
                                    <span class="label label-primary"
                                      >Administrator</span
                                    >
                                  </td>
                                  <td class="bt-switch">
                                    <input
                                      data-size="mini"
                                      type="checkbox"
                                      checked
                                      data-on-color="success"
                                      data-off-color="danger"
                                    />
                                  </td>
                                  <td>
                                    <button
                                      type="submit"
                                      class="btn btn-danger"
                                      data-toggle="tooltip"
                                      data-original-title="Delete"
                                    >
                                      <i class="fas fa-trash-alt"></i>
                                    </button>
                                  </td>
                                </tr>
                                <tr>
                                  <td>
                                    <div class="custom-control custom-checkbox">
                                      <input
                                        type="checkbox"
                                        class="custom-control-input checkAll"
                                        id="customControlValidation3"
                                        required
                                      />
                                      <label
                                        class="custom-control-label"
                                        for="customControlValidation3"
                                      ></label>
                                    </div>
                                  </td>
                                  <td>
                                    <a href="page_mgt_authentication_member.html"
                                      ><img
                                        src="/assets/images/users/2.jpg"
                                        alt="user"
                                        class="rounded-circle"
                                        width="30"
                                      />
                                      Arijit Singh</a
                                    >
                                  </td>
                                  <td>arijit@gmail.com</td>
                                  <td>+234 456 789</td>
                                  <td>
                                    <span class="label label-info">Operation</span>
                                  </td>
                                  <td class="bt-switch">
                                    <input
                                      data-size="mini"
                                      type="checkbox"
                                      checked
                                      data-on-color="success"
                                      data-off-color="danger"
                                    />
                                  </td>
                                  <td>
                                    <button
                                      type="submit"
                                      class="btn btn-danger"
                                      data-toggle="tooltip"
                                      data-original-title="Delete"
                                    >
                                      <i class="fas fa-trash-alt"></i>
                                    </button>
                                  </td>
                                </tr>
                                <tr>
                                  <td>
                                    <div class="custom-control custom-checkbox">
                                      <input
                                        type="checkbox"
                                        class="custom-control-input checkAll"
                                        id="customControlValidation4"
                                        required
                                      />
                                      <label
                                        class="custom-control-label"
                                        for="customControlValidation4"
                                      ></label>
                                    </div>
                                  </td>
                                  <td>
                                    <a href="page_mgt_authentication_member.html"
                                      ><img
                                        src="/assets/images/users/3.jpg"
                                        alt="user"
                                        class="rounded-circle"
                                        width="30"
                                      />
                                      Govinda jalan</a
                                    >
                                  </td>
                                  <td>govinda@gmail.com</td>
                                  <td>+345 456 789</td>
                                  <td>
                                    <span class="label label-success">Finance</span>
                                  </td>
                                  <td class="bt-switch">
                                    <input
                                      data-size="mini"
                                      type="checkbox"
                                      checked
                                      data-on-color="success"
                                      data-off-color="danger"
                                    />
                                  </td>
                                  <td>
                                    <button
                                      type="submit"
                                      class="btn btn-danger"
                                      data-toggle="tooltip"
                                      data-original-title="Delete"
                                    >
                                      <i class="fas fa-trash-alt"></i>
                                    </button>
                                  </td>
                                </tr>
                                <tr>
                                  <td>
                                    <div class="custom-control custom-checkbox">
                                      <input
                                        type="checkbox"
                                        class="custom-control-input checkAll"
                                        id="customControlValidation8"
                                        required
                                      />
                                      <label
                                        class="custom-control-label"
                                        for="customControlValidation8"
                                      ></label>
                                    </div>
                                  </td>
                                  <td>
                                    <a href="page_mgt_authentication_member.html"
                                      ><img
                                        src="/assets/images/users/7.jpg"
                                        alt="user"
                                        class="rounded-circle"
                                        width="30"
                                      />
                                      Ritesh Deshmukh</a
                                    >
                                  </td>
                                  <td>ritesh@gmail.com</td>
                                  <td>+123 456 789</td>
                                  <td>
                                    <span class="label label-primary"
                                      >Administrator</span
                                    >
                                  </td>
                                  <td class="bt-switch">
                                    <input
                                      data-size="mini"
                                      type="checkbox"
                                      checked
                                      data-on-color="success"
                                      data-off-color="danger"
                                    />
                                  </td>
                                  <td>
                                    <button
                                      type="submit"
                                      class="btn btn-danger"
                                      data-toggle="tooltip"
                                      data-original-title="Delete"
                                    >
                                      <i class="fas fa-trash-alt"></i>
                                    </button>
                                  </td>
                                </tr>
                                <tr>
                                  <td>
                                    <div class="custom-control custom-checkbox">
                                      <input
                                        type="checkbox"
                                        class="custom-control-input checkAll"
                                        id="customControlValidation9"
                                        required
                                      />
                                      <label
                                        class="custom-control-label"
                                        for="customControlValidation9"
                                      ></label>
                                    </div>
                                  </td>
                                  <td>
                                    <a href="page_mgt_authentication_member.html"
                                      ><img
                                        src="/assets/images/users/8.jpg"
                                        alt="user"
                                        class="rounded-circle"
                                        width="30"
                                      />
                                      Salman Khan</a
                                    >
                                  </td>
                                  <td>salman@gmail.com</td>
                                  <td>+234 456 789</td>
                                  <td>
                                    <span class="label label-info">Operation</span>
                                  </td>
                                  <td class="bt-switch">
                                    <input
                                      data-size="mini"
                                      type="checkbox"
                                      checked
                                      data-on-color="success"
                                      data-off-color="danger"
                                    />
                                  </td>
                                  <td>
                                    <button
                                      type="submit"
                                      class="btn btn-danger"
                                      data-toggle="tooltip"
                                      data-original-title="Delete"
                                    >
                                      <i class="fas fa-trash-alt"></i>
                                    </button>
                                  </td>
                                </tr>
                                <tr>
                                  <td>
                                    <div class="custom-control custom-checkbox">
                                      <input
                                        type="checkbox"
                                        class="custom-control-input checkAll"
                                        id="customControlValidation10"
                                        required
                                      />
                                      <label
                                        class="custom-control-label"
                                        for="customControlValidation10"
                                      ></label>
                                    </div>
                                  </td>
                                  <td>
                                    <a href="page_mgt_authentication_member.html"
                                      ><img
                                        src="/assets/images/users/1.jpg"
                                        alt="user"
                                        class="rounded-circle"
                                        width="30"
                                      />
                                      Govinda jalan</a
                                    >
                                  </td>
                                  <td>govinda@gmail.com</td>
                                  <td>+345 456 789</td>
                                  <td>
                                    <span class="label label-success">Finance</span>
                                  </td>
                                  <td class="bt-switch">
                                    <input
                                      data-size="mini"
                                      type="checkbox"
                                      checked
                                      data-on-color="success"
                                      data-off-color="danger"
                                    />
                                  </td>
                                  <td>
                                    <button
                                      type="submit"
                                      class="btn btn-danger"
                                      data-toggle="tooltip"
                                      data-original-title="Delete"
                                    >
                                      <i class="fas fa-trash-alt"></i>
                                    </button>
                                  </td>
                                </tr>
                                <tr>
                                  <td>
                                    <div class="custom-control custom-checkbox">
                                      <input
                                        type="checkbox"
                                        class="custom-control-input checkAll"
                                        id="customControlValidation13"
                                        required
                                      />
                                      <label
                                        class="custom-control-label"
                                        for="customControlValidation13"
                                      ></label>
                                    </div>
                                  </td>
                                  <td>
                                    <a href="page_mgt_authentication_member.html"
                                      ><img
                                        src="/assets/images/users/4.jpg"
                                        alt="user"
                                        class="rounded-circle"
                                        width="30"
                                      />
                                      Genelia Deshmukh</a
                                    >
                                  </td>
                                  <td>genelia@gmail.com</td>
                                  <td>+123 456 789</td>
                                  <td>
                                    <span class="label label-primary"
                                      >Administrator</span
                                    >
                                  </td>
                                  <td class="bt-switch">
                                    <input
                                      data-size="mini"
                                      type="checkbox"
                                      checked
                                      data-on-color="success"
                                      data-off-color="danger"
                                    />
                                  </td>
                                  <td>
                                    <button
                                      type="submit"
                                      class="btn btn-danger"
                                      data-toggle="tooltip"
                                      data-original-title="Delete"
                                    >
                                      <i class="fas fa-trash-alt"></i>
                                    </button>
                                  </td>
                                </tr>
                                <tr>
                                  <td>
                                    <div class="custom-control custom-checkbox">
                                      <input
                                        type="checkbox"
                                        class="custom-control-input checkAll"
                                        id="customControlValidation14"
                                        required
                                      />
                                      <label
                                        class="custom-control-label"
                                        for="customControlValidation14"
                                      ></label>
                                    </div>
                                  </td>
                                  <td>
                                    <a href="page_mgt_authentication_member.html"
                                      ><img
                                        src="/assets/images/users/5.jpg"
                                        alt="user"
                                        class="rounded-circle"
                                        width="30"
                                      />
                                      Arijit Singh</a
                                    >
                                  </td>
                                  <td>arijit@gmail.com</td>
                                  <td>+234 456 789</td>
                                  <td>
                                    <span class="label label-info">Operation</span>
                                  </td>
                                  <td class="bt-switch">
                                    <input
                                      data-size="mini"
                                      type="checkbox"
                                      checked
                                      data-on-color="success"
                                      data-off-color="danger"
                                    />
                                  </td>
                                  <td>
                                    <button
                                      type="submit"
                                      class="btn btn-danger"
                                      data-toggle="tooltip"
                                      data-original-title="Delete"
                                    >
                                      <i class="fas fa-trash-alt"></i>
                                    </button>
                                  </td>
                                </tr>
                                <tr>
                                  <td>
                                    <div class="custom-control custom-checkbox">
                                      <input
                                        type="checkbox"
                                        class="custom-control-input checkAll"
                                        id="customControlValidation15"
                                        required
                                      />
                                      <label
                                        class="custom-control-label"
                                        for="customControlValidation15"
                                      ></label>
                                    </div>
                                  </td>
                                  <td>
                                    <a href="page_mgt_authentication_member.html"
                                      ><img
                                        src="/assets/images/users/6.jpg"
                                        alt="user"
                                        class="rounded-circle"
                                        width="30"
                                      />
                                      Govinda jalan</a
                                    >
                                  </td>
                                  <td>govinda@gmail.com</td>
                                  <td>+345 456 789</td>
                                  <td>
                                    <span class="label label-success">Finance</span>
                                  </td>
                                  <td class="bt-switch">
                                    <input
                                      data-size="mini"
                                      type="checkbox"
                                      checked
                                      data-on-color="success"
                                      data-off-color="danger"
                                    />
                                  </td>
                                  <td>
                                    <button
                                      type="submit"
                                      class="btn btn-danger"
                                      data-toggle="tooltip"
                                      data-original-title="Delete"
                                    >
                                      <i class="fas fa-trash-alt"></i>
                                    </button>
                                  </td>
                                </tr>
                                <tr>
                                  <td>
                                    <div class="custom-control custom-checkbox">
                                      <input
                                        type="checkbox"
                                        class="custom-control-input checkAll"
                                        id="customControlValidation16"
                                        required
                                      />
                                      <label
                                        class="custom-control-label"
                                        for="customControlValidation16"
                                      ></label>
                                    </div>
                                  </td>
                                  <td>
                                    <a href="page_mgt_authentication_member.html"
                                      ><img
                                        src="/assets/images/users/1.jpg"
                                        alt="user"
                                        class="rounded-circle"
                                        width="30"
                                      />
                                      Genelia Deshmukh</a
                                    >
                                  </td>
                                  <td>genelia@gmail.com</td>
                                  <td>+123 456 789</td>
                                  <td>
                                    <span class="label label-primary"
                                      >Administrator</span
                                    >
                                  </td>
                                  <td class="bt-switch">
                                    <input
                                      data-size="mini"
                                      type="checkbox"
                                      checked
                                      data-on-color="success"
                                      data-off-color="danger"
                                    />
                                  </td>
                                  <td>
                                    <button
                                      type="submit"
                                      class="btn btn-danger"
                                      data-toggle="tooltip"
                                      data-original-title="Delete"
                                    >
                                      <i class="fas fa-trash-alt"></i>
                                    </button>
                                  </td>
                                </tr>
                                <tr>
                                  <td>
                                    <div class="custom-control custom-checkbox">
                                      <input
                                        type="checkbox"
                                        class="custom-control-input checkAll"
                                        id="customControlValidation17"
                                        required
                                      />
                                      <label
                                        class="custom-control-label"
                                        for="customControlValidation17"
                                      ></label>
                                    </div>
                                  </td>
                                  <td>
                                    <a href="page_mgt_authentication_member.html"
                                      ><img
                                        src="/assets/images/users/2.jpg"
                                        alt="user"
                                        class="rounded-circle"
                                        width="30"
                                      />
                                      Arijit Singh</a
                                    >
                                  </td>
                                  <td>arijit@gmail.com</td>
                                  <td>+234 456 789</td>
                                  <td>
                                    <span class="label label-info">Operation</span>
                                  </td>
                                  <td class="bt-switch">
                                    <input
                                      data-size="mini"
                                      type="checkbox"
                                      checked
                                      data-on-color="success"
                                      data-off-color="danger"
                                    />
                                  </td>
                                  <td>
                                    <button
                                      type="submit"
                                      class="btn btn-danger"
                                      data-toggle="tooltip"
                                      data-original-title="Delete"
                                    >
                                      <i class="fas fa-trash-alt"></i>
                                    </button>
                                  </td>
                                </tr>
                                <tr>
                                  <td>
                                    <div class="custom-control custom-checkbox">
                                      <input
                                        type="checkbox"
                                        class="custom-control-input checkAll"
                                        id="customControlValidation18"
                                        required
                                      />
                                      <label
                                        class="custom-control-label"
                                        for="customControlValidation18"
                                      ></label>
                                    </div>
                                  </td>
                                  <td>
                                    <a href="page_mgt_authentication_member.html"
                                      ><img
                                        src="/assets/images/users/3.jpg"
                                        alt="user"
                                        class="rounded-circle"
                                        width="30"
                                      />
                                      Govinda jalan</a
                                    >
                                  </td>
                                  <td>govinda@gmail.com</td>
                                  <td>+345 456 789</td>
                                  <td>
                                    <span class="label label-success">Finance</span>
                                  </td>
                                  <td class="bt-switch">
                                    <input
                                      data-size="mini"
                                      type="checkbox"
                                      checked
                                      data-on-color="success"
                                      data-off-color="danger"
                                    />
                                  </td>
                                  <td>
                                    <button
                                      type="submit"
                                      class="btn btn-danger"
                                      data-toggle="tooltip"
                                      data-original-title="Delete"
                                    >
                                      <i class="fas fa-trash-alt"></i>
                                    </button>
                                  </td>
                                </tr>
                                <tr>
                                  <td>
                                    <div class="custom-control custom-checkbox">
                                      <input
                                        type="checkbox"
                                        class="custom-control-input checkAll"
                                        id="customControlValidation22"
                                        required
                                      />
                                      <label
                                        class="custom-control-label"
                                        for="customControlValidation22"
                                      ></label>
                                    </div>
                                  </td>
                                  <td>
                                    <a href="page_mgt_authentication_member.html"
                                      ><img
                                        src="/assets/images/users/7.jpg"
                                        alt="user"
                                        class="rounded-circle"
                                        width="30"
                                      />
                                      Ritesh Deshmukh</a
                                    >
                                  </td>
                                  <td>ritesh@gmail.com</td>
                                  <td>+123 456 789</td>
                                  <td>
                                    <span class="label label-primary"
                                      >Administrator</span
                                    >
                                  </td>
                                  <td class="bt-switch">
                                    <input
                                      data-size="mini"
                                      type="checkbox"
                                      checked
                                      data-on-color="success"
                                      data-off-color="danger"
                                    />
                                  </td>
                                  <td>
                                    <button
                                      type="submit"
                                      class="btn btn-danger"
                                      data-toggle="tooltip"
                                      data-original-title="Delete"
                                    >
                                      <i class="fas fa-trash-alt"></i>
                                    </button>
                                  </td>
                                </tr>
                                <tr>
                                  <td>
                                    <div class="custom-control custom-checkbox">
                                      <input
                                        type="checkbox"
                                        class="custom-control-input checkAll"
                                        id="customControlValidation23"
                                        required
                                      />
                                      <label
                                        class="custom-control-label"
                                        for="customControlValidation23"
                                      ></label>
                                    </div>
                                  </td>
                                  <td>
                                    <a href="page_mgt_authentication_member.html"
                                      ><img
                                        src="/assets/images/users/8.jpg"
                                        alt="user"
                                        class="rounded-circle"
                                        width="30"
                                      />
                                      Salman Khan</a
                                    >
                                  </td>
                                  <td>salman@gmail.com</td>
                                  <td>+234 456 789</td>
                                  <td>
                                    <span class="label label-info">Operation</span>
                                  </td>
                                  <td class="bt-switch">
                                    <input
                                      data-size="mini"
                                      type="checkbox"
                                      checked
                                      data-on-color="success"
                                      data-off-color="danger"
                                    />
                                  </td>
                                  <td>
                                    <button
                                      type="submit"
                                      class="btn btn-danger"
                                      data-toggle="tooltip"
                                      data-original-title="Delete"
                                    >
                                      <i class="fas fa-trash-alt"></i>
                                    </button>
                                  </td>
                                </tr>
                                <tr>
                                  <td>
                                    <div class="custom-control custom-checkbox">
                                      <input
                                        type="checkbox"
                                        class="custom-control-input checkAll"
                                        id="customControlValidation24"
                                        required
                                      />
                                      <label
                                        class="custom-control-label"
                                        for="customControlValidation24"
                                      ></label>
                                    </div>
                                  </td>
                                  <td>
                                    <a href="page_mgt_authentication_member.html"
                                      ><img
                                        src="/assets/images/users/1.jpg"
                                        alt="user"
                                        class="rounded-circle"
                                        width="30"
                                      />
                                      Govinda jalan</a
                                    >
                                  </td>
                                  <td>govinda@gmail.com</td>
                                  <td>+345 456 789</td>
                                  <td>
                                    <span class="label label-success">Finance</span>
                                  </td>
                                  <td class="bt-switch">
                                    <input
                                      data-size="mini"
                                      type="checkbox"
                                      checked
                                      data-on-color="success"
                                      data-off-color="danger"
                                    />
                                  </td>
                                  <td>
                                    <button
                                      type="submit"
                                      class="btn btn-danger"
                                      data-toggle="tooltip"
                                      data-original-title="Delete"
                                    >
                                      <i class="fas fa-trash-alt"></i>
                                    </button>
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                            <div class="ikut-table" style="display:none;">
                              <div style="float:left;padding: 7px;">
                                Total Record : 12
                              </div>
      
                              <div style="float:right;">
                                <div class="text-right">
                                  <ul class="pagination">
                                    <li class="footable-page-arrow page-item">
                                      <a
                                        data-page="next"
                                        href="#next"
                                        class="page-link"
                                        >›</a
                                      >
                                    </li>
                                    <li class="footable-page-arrow page-item">
                                      <a
                                        data-page="last"
                                        href="#last"
                                        class="page-link"
                                        >»</a
                                      >
                                    </li>
                                  </ul>
                                </div>
                              </div>
      
                              <div style="float:right;padding: 7px;">/ 2 Go</div>
      
                              <div style="float:right;padding-left: 7px;">
                                <input
                                  style="width: 44px;"
                                  type="text"
                                  class="form-control"
                                />
                              </div>
      
                              <div style="float:right;">
                                <div class="text-right">
                                  <ul class="pagination">
                                    <li
                                      class="footable-page-arrow disabled page-item"
                                    >
                                      <a
                                        data-page="first"
                                        href="#first"
                                        class="page-link"
                                        >«</a
                                      >
                                    </li>
                                    <li
                                      class="footable-page-arrow disabled page-item"
                                    >
                                      <a
                                        data-page="prev"
                                        href="#prev"
                                        class="page-link"
                                        >‹</a
                                      >
                                    </li>
                                  </ul>
                                </div>
                              </div>
      
                              <div style="float:right;padding: 7px;">Records</div>
      
                              <div style="float:right;">
                                <select
                                  class="form-control"
                                  id="exampleFormControlSelect1"
                                >
                                  <option>10</option>
                                  <option>20</option>
                                  <option>50</option>
                                  <option>100</option>
                                </select>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!--Create New Modal -->
                  <div
                    class="modal fade"
                    id="createmodel"
                    tabindex="-1"
                    role="dialog"
                    aria-labelledby="createModalLabel"
                    aria-hidden="true"
                  >
                    <div class="modal-dialog" role="document">
                      <div class="modal-content">
                        <form>
                          <div class="modal-header">
                            <h5 class="modal-title" id="createModalLabel">
                              <i class="fas fa-user-plus m-r-10"></i> Create New
                              Member
                            </h5>
                            <button
                              type="button"
                              class="close"
                              data-dismiss="modal"
                              aria-label="Close"
                            >
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                          <div class="modal-body">
                            <div class="input-group mb-3">
                              <button type="button" class="btn btn-info">
                                <i class="ti-user text-white"></i>
                              </button>
                              <input
                                type="text"
                                class="form-control"
                                placeholder="Enter Name Here"
                                aria-label="name"
                              />
                            </div>
                            <div class="input-group mb-3">
                              <button type="button" class="btn btn-info">
                                <i class="ti-email text-white"></i>
                              </button>
                              <input
                                type="text"
                                class="form-control"
                                placeholder="Enter Email Here"
                                aria-label="no"
                              />
                            </div>
                            <div class="input-group mb-3">
                              <button type="button" class="btn btn-info">
                                <i class="ti-mobile text-white"></i>
                              </button>
                              <input
                                type="text"
                                class="form-control"
                                placeholder="Enter Mobile Number Here"
                                aria-label="no"
                              />
                            </div>
                            <div class="input-group mb-3">
                              <button type="button" class="btn btn-info">
                                <i class="ti-more text-white"></i>
                              </button>
                              <select
                                class="select2 form-control custom-select"
                                style="height:36px;"
                              >
                                <option>Select Role Here</option>
                                <option value="AK">Administrator</option>
                                <option value="HI">Operation</option>
                                <option value="HI">Finance</option>
                              </select>
                            </div>
                            <div class="input-group mb-3">
                              <button type="button" class="btn btn-info">
                                <i class="ti-import text-white"></i>
                              </button>
                              <div class="custom-file">
                                <input
                                  type="file"
                                  class="custom-file-input"
                                  id="inputGroupFile01"
                                />
                                <label
                                  class="custom-file-label"
                                  for="inputGroupFile01"
                                  >Choose Image</label
                                >
                              </div>
                            </div>
                          </div>
                          <div class="modal-footer">
                            <button type="submit" class="btn btn-success">
                              <i class="ti-save"></i> Save
                            </button>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                  <!--delete Modal -->
                  <div
                    class="modal fade"
                    id="deletemodel"
                    tabindex="-1"
                    role="dialog"
                    aria-labelledby="deleteModalLabel"
                    aria-hidden="true"
                  >
                    <div class="modal-dialog" role="document">
                      <div class="modal-content">
                        <form>
                          <div class="modal-header">
                            <h5 class="modal-title" id="deleteModalLabel">
                              <i class="fas fa-trash-alt m-r-10"></i> Delete
                              Confirmation
                            </h5>
                            <button
                              type="button"
                              class="close"
                              data-dismiss="modal"
                              aria-label="Close"
                            >
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                          <div class="modal-body">
                            Continue to Delete this value ?
                          </div>
                          <div class="modal-footer">
                            <button type="submit" class="btn btn-success">
                              <i class="fas fa-check"></i> Continue
                            </button>
                            <button type="submit" class="btn btn-dark">
                              <i class="fas fa-chevron-left"></i> Back
                            </button>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                  <!--Filter Modal -->
                  <div
                    class="modal fade"
                    id="filtermodel"
                    tabindex="-1"
                    role="dialog"
                    aria-labelledby="filterModalLabel"
                    aria-hidden="true"
                  >
                    <div class="modal-dialog" role="document">
                      <div class="modal-content">
                        <form>
                          <div class="modal-header">
                            <h5 class="modal-title" id="filterModalLabel">
                              <i class="fas fa-filter m-r-10"></i> Choose the Filter
                            </h5>
                            <button
                              type="button"
                              class="close"
                              data-dismiss="modal"
                              aria-label="Close"
                            >
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                          <div class="modal-body">
                            <div class="row pt-3">
                              <div class="col-md-6">
                                <div class="form-group">
                                  <label class="control-label">Name</label>
                                  <input
                                    type="text"
                                    id="firstName"
                                    class="form-control"
                                    placeholder="Input Filter Name"
                                  />
                                </div>
                              </div>
                              <div class="col-md-6">
                                <div class="form-group has-danger">
                                  <label class="control-label">Role Profile</label>
                                  <select class="form-control custom-select">
                                    <option>Select Filter Role</option>
                                    <option value="AK">Administrator</option>
                                    <option value="HI">Operation</option>
                                    <option value="HI">Finance</option>
                                  </select>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="modal-footer">
                            <button type="submit" class="btn btn-success">
                              <i class="fas fa-play"></i> Submit
                            </button>
                            <button type="button" class="btn btn-dark">
                              <i class="fas fa-undo"></i> Reset
                            </button>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
                <div
                  class="tab-pane fade"
                  id="profile5"
                  role="tabpanel"
                  aria-labelledby="profile-tab"
                >
                  <p>Example</p>
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- row -->
      <!-- ============================================================== -->
      <!-- End PAge Content -->
      <!-- ============================================================== -->
      <!-- ============================================================== -->
      <!-- Right sidebar -->
      <!-- ============================================================== -->
      <!-- .right-sidebar -->
      <!-- ============================================================== -->
      <!-- End Right sidebar -->
      <!-- ============================================================== -->
      