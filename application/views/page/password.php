<div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-body">
          <div class="tab-content tabcontent-border p-20" id="myTabContent">
            <div
              role="tabpanel"
              class="tab-pane fade show active"
              id="home5"
              aria-labelledby="home-tab"
            >
              <h3 class="title-change-password">Change Password</h3>
              <hr />
              <!-- Row -->
              <form class="password_form_update" method="POST">
                <div class="row">
                  <div class="col-md-12">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-key"></i></span>
                        </div>
                        <input type="password" class="form-control password-new password-box-pwd placeholder-password" name="password" placeholder="Insert New Password">
                        <button class="btn password-btn-show-pwd" type="button"><i class="fas fa-eye"></i></button>
                        <button class="btn password-btn-hide-pwd" type="button"><i class="fas fa-eye-slash"></i></button>
                    </div>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-key"></i></span>
                        </div>
                        <input type="password" class="form-control password-new-confirm password-box-pwd-conf  placeholder-conf-password" name="conf_password" placeholder="Confirm New Password">
                        <button class="btn password-btn-show-pwd-conf" type="button"><i class="fas fa-eye"></i></button>
                        <button class="btn password-btn-hide-pwd-conf" type="button"><i class="fas fa-eye-slash"></i></button>
                    </div>
                    <div class="text-center">
                      <button
                        type="button"
                        class="btn btn-success password-btn-save btn-bahasa-save"
                        data-dismiss="modal"
                        aria-label="Close"
                      >
                        SAVE
                      </button>
                    </div>
                  </div>
              </div>
            </form>

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <script>
        function password_ready() {
          update_data();

          $(".password-btn-hide-pwd").hide();
          $(".password-btn-show-pwd").click(function(e) {
            $(".password-box-pwd").get(0).type = "text";
            $(".password-btn-show-pwd").hide();
            $(".password-btn-hide-pwd").show();
          });

          $(".password-btn-hide-pwd").click(function(e) {
            $(".password-box-pwd").get(0).type = "password";
            $(".password-btn-show-pwd").show();
            $(".password-btn-hide-pwd").hide();
          });

          $(".password-btn-hide-pwd-conf").hide();
          $(".password-btn-show-pwd-conf").click(function(e) {
            $(".password-box-pwd-conf").get(0).type = "text";
            $(".password-btn-show-pwd-conf").hide();
            $(".password-btn-hide-pwd-conf").show();
          });

          $(".password-btn-hide-pwd-conf").click(function(e) {
            $(".password-box-pwd-conf").get(0).type = "password";
            $(".password-btn-show-pwd-conf").show();
            $(".password-btn-hide-pwd-conf").hide();
          });
        }

        function update_data(){
          $('.password-btn-save').unbind('click');
          $('.password_form_update').unbind('serialize');
          $('.password-btn-save').click(function(e) {
			  
              e.preventDefault();
              var data = $('.password_form_update').serialize();
              $.ajax({
                  url: url_api+'profile/password',
                  type: 'POST',
                  dataType: 'json',
                  data:data,
                  beforeSend: function(xhr) {
						xhr.setRequestHeader("Authorization", "Basic " + btoa(usr + ":" + pwd));
						xhr.setRequestHeader("commandid", "Change Password");
						xhr.setRequestHeader("requestid", "changepassword" + Date.now());
						xhr.setRequestHeader("requestdt", datetime);
						xhr.setRequestHeader("clientid", insurance_username);
						xhr.setRequestHeader("signature", signature);
						xhr.setRequestHeader("uuid", uuid)
						xhr.setRequestHeader("token", localStorage.getItem('token'))
                  },
                  success: function (data, textStatus, xhr) {
					  console.log(data)
                      if(data.body.isupdate==true){
                          toastr.success("Success Updated!", 'Success!');
                      }else{
                          var ret_message = "";
                          $.each(data.message, function( index, value ) {
                              ret_message += "<br>" + index + ": " + value;
                          });
                          toastr.error(ret_message, 'Error!');
                      }
                  },
                  error: function (xhr, textStatus, errorThrown) {

                  }
              });
          })
        }
		$('.title-change-password').html(bahasa_translate.title_change_password)
		$('.btn-bahasa-save').html(bahasa_translate.btn_bahasa_save)
		$(".placeholder-password").attr("placeholder",bahasa_translate.btn_placeholder_input_password);
		$(".placeholder-conf-password").attr("placeholder",bahasa_translate.btn_placeholder_input_conf_password);
  </script>
