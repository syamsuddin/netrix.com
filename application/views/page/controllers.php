<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="tab-content tabcontent-border p-20" id="myTabContent">
                    <div role="tabpanel" class="tab-pane fade show active" id="home5" aria-labelledby="home-tab">
                        <div class="row">
                          <div class="col-5 align-self-center">
                              <div class="d-flex align-items-center">
                                  <nav aria-label="breadcrumb">
                                      <ol class="breadcrumb">
                                          <li class="breadcrumb-item">Controllers</li>
                                      </ol>
                                  </nav>
                              </div>
                          </div>
                        </div>
                        <hr />
                        <!-- Row -->
                        <div class="row">
                            <div class="col-md-9">
                                <button type="button" class="btn btn-success controllers-btn-new" data-toggle="modal" data-target="#controllersmodalcreate">
                                    Create New
                                </button>
                            </div>

                            <div class="col-md-3" align="right">
                                <button type="button" class="btn btn-warning controllers-btn-filter" data-toggle="modal" data-target="#controllersmodalfilter">
                                    Filter
                                </button>
                                <button type="button" class="btn btn-info controllers-btn-refresh" onclick="refresh_data('controllers')">
                                    Refresh
                                </button>
                            </div>

                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table style="margin-top:7px;" id="file_export" class="table table-bordered nowrap display table-striped">
                                        <thead>
                                            <tr>
                                                <th><strong>Name</strong></th>
                                                <th width="10%"><strong>Actions</strong></th>
                                            </tr>
                                        </thead>
                                        <tbody class="controllers-api-list">

                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <div class="col-md-12" style="margin-top:7px;">
                                <form class="paging_form_controllers" method="GET">
                                    <div class="ikut-table-controllers row" style="display: none;">
                                        <div class="col-md-3">
                                            <div class="total-data-get-controllers form-control"></div>
                                        </div>

                                        <div class="col-md-9">
                                            <ul class="pagination float-right">
                                                <li>
                                                    <select class="form-control change-num-records-controllers" name="item_per_page">
                                                        <option value="10">10</option>
                                                        <option value="20">20</option>
                                                        <option value="50">50</option>
                                                        <option value="100">100</option>
                                                    </select>
                                                </li>
                                                <li style="margin-right: 15px;">
                                                    <div style="padding: .375rem .75rem;">
                                                        Records
                                                    </div>
                                                </li>
                                                <li class="footable-page-arrow page-item first-page-controllers">
                                                    <a data-page="first" class="page-link link-first-page-controllers">«</a>
                                                </li>
                                                <li class="footable-page-arrow page-item prev-page-controllers" style="padding-right: .75rem;">
                                                    <a data-page="prev" class="page-link link-prev-page-controllers">‹</a>
                                                </li>
                                                <li>
                                                    <input type="text" class="form-control this-page-get-controllers" name="page" style="width: 40px;">
                                                </li>
                                                <li>
                                                    <div class="total-page-get-controllers" style="padding: .375rem .75rem;"></div>
                                                </li>
                                                <li>
                                                    <btn type="button" class="btn btn-success go-page-controllers">Go</btn>
                                                </li>
                                                <li class="footable-page-arrow page-item next-page-controllers" style="padding-left: .75rem;">
                                                    <a data-page="next" class="page-link link-next-page-controllers">›</a>
                                                </li>
                                                <li class="footable-page-arrow page-item last-page-controllers">
                                                    <a data-page="last" class="page-link link-last-page-controllers">»</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                        <!--Create New Modal -->
                        <div class="modal fade" id="controllersmodalcreate" tabindex="-1" role="dialog" aria-labelledby="createModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <form class="controllers_form" method="POST">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="createModalLabel">
                                              <i class="fas fa-user-plus m-r-10"></i> Create New controllers
                                            </h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <label class="input-group-text" style="width: 90px;">Name</label>
                                                </div>
                                                <input type="text" class="form-control" name="controllers_name" placeholder="Enter Controllers Name">
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="submit" class="btn btn-success controllers-btn-save">
                                                <i class="ti-save"></i> Save
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        <!--Edit Modal -->
                        <div class="modal fade" id="controllersmodaledit" tabindex="-1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <form class="controllers_form_update" method="POST">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="editModalLabel">
                                              <i class="fas fa-user-plus m-r-10"></i> Edit Controllers
                                            </h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <input type="hidden" name="controllers_id" class="controllers_id_edit" />
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <label class="input-group-text" style="width: 90px;">Name</label>
                                                </div>
                                                <input type="text" class="form-control controllers_name_edit" name="controllers_name">
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="submit" class="btn btn-success controllers-btn-update">
                                                <i class="ti-save"></i> Update
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        <!--Filter Modal -->
                        <div class="modal fade" id="controllersmodalfilter" tabindex="-1" role="dialog" aria-labelledby="filterModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <form class="controllers_form_filter" method="POST">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="filterModalLabel">
                                              <i class="fas fa-insurancefile-plus m-r-10"></i> Filter
                                            </h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <label class="input-group-text" style="width: 90px;">Name</label>
                                                </div>
                                                <input type="text" class="form-control" name="name">
                                            </div>

                                        </div>
                                        <div class="modal-footer">
                                            <div class="col-md-6">
                                                <button class="btn btn-warning controllers-btn-reset-filter">
                                                    <i class="ti-loop"></i> Reset
                                                </button>
                                            </div>
                                            <div class="col-md-6" align="right">
                                                <button type="submit" class="btn btn-success controllers-btn-filter-exe">
                                                    <i class="ti-save"></i> Submit
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    var this_page_controllers = 1;
    var total_page_controllers = 1;

    function controllers_ready() {
        table_get_controllers();
        save_data_controllers();
        controllers_filter();
        paging("controllers");

        $('.controllers-btn-new').unbind('click')
        $('.controllers-btn-new').click(function() {
            $('input[name="controllers_name"]').val('')
        })

        $('.controllers-btn-reset-filter').unbind('click')
        $('.controllers-btn-reset-filter').click(function(e) {
            e.preventDefault();
            $('.controllers_form_filter').trigger("reset");
            $('#controllersmodalfilter').modal('toggle');
            var data = $('.controllers_form_filter').serialize();
            table_get_controllers(data);
        })
    }

    function table_get_controllers(data) {
        $('.controllers-api-list').html('')
        $.ajax({
            url: url_api+'controllers',
            type: 'GET',
            data: data,
            dataType: 'json',
            beforeSend: function(xhr) {
              xhr.setRequestHeader("Authorization", "Basic " + btoa(usr + ":" + pwd));
              xhr.setRequestHeader("commandid", "Cek Login");
              xhr.setRequestHeader("requestid", "ceklogin" + Date.now());
              xhr.setRequestHeader("requestdt", datetime);
              xhr.setRequestHeader("clientid", insurance_username);
              xhr.setRequestHeader("signature", signature);
            },
            success: function (res, textStatus, xhr) {
                res.body.data.map(function(value) {
                    $('.controllers-api-list').append(`<tr>
                                                <td>${value.name}</td>
                                                <td>
                                                    <button
                                                    type="button"
                                                    class="btn btn-warning controllers-btn-edit"
                                                    attr-id="${value.id}"
                                                    data-toggle="modal"
                                                    data-target="#controllersmodaledit"
                                                    >
                                                    <i class="fas fa-pencil-alt"></i>
                                                    </button>
                                                    <button
                                                    type="button"
                                                    class="btn btn-danger controllers-btn-delete"
                                                    attr-id="${value.id}"
                                                    >
                                                    <i class="fas fa-trash-alt"></i>
                                                    </button>
                                                </td>
                                                </tr>`)
                })
                delete_data_controllers();
                edit_data_controllers();
                paging_ajax("controllers", res);
            },
            error: function (xhr, textStatus, errorThrown) {

            }
        });
    }

    function save_data_controllers() {
        $('.controllers-btn-save').unbind('click');
        $('.controllers_form').unbind('serialize');
        $('.controllers-btn-save').click(function(e) {
            e.preventDefault();
            var data = $('.controllers_form').serialize();
            $.ajax({
                url: url_api+'controllers',
                type: 'POST',
                dataType: 'json',
                data:data,
                beforeSend: function(xhr) {
                    xhr.setRequestHeader("Authorization", "Basic " + btoa(usr + ":" + pwd));
                    xhr.setRequestHeader("commandid", "Controller");
                    xhr.setRequestHeader("requestid", "controller" + Date.now());
                    xhr.setRequestHeader("requestdt", datetime);
                    xhr.setRequestHeader("clientid", insurance_username);
                    xhr.setRequestHeader("signature", signature);
                },
                success: function (data, textStatus, xhr) {
                    if(data.body.isinsert){
                        $('#controllersmodalcreate').modal('toggle');
                        $('input[name="controllers_name"]').val('')
                        table_get_controllers();
                        toastr.success("Success Saved!", 'Success!');
                    }else{
                        var ret_message = "";
                        $.each(data.body.message, function( index, value ) {
                            ret_message += "<br>" + index + ": " + value;
                        });
                        toastr.error(ret_message, 'Error!');
                    }
                },
                error: function (xhr, textStatus, errorThrown) {

                }
            });
        })
    }

    function delete_data_controllers() {
        $('.controllers-btn-delete').unbind('click')
        $('.controllers-btn-delete').click(function() {
            var id_param = $(this).attr('attr-id');
            swal({
                title: "Are you sure?",
                text: "Are you sure for delete this data ?",
                icon: "warning",
                buttons: ["No","Yes"],
                dangerMode: true
            }).then(function(isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        url: url_api+'controllers/' + id_param,
                        type: 'DELETE',
                        dataType: 'json',
                        beforeSend: function(xhr) {
                            xhr.setRequestHeader("Authorization", "Basic " + btoa(usr + ":" + pwd));
                            xhr.setRequestHeader("commandid", "Controller");
                            xhr.setRequestHeader("requestid", "controller" + Date.now());
                            xhr.setRequestHeader("requestdt", datetime);
                            xhr.setRequestHeader("clientid", insurance_username);
                            xhr.setRequestHeader("signature", signature);
                        },
                        success: function (data, textStatus, xhr) {
                            if(data.body.isdelete){
                                table_get_controllers();
                                toastr.success("Success Deleted!", 'Success!');
                            }else{
                                toastr.error(data.message, 'Error!');
                            }
                        },
                        error: function (xhr, textStatus, errorThrown) {

                        }
                    });
                }
            })
        })
    }

    function edit_data_controllers() {
        $('.controllers-btn-edit').unbind('click');
        $('.controllers-btn-edit').click(function() {
            $('.controllers_id_edit').val('');
            $('.controllers_name_edit').val('');
            var id_param = $(this).attr('attr-id');
            $.ajax({
            url: url_api+'controllers/' + id_param,
            type: 'GET',
            dataType: 'json',
            beforeSend: function(xhr) {
              xhr.setRequestHeader("Authorization", "Basic " + btoa(usr + ":" + pwd));
              xhr.setRequestHeader("commandid", "Controller");
              xhr.setRequestHeader("requestid", "controller" + Date.now());
              xhr.setRequestHeader("requestdt", datetime);
              xhr.setRequestHeader("clientid", insurance_username);
              xhr.setRequestHeader("signature", signature);
            },
            success: function (res, textStatus, xhr) {
                $('.controllers_id_edit').val(res.body.data[0].id);
                $('.controllers_name_edit').val(res.body.data[0].name);
                update_data_controllers();
            },
            error: function (xhr, textStatus, errorThrown) {

            }
        });
        })
    }

    function update_data_controllers() {
        $('.controllers-btn-update').unbind('click');
        $('.controllers_form_update').unbind('serialize');
        $('.controllers-btn-update').click(function(e) {
            e.preventDefault();
            var data = $('.controllers_form_update').serialize();
            $.ajax({
                url: url_api+'controllers/update',
                type: 'POST',
                dataType: 'json',
                data:data,
                beforeSend: function(xhr) {
                    xhr.setRequestHeader("Authorization", "Basic " + btoa(usr + ":" + pwd));
                    xhr.setRequestHeader("commandid", "Controller");
                    xhr.setRequestHeader("requestid", "controller" + Date.now());
                    xhr.setRequestHeader("requestdt", datetime);
                    xhr.setRequestHeader("clientid", insurance_username);
                    xhr.setRequestHeader("signature", signature);
                },
                success: function (data, textStatus, xhr) {
                    if(data.body.isupdate){
                        $('#controllersmodaledit').modal('toggle');
                        table_get_controllers();
                        toastr.success("Success Update!", 'Success!');
                    }else{
                        var ret_message = "";
                        $.each(data.body.message, function( index, value ) {
                            ret_message += "<br>" + index + ": " + value;
                        });
                        toastr.error(ret_message, 'Error!');
                    }
                },
                error: function (xhr, textStatus, errorThrown) {

                }
            });
        })
    }

    function controllers_filter(){
        $('.controllers-btn-filter-exe').unbind('click');
        $('.controllers-btn-filter-exe').click(function(e){
            e.preventDefault();
            var data = $('.controllers_form_filter').serialize();
            table_get_controllers(data);
            $('#controllersmodalfilter').modal('toggle');
        });
    }
</script>
