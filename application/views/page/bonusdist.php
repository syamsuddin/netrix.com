<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="tab-content tabcontent-border p-20" id="myTabContent">
                    <div role="tabpanel" class="tab-pane fade show active" id="home5" aria-labelledby="home-tab">
                        <div class="row">
                          <div class="col-5 align-self-center">
                              <div class="d-flex align-items-center">
                                  <nav aria-label="breadcrumb">
                                      <ol class="breadcrumb">
                                          <li class="breadcrumb-item sub-bonusdist">Bonus Calculation</li>
                                      </ol>
                                  </nav>
                              </div>
                          </div>
                        </div>
                        <hr />
                        <!-- Row -->
                        <div class="row">
                            <div class="col-md-9">
                                <button type="button" class="btn btn-success bonusdist-btn-new sub-generate" data-toggle="modal" data-target="#bonusdistmodalcreate">
                                    Generate
                                </button>
                            </div>

                            <div class="col-md-3" align="right">
                                <button type="button" class="btn btn-warning bonusdist-btn-filter sub-filter" data-toggle="modal" data-target="#bonusdistmodalfilter">
                                    Filter
                                </button>
                                <button type="button" class="btn btn-info bonusdist-btn-refresh sub-refresh" onclick="refresh_data('bonusdist','bonusdist_form_filter')">
                                    Refresh
                                </button>
                            </div>

                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table style="margin-top:7px;" id="table-bonusdist" class="table table-bordered nowrap display table-striped">
                                        <thead>
                                            <tr>
                                                <th><strong class="th-bonusdist-id">ID</strong></th>
                                                <th><strong class="th-bonusdist-ins">INSURANCE</strong></th>
                                                <th><strong class="th-bonusdist-period">PERIOD</strong></th>
                                                <th><strong class="th-bonusdist-cfc">FILE #</strong></th>
                                                <th><strong class="th-bonusdist-ct">TRANSACTION #</strong></th>
                                                <th><strong class="th-bonusdist-ck">MEMBER #</strong></th>
                                                <th><strong class="th-bonusdist-tp">PREMIUM</strong></th>
                                                <th><strong class="th-bonusdist-tbp">BONUS PERSONAL</strong></th>
                                                <th><strong class="th-bonusdist-tgc">GROSS COMMISSION</strong></th>
                                                <th><strong class="th-bonusdist-status">PROCESS</strong></th>
                                                <th width="20%"><strong class="th-bonusdist-act">ACTION</strong></th>
                                            </tr>
                                        </thead>
                                        <tbody class="bonusdist-api-list">

                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <div class="col-md-12" style="margin-top:7px;">
                                <form class="paging_form_bonusdist" method="GET">
                                    <div class="ikut-table-bonusdist row">
                                        <div class="col-md-3">
                                            <div class="total-data-get-bonusdist form-control"></div>
                                        </div>

                                        <div class="col-md-9">
                                            <ul class="pagination float-right">
                                                <li>
                                                    <select class="form-control change-num-records-bonusdist" name="item_per_page">
                                                        <option value="10">10</option>
                                                        <option value="20">20</option>
                                                        <option value="50">50</option>
                                                        <option value="100">100</option>
                                                    </select>
                                                </li>
                                                <li style="margin-right: 15px;">
                                                    <div style="padding: .375rem .75rem;" class="sub-records">
                                                        Records
                                                    </div>
                                                </li>
                                                <li class="footable-page-arrow page-item first-page-bonusdist">
                                                    <a data-page="first" class="page-link link-first-page-bonusdist">«</a>
                                                </li>
                                                <li class="footable-page-arrow page-item prev-page-bonusdist" style="padding-right: .75rem;">
                                                    <a data-page="prev" class="page-link link-prev-page-bonusdist">‹</a>
                                                </li>
                                                <li>
                                                    <input type="text" class="form-control this-page-get-bonusdist" name="page" style="width: 40px;">
                                                </li>
                                                <li>
                                                    <div class="total-page-get-bonusdist" style="padding: .375rem .75rem;"></div>
                                                </li>
                                                <li>
                                                    <btn type="button" class="btn btn-success go-page-bonusdist sub-go">Go</btn>
                                                </li>
                                                <li class="footable-page-arrow page-item next-page-bonusdist" style="padding-left: .75rem;">
                                                    <a data-page="next" class="page-link link-next-page-bonusdist">›</a>
                                                </li>
                                                <li class="footable-page-arrow page-item last-page-bonusdist">
                                                    <a data-page="last" class="page-link link-last-page-bonusdist">»</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                        <!--Create New Modal -->
                        <div class="modal fade" id="bonusdistmodalcreate" tabindex="-1" role="dialog" aria-labelledby="createModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <form class="bonusdist_form" method="POST" enctype="multipart/form-data" role="form">
                                        <div class="modal-header">
                                            <h5 class="modal-title sub-generate" id="createModalLabel">
                                              Generate
                                            </h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text sub-label-ins" style="min-width: 125px;">Insurance</span>
                                                </div>
                                                <select class="form-control insurance-api-list-bonusdist" name="bonusdist_insurance" id="bonusdist_insurance_select">

                                                </select>
                                            </div>
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text sub-label-file-period" style="min-width: 125px;">File Period</span>
                                                </div>
                                                <input class="form-control" id="bonusdist_period_select" type="month" name="start" min="2018-03" value="<?php echo date(" Y-m ");?>">
                                            </div>
                                            <div class="form-group file-commission-api-bonusdist" align="center">

                                            </div>

                                        </div>
                                        <div class="modal-footer">
                                            <button type="submit" class="btn btn-success bonusdist-btn-save sub-save">
                                                Save
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        <!--Tax Modal -->
                        <div class="modal fade" id="bonusdistmodaltax" tabindex="-1" role="dialog" aria-labelledby="createModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <form class="bonusdist_form_tax" method="POST" enctype="multipart/form-data">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="createModalLabel">
                                              <i class="fas fa-bonusdist-plus m-r-10"></i>Select Tax
                                            </h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" style="min-width: 125px;">Tax</span>
                                                </div>
                                                <input type="hidden" name="bonusdist_id_tax" class="bonusdist_id_tax">
                                                <input type="hidden" name="bonusdist_period" class="bonusdist_period">
                                                <select class="form-control tax-api-list-bonusdist" name="bonusdist_tax_id_tax">

                                                </select>
                                            </div>

                                        </div>
                                        <div class="modal-footer">
                                            <button type="submit" class="btn btn-success bonusdist-btn-update-tax">
                                                <i class="ti-save"></i> Save
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        <!--Filter Modal -->
                        <div class="modal fade" id="bonusdistmodalfilter" tabindex="-1" role="dialog" aria-labelledby="filterModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <form class="bonusdist_form_filter" method="POST">
                                        <div class="modal-header">
                                            <h5 class="modal-title sub-filter" id="filterModalLabel">
                                              Filter
                                            </h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text sub-label-ins" style="min-width: 125px;">Insurance</span>
                                                </div>
                                                <select class="form-control insurance-api-list-bonusdist insurance-api-list-bonusdist-filter" name="caller">

                                                </select>
                                            </div>
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text sub-label-period" style="min-width: 125px;">Period</span>
                                                </div>
                                                <input class="form-control" type="month" id="start" name="date" min="2018-03" value="">
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button class="btn btn-warning bonusdist-btn-reset-filter sub-reset">
                                                Reset
                                            </button>

                                            <button type="submit" class="btn btn-success bonusdist-btn-filter-exe sub-submit">
                                                Submit
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
      var this_page_bonusdist = 1;
      var total_page_bonusdist = 1;

      function bonusdist_ready() {
          table_get_bonusdist();
          save_data_bonusdist();
          bonusdist_filter();
          paging("bonusdist");
          tax_data_bonusdist();

          var lang = localStorage.getItem("bahasa");

          $('.bonusdist-btn-new').unbind('click')
          $('.bonusdist-btn-new').click(function() {
              $('.bonusdist_form').trigger("reset");
              $('.file-commission-api-bonusdist').html('');
          })

          $('.bonusdist-btn-reset-filter').unbind('click')
          $('.bonusdist-btn-reset-filter').click(function(e) {
              e.preventDefault();
              $('.bonusdist_form_filter').trigger("reset");
              $('#bonusdistmodalfilter').modal('toggle');
              var data = $('.bonusdist_form_filter').serialize();
              table_get_bonusdist(data);
          })

          $.ajax({
              url: url_api+'insurance',
              type: 'GET',
              dataType: 'json',
              beforeSend: function(xhr) {
                xhr.setRequestHeader("Authorization", "Basic " + btoa(usr + ":" + pwd));
                xhr.setRequestHeader("commandid", "insurance List");
                xhr.setRequestHeader("requestid", "insurancelist" + Date.now());
                xhr.setRequestHeader("requestdt", datetime);
                xhr.setRequestHeader("clientid", insurance_username);
                xhr.setRequestHeader("signature", signature);
              },
              success: function (res, textStatus, xhr) {
				  $('.insurance-api-list-bonusdist').html('');
                  $('.insurance-api-list-bonusdist-filter').append(`<option value="ALL">ALL</option>`);
                  res.body.data.map(function(value) {
                      $('.insurance-api-list-bonusdist').append(`<option value="${value.id}">${value.name}</option>`);
                  })
              },
              error: function (xhr, textStatus, errorThrown) {

              }
          });

          $('#bonusdist_insurance_select').change(function(e){
              $('#bonusdist_period_select').val('');
              $('.file-commission-api-bonusdist').html('');
          })

          $('#bonusdist_period_select').change(function(e){
              var insurance = $('#bonusdist_insurance_select').val();
              var period = $(this).val();

              var data = $('.bonusdist_form').serialize();

              $.ajax({
                  url: url_api+'bonuscalc/listcommission',
                  type: 'GET',
                  data: data,
                  dataType: 'json',
                  beforeSend: function(xhr) {
                    xhr.setRequestHeader("Authorization", "Basic " + btoa(usr + ":" + pwd));
                    xhr.setRequestHeader("commandid", "List Commission");
                    xhr.setRequestHeader("requestid", "listcommission" + Date.now());
                    xhr.setRequestHeader("requestdt", datetime);
                    xhr.setRequestHeader("clientid", insurance_username);
                    xhr.setRequestHeader("signature", signature);
                  },
                  success: function (res, textStatus, xhr) {
    				  $('.file-commission-api-bonusdist').html('');
                      if(res.body.data.length == 0){
                          if(lang == 2){
                              var res_fc = "Berkas komisi untuk asuransi dan periode tersebut tidak ada";
                          }else{
                              var res_fc = "File comission not found for this insurance and period";
                          }

                          $('.file-commission-api-bonusdist').append(`
                             <p>${res_fc}</p>
                          `);
                      }else{
                          res.body.data.map(function(value) {
                              $('.file-commission-api-bonusdist').append(`
                                  <div class="checkbox">
                                    <label>
                                      <input type="checkbox" value="${value.id}" name="commission_file[]">
                                    </label>
                                    ${value.file}
                                  </div>
                              `);
                          })
                      }
                  },
                  error: function (xhr, textStatus, errorThrown) {

                  }
              });
          })

      }

      function table_get_bonusdist(data) {
          $('.bonusdist-api-list').html('')
          $.ajax({
              url: url_api+'bonuscalc',
              type: 'GET',
	          data: data,
              dataType: 'json',
              beforeSend: function(xhr) {
                xhr.setRequestHeader("Authorization", "Basic " + btoa(usr + ":" + pwd));
                xhr.setRequestHeader("commandid", "bonusdist");
                xhr.setRequestHeader("requestid", "bonusdist" + Date.now());
                xhr.setRequestHeader("requestdt", datetime);
                xhr.setRequestHeader("clientid", insurance_username);
                xhr.setRequestHeader("signature", signature);
              },
              success: function (res, textStatus, xhr) {
                  res.body.data.map(function(value) {
                      var process_button='';
                      var delete_button='';
                      var detail_summary_button='';

                      if(value.gen_process == 9){
                        process_button =`<button
                            type="button"
                            id="button-${value.gen_id}"
                            class="btn btn-warning bonusdist-btn-edit"
                            onclick="process('${value.gen_id}')"
                            >
                              <i class="fas fa-play"></i>
                            </button>`;
                      }

                      if(value.gen_process == 11){
						detail_summary_button = `<a href="detailsummary.html" class="trigger-side-menu menu-detailsummary btn btn-success" side-attr-id="${value.id}" side-menu-active="bonusdist.html">
							<i class="fas fa-list"></i><span class="title-detailsummary" attr-id="${value.id}" style="display:none;">Summary Detail - ${value.id}</span>
						  </a>`;
                      }

                      if(value.gen_process == 9 || value.gen_process == 12){
                          delete_button = `<button
                                              type="button"
                                              class="btn btn-danger bonusdist-btn-delete"
                                              attr-id="${value.gen_id}"
                                              >
                                              <i class="fas fa-trash-alt"></i>
                                           </button>`;
                      }

			          var detail_button=`<a href="detailcalc.html" class="trigger-side-menu menu-detailcalc btn btn-primary" side-attr-id="${value.gen_id}" side-menu-active="bonusdist.html">
                        <i class="fas fa-list"></i><span class="title-detailcalc" attr-id="${value.gen_id}" style="display:none;">Bonus Calculation Detail - ${value.gen_id}</span>
                      </a>`;

                      $('.bonusdist-api-list').append(`<tr>
                                                  <td>${value.gen_id}</td>
                                                  <td>${value.caller_name}</td>
                                                  <td>${value.gen_period}</td>
                                                  <td>${value.count_fc_id}</td>
                                                  <td>${value.count_product}</td>
                                                  <td>${value.count_k_link_member_id}</td>
                                                  <td align="right">${Number(value.sum_ins_premium).toLocaleString('id')}</td>
                                                  <td align="right">${Number(value.sum_ins_bonus_personal).toLocaleString('id')}</td>
                                                  <td align="right">${Number(value.sum_ins_gross_commission).toLocaleString('id')}</td>
                                                  <td align="right">${value.process_name}</td>
                                                  <td>
                                                      ${process_button}
                                                      ${detail_button}
                                                      ${detail_summary_button}
                                                      ${delete_button}
                                                  </td>
                                                  </tr>`)
                  });
                  total_page_bonusdist = res.body.pagination.total_page;
                  paging_ajax("bonusdist", res);
                  delete_data_bonusdist();
                  tax_data_bonusdist();
                  load_side_menu();
              },
              error: function (xhr, textStatus, errorThrown) {

              }
          }).done(function() {
              if(lang == 2){
                  // $('.th-bonusdist-id').text('ID');
                  $('.th-bonusdist-ins').text('Asuransi');
                  $('.th-bonusdist-period').text('Periode');
                  // $('.th-bonusdist-cfc').text('ID');
                  // $('.th-bonusdist-ct').text('ID');
                  // $('.th-bonusdist-ck').text('ID');
                  // $('.th-bonusdist-tbp').text('ID');
                  // $('.th-bonusdist-tp').text('ID');
                  // $('.th-bonusdist-tgc').text('ID');
                  // $('.th-bonusdist-ebr').text('ID');
                  // $('.th-bonusdist-ebn').text('ID');
                  // $('.th-bonusdist-ec').text('ID');
                  $('.th-bonusdist-status').text('Status');
                  $('.th-bonusdist-act').text('Aksi');
              }
          });
      }

	     function process(id){
              $("#button-"+id).hide();
              $.ajax({
              url: url_api+'bonuscalc/process/'+id,
              type: 'GET',
              dataType: 'json',
              beforeSend: function(xhr) {
                xhr.setRequestHeader("Authorization", "Basic " + btoa(usr + ":" + pwd));
                xhr.setRequestHeader("commandid", "bonusdist");
                xhr.setRequestHeader("requestid", "bonusdist" + Date.now());
                xhr.setRequestHeader("requestdt", datetime);
                xhr.setRequestHeader("clientid", insurance_username);
                xhr.setRequestHeader("signature", signature);
              },
              success: function (res, textStatus, xhr) {
                  if(res.body.isinsert){
                      table_get_bonusdist();
                      toastr.success("Success Process!", 'Success!');
                  }else{
                      var ret_message = "";
                      $.each(res.body.message, function( index, value ) {
                          ret_message += "<br>" + index + ": " + value;
                      });
                      toastr.error(ret_message, 'Error!');
                      $("#button-"+id).show();
                  }
              },
              error: function (xhr, textStatus, errorThrown) {

              }
          });

	}

      function save_data_bonusdist() {
          var options = {
              url: url_api+'bonuscalc',
              type: 'POST',
              beforeSend: function(xhr) {
                  xhr.setRequestHeader("Authorization", "Basic " + btoa(usr + ":" + pwd));
                  xhr.setRequestHeader("commandid", "bonusdist");
                  xhr.setRequestHeader("requestid", "bonusdist" + Date.now());
                  xhr.setRequestHeader("requestdt", datetime);
                  xhr.setRequestHeader("clientid", insurance_username);
                  xhr.setRequestHeader("signature", signature);
              },
              success: function (data, textStatus, xhr) {
                  if(data.body.isinsert){
                      $('#bonusdistmodalcreate').modal('toggle');
                      table_get_bonusdist();
                      toastr.success("Success Saved!", 'Success!');
                  }else{
                      var ret_message = "";
                      $.each(data.body.message, function( index, value ) {
                          ret_message += "<br>" + index + ": " + value;
                      });
                      toastr.error(ret_message, 'Error!');
                  }
              }
          };

          $('.bonusdist_form').submit(function() {
              // inside event callbacks 'this' is the DOM element so we first
              // wrap it in a jQuery object and then invoke ajaxSubmit
              $(this).ajaxSubmit(options);

              // !!! Important !!!
              // always return false to prevent standard browser submit and page navigation
              return false;
          });
      }

      function delete_data_bonusdist() {
          $('.bonusdist-btn-delete').unbind('click')
          $('.bonusdist-btn-delete').click(function() {
              var id_param = $(this).attr('attr-id');
              swal({
                  title: "Are you sure?",
                  text: "Are you sure for delete this data ?",
                  icon: "warning",
                  buttons: ["No","Yes"],
                  dangerMode: true
              }).then(function(isConfirm) {
                  if (isConfirm) {
                      $.ajax({
                          url: url_api+'bonuscalc/' + id_param,
                          type: 'DELETE',
                          dataType: 'json',
                          beforeSend: function(xhr) {
                            xhr.setRequestHeader("Authorization", "Basic " + btoa(usr + ":" + pwd));
                            xhr.setRequestHeader("commandid", "bonusdist");
                            xhr.setRequestHeader("requestid", "bonusdist" + Date.now());
                            xhr.setRequestHeader("requestdt", datetime);
                            xhr.setRequestHeader("clientid", insurance_username);
                            xhr.setRequestHeader("signature", signature);
                          },
                          success: function (data, textStatus, xhr) {
                              if(data.body.isdelete){
                                  toastr.success("Success Deleted!", 'Success!');
                                  table_get_bonusdist();
                              }else{
                                  var ret_message = "";
                                  $.each(data.body.message, function( index, value ) {
                                      ret_message += "<br>" + index + ": " + value;
                                  });
                                  toastr.error(ret_message, 'Error!');
                              }
                          },
                          error: function (xhr, textStatus, errorThrown) {

                          }
                      });
                  }
              })
          })
      }

      function tax_data_bonusdist() {
          $('.bonusdist-btn-tax').unbind('click');
          $('.bonusdist-btn-tax').click(function() {
              var id_param = $(this).attr('attr-id');
              var id_insurance = $(this).attr('insurance-id');
              var period = $(this).attr('attr-period');

              $('.bonusdist_id_tax').val(id_param);
              $('.bonusdist_period').val(period);

              $.ajax({
                  url: url_api+'tax/insurance/'+id_insurance,
                  type: 'GET',
                  dataType: 'json',
                  beforeSend: function(xhr) {
                    xhr.setRequestHeader("Authorization", "Basic " + btoa(usr + ":" + pwd));
                    xhr.setRequestHeader("commandid", "bonusdist");
                    xhr.setRequestHeader("requestid", "bonusdist" + Date.now());
                    xhr.setRequestHeader("requestdt", datetime);
                    xhr.setRequestHeader("clientid", insurance_username);
                    xhr.setRequestHeader("signature", signature);
                  },
                  success: function (res, textStatus, xhr) {
					  $('.tax-api-list-bonusdist').html('');
                      res.body.data.map(function(value) {
                          $('.tax-api-list-bonusdist').append(`<option value="${value.id}">${value.file_name}</option>`);
                      })
                      update_tax_data_bonusdist();
                  },
                  error: function (xhr, textStatus, errorThrown) {

                  }
              });
          })
      }

      function update_tax_data_bonusdist() {
          $('.bonusdist-btn-update-tax').unbind('click');
          $('.bonusdist_form_tax').unbind('serialize');
          $('.bonusdist-btn-update-tax').click(function(e) {
              e.preventDefault();
              var data = $('.bonusdist_form_tax').serialize();
              $.ajax({
                  url: url_api+'bonuscalc/tax',
                  type: 'POST',
                  dataType: 'json',
                  data:data,
                  beforeSend: function(xhr) {
                    xhr.setRequestHeader("Authorization", "Basic " + btoa(usr + ":" + pwd));
                    xhr.setRequestHeader("commandid", "bonusdist");
                    xhr.setRequestHeader("requestid", "bonusdist" + Date.now());
                    xhr.setRequestHeader("requestdt", datetime);
                    xhr.setRequestHeader("clientid", insurance_username);
                    xhr.setRequestHeader("signature", signature);
                  },
                  success: function (data, textStatus, xhr) {
                      if(data.body.isupdate){
                          $('#bonusdistmodaledit').modal('toggle');
                          table_get_bonusdist();
                          toastr.success("Success Updated!", 'Success!');
                      }else{
                          var ret_message = "";
                          $.each(data.body.message, function( index, value ) {
                              ret_message += "<br>" + index + ": " + value;
                          });
                          toastr.error(ret_message, 'Error!');
                      }
                  },
                  error: function (xhr, textStatus, errorThrown) {

                  }
              });
          })
      }

      function bonusdist_filter(){
          $('.bonusdist-btn-filter-exe').unbind('click');
          $('.bonusdist-btn-filter-exe').click(function(e){
              e.preventDefault();
              var data = $('.bonusdist_form_filter').serialize();
              table_get_bonusdist(data);
              $('#bonusdistmodalfilter').modal('toggle');
          });
      }
</script>
