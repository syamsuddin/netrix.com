<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="tab-content tabcontent-border p-20" id="myTabContent">
                    <div role="tabpanel" class="tab-pane fade show active" id="home5" aria-labelledby="home-tab">
                        <div class="row">
                            <div class="col-5 align-self-center">
                                <div class="d-flex align-items-center">
                                    <nav aria-label="breadcrumb">
                                        <ol class="breadcrumb">
                                            <li class="breadcrumb-item"><a href="#">Service</a></li>
                                            <li class="breadcrumb-item active" aria-current="page">Bonus Calculation</li>
                                        </ol>
                                    </nav>
                                </div>
                            </div>
                            <div class="col-7 align-self-center">
                                <div class="d-flex no-block justify-content-end align-items-center">
                                    <div class="">
                                        <button type="submit" class="btn btn-warning float-right" data-toggle="modal" data-target="#bonuscalcmodalfilter"> <i class="fas fa-filter"></i> Filter</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr />
                        <!-- Row -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive ">
                                    <button type="button" class="btn btn-success user-btn-new" data-toggle="modal" data-target="#bonuscalcmodalcreate">
                                        <i class="fas fa-bonuscalc-plus"></i> Create New
                                    </button>

                                    <table style="margin-top:7px;" id="table-bonuscalc" class="table table-bordered nowrap display">
                                        <thead>
                                            <tr>
                                                <th><strong>Month</strong></th>
                                                <th><strong>Total Plan</strong></th>
                                                <th><strong>Total Amount (USD)</strong></th>
                                                <th><strong>Total Amount (IDR)</strong></th>
                                                <th><strong>Convertion Rate</strong></th>
                                                <th><strong>Status</strong></th>
                                                <th><strong>Task Type</strong></th>
                                                <th><strong>Task Start Time</strong></th>
                                                <th><strong>Task End Time</strong></th>
                                                <th><strong>Success Rate</strong></th>
                                                <th width="10%"><strong>Actions</strong></th>
                                            </tr>
                                        </thead>
                                        <tbody class="bonuscalc-api-list">

                                        </tbody>
                                    </table>

                                    <div class="ikut-table-bonuscalc" style="display: none;">
                                        <form class="paging_form_bonuscalc" method="GET">
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <div class="total-data-get-bonuscalc form-control"></div>
                                                </div>

                                                <div class="col-md-9">
                                                    <ul class="pagination float-right">
                                                        <li>
                                                            <select class="form-control change-num-records-bonuscalc" name="item_per_page">
                                                                <option value="10">10</option>
                                                                <option value="1">1</option>
                                                                <option value="20">20</option>
                                                                <option value="50">50</option>
                                                                <option value="100">100</option>
                                                            </select>
                                                        </li>
                                                        <li style="margin-right: 15px;">
                                                            <div style="padding: .375rem .75rem;">
                                                                Records
                                                            </div>
                                                        </li>
                                                        <li class="footable-page-arrow page-item first-page-bonuscalc">
                                                            <a data-page="first" class="page-link link-first-page-bonuscalc">«</a>
                                                        </li>
                                                        <li class="footable-page-arrow page-item prev-page-bonuscalc" style="padding-right: .75rem;">
                                                            <a data-page="prev" class="page-link link-prev-page-bonuscalc">‹</a>
                                                        </li>
                                                        <li>
                                                            <input type="text" class="form-control this-page-get-bonuscalc" name="page" style="width: 40px;">
                                                        </li>
                                                        <li>
                                                            <div class="total-page-get-bonuscalc" style="padding: .375rem .75rem;"></div>
                                                        </li>
                                                        <li>
                                                            <btn type="button" class="btn btn-success go-page-bonuscalc">Go</btn>
                                                        </li>
                                                        <li class="footable-page-arrow page-item next-page-bonuscalc" style="padding-left: .75rem;">
                                                            <a data-page="next" class="page-link link-next-page-bonuscalc">›</a>
                                                        </li>
                                                        <li class="footable-page-arrow page-item last-page-bonuscalc">
                                                            <a data-page="last" class="page-link link-last-page-bonuscalc">»</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!--Create New Modal -->
                        <div class="modal fade" id="bonuscalcmodalcreate" tabindex="-1" role="dialog" aria-labelledby="createModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <form class="bonuscalc_form" method="POST">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="createModalLabel">
                      												<i class="fas fa-bonuscalc-plus m-r-10"></i> Create New User
                      											</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="input-group mb-3">
                                                <button type="button" class="btn btn-info">
                                                    <i class="ti-email text-white"></i>
                                                </button>
                                                <input type="text" class="form-control" placeholder="Enter Email" aria-label="name" name="bonuscalcname" />
                                            </div>
                                            <div class="input-group mb-3">
                                                <button type="button" class="btn btn-info">
                                                    <i class="ti-bonuscalc text-white"></i>
                                                </button>
                                                <input type="text" class="form-control" placeholder="Enter Full Name" aria-label="name" name="fullname" />
                                            </div>
                                            <div class="input-group mb-3">
                                                <button type="button" class="btn btn-info">
                                                    <i class="ti-mobile text-white"></i>
                                                </button>
                                                <input type="text" class="form-control" placeholder="Enter Phone" aria-label="name" name="phone" />
                                            </div>
                                            <div class="input-group mb-3">
                                                <button type="button" class="btn btn-info">
                                                    <i class="ti-shield text-white"></i>
                                                </button>
                                                <input type="password" class="form-control box-pwd" placeholder="Enter Password" aria-label="name" name="password" />
                                                <button class="btn btn-show-pwd" type="button"><i class="fas fa-eye"></i></button>
                                                <button class="btn btn-hide-pwd" type="button"><i class="fas fa-eye-slash"></i></button>
                                            </div>
                                            <div class="input-group mb-3">
                                                <button type="button" class="btn btn-info">
                                                    <i class="ti-key text-white"></i>
                                                </button>
                                                <select class="form-control bonuscalc-access-api-list" name="role_id">

                                                </select>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="submit" class="btn btn-success bonuscalc-btn-save">
                                                <i class="ti-save"></i> Save
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        <!--View Modal -->
                        <div class="modal fade" id="bonuscalcmodaledit" tabindex="-1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document" style="max-width: 900px;">
                                <div class="modal-content">
                                    <form class="bonuscalc_form_update" method="POST">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="editModalLabel">
                      											  <i class="fas fa-bonuscalc-plus m-r-10 title-bonuscalc"></i>
                      											</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <h5>Basic Information</h5>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="input-group mb-3">
                                                        <button type="button" class="btn btn-info">
                                                            Plan Name
                                                        </button>
                                                        <input type="hidden" class="form-control id_edit" placeholder="Enter Email" aria-label="name" name="bonuscalc_id" />
                                                        <input type="text" class="form-control plan_name_view" aria-label="name" name="plan_name_view" />
                                                    </div>
                                                    <div class="input-group mb-3">
                                                        <button type="button" class="btn btn-info">
                                                            Created By
                                                        </button>
                                                        <input type="text" class="form-control created_by_view" aria-label="name" name="created_by_view" />
                                                    </div>
                                                    <div class="input-group mb-3">
                                                        <button type="button" class="btn btn-info">
                                                            Status
                                                        </button>
                                                        <input type="text" class="form-control status_view" aria-label="name" name="status_view" />
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="input-group mb-3">
                                                        <button type="button" class="btn btn-info">
                                                            bonuscalc Type
                                                        </button>
                                                        <input type="text" class="form-control bonuscalc_type_view" aria-label="name" name="bonuscalc_type_view" />
                                                    </div>
                                                    <div class="input-group mb-3">
                                                        <button type="button" class="btn btn-info">
                                                            bonuscalc Source
                                                        </button>
                                                        <input type="text" class="form-control bonuscalc_source_view" aria-label="name" name="bonuscalc_source_view" />
                                                    </div>
                                                    <div class="input-group mb-3">
                                                        <button type="button" class="btn btn-info">
                                                            Data File
                                                        </button>
                                                        <input type="text" class="form-control data_file_view" aria-label="name" name="data_file_view" />
                                                    </div>
                                                </div>
                                            </div>
                                            <h5>Basic Information</h5>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="input-group mb-3">
                                                        <button type="button" class="btn btn-info">
                                                            Start Time
                                                        </button>
                                                        <input type="text" class="form-control start_time_view" aria-label="name" name="start_time_view" />
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="input-group mb-3">
                                                        <button type="button" class="btn btn-info">
                                                            End Time
                                                        </button>
                                                        <input type="text" class="form-control end_time_view" aria-label="name" name="end_time_view" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="table-responsive">
                                                <table style="margin-top:7px;" class="table table-bordered nowrap display">
                                                    <thead>
                                                        <tr>
                                                            <th colspan="2"><strong>Success Items</strong></th>
                                                            <th colspan="2"><strong>Failed Items</strong></th>
                                                            <th><strong>Processing Items</strong></th>
                                                            <th><strong>Waiting Items</strong></th>
                                                            <th colspan="2"><strong>Details</strong></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td><strong>Success Items</strong></td>
                                                            <td><strong>Success Items</strong></td>
                                                            <td><strong>Failed Items</strong></td>
                                                            <td><strong>Failed Items</strong></td>
                                                            <td><strong>Processing Items</strong></td>
                                                            <td><strong>Waiting Items</strong></td>
                                                            <td><strong>Details</strong></td>
                                                            <td><strong>Details</strong></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="modal-footer">

                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        <!--Filter Modal -->
                        <div class="modal fade" id="bonuscalcmodalfilter" tabindex="-1" role="dialog" aria-labelledby="filterModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <form class="bonuscalc_form_filter" method="GET">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="filterModalLabel"><i class="fas fa-filter m-r-10"></i> Choose the Filter</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="row pt-3">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Start Month</label>
                                                        <input type="text" class="form-control plan_name_filter" placeholder="Select Month Here" name="start_month_filter">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group has-danger">
                                                        <label class="control-label">End Month</label>
                                                        <select class="form-control custom-select plan_type_filter" placeholder="Select Month Here" name="end_month_filter">
                                                            <option value="">Select Plan Type Here</option>
                                                            <option value="01">01</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Task Type</label>
                                                        <input type="text" class="form-control" placeholder="Select Type here" name="task_type_filter">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group has-danger">
                                                        <label class="control-label">Status</label>
                                                        <select class="form-control custom-select" name="status_filter">
                                                            <option value="">Select Status Here</option>
                                                            <option value="INITIAL">INITIAL</option>
                                                            <option value="DUPLICATED">DUPLICATED</option>
                                                            <option value="CANCELLED">CANCELLED</option>
                                                            <option value="WAITING">WAITING</option>
                                                            <option value="PROCESSING">PROCESSING</option>
                                                            <option value="COMPLETED">COMPLETED</option>
                                                            <option value="FAILED">FAILED</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-success btn-submit-filter-bonuscalc"> <i class="fas fa-play"></i> Submit</button>
                                            <button type="button" class="btn btn-dark btn-reset-filter-bonuscalc"><i class="fas fa-undo"></i> Reset</button>
                                        </div>
                                    </form>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
	var this_page_bonuscalc = 1;
	var total_page_bonuscalc = 1;

  function bonuscalc_ready() {
      get_all_data();
      save_data();
      paging("bonuscalc");
  }

	function table_get_bonuscalc(data){
		$('.bonuscalc-api-list').html('')
        $.ajax({
            url: url_api + 'bonuscalc',
            type: 'GET',
			      data: data,
            dataType: 'json',
            beforeSend: function(xhr) {
              xhr.setRequestHeader("Authorization", "Basic " + btoa(usr + ":" + pwd));
              xhr.setRequestHeader("commandid", "Cek Login");
              xhr.setRequestHeader("requestid", "ceklogin" + Date.now());
              xhr.setRequestHeader("requestdt", datetime);
              xhr.setRequestHeader("clientid", insurance_username);
              xhr.setRequestHeader("signature", signature);
            },
            success: function(res, textStatus, xhr) {
                res.body.data.map(function(value) {
                    $('.bonuscalc-api-list').append(`<tr>
                                                    <td>${value.id}</td>
                                                    <td>${value.plan_type}</td>
                                                    <td>${value.plan_name}</td>
                                                    <td>${value.task_status}</td>
                                                    <td>${value.plan_channel}</td>
                                                    <td>${value.plan_channel}</td>
                                                    <td>${value.task_type}</td>
                                                    <td>${value.task_start_date}</td>
                                                    <td>${value.task_end_date}</td>
                                                    <td>${value.plan_name}</td>
                                                    <td>
                                                        <button type="button" class="btn btn-info bonuscalc-btn-view" data-original-title="view" attr-id="${value.id}" data-toggle="modal" data-target="#bonuscalcmodaledit"> <i class="fas fa-file-alt"></i></button>
                                                    </td>
                                                    </tr>`)
                });
				$('.total-data-get-bonuscalc').text("Total Data : " + res.body.pagination.total_data);
				$('.total-page-get-bonuscalc').text(" / " + res.body.pagination.total_page + " Page");
				$('.this-page-get-bonuscalc').val(res.body.pagination.this_page);
				$(".ikut-table-bonuscalc").show();
				// $(".ikut-table-bonuscalc").css("width", $("#table-bonuscalc").width()).show();

				this_page_bonuscalc = res.body.pagination.this_page;
				total_page_bonuscalc = res.body.pagination.total_page;

				if(res.body.pagination.this_page == 1){
					$('.first-page-bonuscalc').hide();
					$('.prev-page-bonuscalc').hide();
				}else{
					$('.first-page-bonuscalc').show();
					$('.prev-page-bonuscalc').show();
				}

				if(res.body.pagination.this_page == res.body.pagination.total_page){
					$('.next-page-bonuscalc').hide();
					$('.last-page-bonuscalc').hide();
				}else{
					$('.next-page-bonuscalc').show();
					$('.last-page-bonuscalc').show();
				}

				delete_data();
                edit_data();
            },
            error: function(xhr, textStatus, errorThrown) {

            }
        });
	}

    function get_all_data() {
        table_get_bonuscalc();
    }

    function save_data() {
        $('.bonuscalc-btn-save').unbind('click');
        $('.bonuscalc_form').unbind('serialize');
        $('.bonuscalc-btn-save').click(function(e) {
            e.preventDefault();
            var data = $('.bonuscalc_form').serialize();
            $.ajax({
                url: url_api + 'bonuscalc',
                type: 'POST',
                dataType: 'json',
                data: data,
                beforeSend: function(xhr) {
                    xhr.setRequestHeader("uuid", uuid)
                    xhr.setRequestHeader("token", localStorage.getItem('token'))
                },
                success: function(data, textStatus, xhr) {
                    if (data.error == 0) {
                        $('#bonuscalcmodalcreate').modal('toggle');
                        $('input[name="bonuscalc_name"]').val('')
                        get_all_data();
                        toastr.success("Success Saved!", 'Success!');
                    } else {
                        var ret_message = "";
                        $.each(data.message, function(index, value) {
                            ret_message += "<br>" + index + ": " + value;
                        });
                        toastr.error(ret_message, 'Error!');
                    }
                },
                error: function(xhr, textStatus, errorThrown) {

                }
            });
        })
    }

    function delete_data() {
        $('.bonuscalc-btn-delete').unbind('click')
        $('.bonuscalc-btn-delete').click(function() {
            var id_param = $(this).attr('attr-id');
            swal({
                title: "Are you sure?",
                text: "Are you sure for delete this data ?",
                icon: "warning",
                buttons: ["No", "Yes"],
                dangerMode: true
            }).then(function(isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        url: url_api + 'bonuscalc/' + id_param,
                        type: 'DELETE',
                        dataType: 'json',
                        beforeSend: function(xhr) {
                            xhr.setRequestHeader("uuid", uuid)
                            xhr.setRequestHeader("token", localStorage.getItem('token'))
                        },
                        success: function(data, textStatus, xhr) {
                            if (data.error == 0) {
                                get_all_data();
                            } else {
                                toastr.error(data.message, 'Error!');
                            }
                        },
                        error: function(xhr, textStatus, errorThrown) {

                        }
                    });
                }
            })
        })
    }

    function edit_data() {
        $('.bonuscalc-btn-view').unbind('click');
        $('.bonuscalc-btn-view').click(function() {
            $('.bonuscalc_id_edit').val('');
            $('.bonuscalc_name_edit').val('');
            var id_param = $(this).attr('attr-id');
            $.ajax({
                url: url_api + 'bonuscalc/' + id_param,
                type: 'GET',
                dataType: 'json',
                beforeSend: function(xhr) {
                    xhr.setRequestHeader("uuid", uuid)
                    xhr.setRequestHeader("token", localStorage.getItem('token'))
                },
                success: function(res, textStatus, xhr) {
                    $('.title-bonuscalc').text("View bonuscalc Detail - " + res.data[0].id);
                    $('.plan_name_view_bonuscalc').val(res.data[0].plan_name);
                    $('.bonuscalcname_edit_bonuscalc').val(res.data[0].bonuscalcname);
                    $('.fullname_edit_bonuscalc').val(res.data[0].fullname);
                    $('.phone_edit_bonuscalc').val(res.data[0].phone);
                    $('.role_id_edit_bonuscalc').val(res.data[0].role_id);
                    update_data();
                },
                error: function(xhr, textStatus, errorThrown) {

                }
            });
        })
    }

    function update_data() {
        $('.bonuscalc-btn-update').unbind('click');
        $('.bonuscalc_form_update').unbind('serialize');
        $('.bonuscalc-btn-update').click(function(e) {
            e.preventDefault();
            var data = $('.bonuscalc_form_update').serialize();
            $.ajax({
                url: url_api + 'bonuscalc/update',
                type: 'POST',
                dataType: 'json',
                data: data,
                beforeSend: function(xhr) {
                    xhr.setRequestHeader("uuid", uuid)
                    xhr.setRequestHeader("token", localStorage.getItem('token'))
                },
                success: function(data, textStatus, xhr) {
                    if (data.error == 0) {
                        $('#bonuscalcmodaledit').modal('toggle');
                        get_all_data();
                        toastr.success("Success Updated!", 'Success!');
                    } else {
                        var ret_message = "";
                        $.each(data.message, function(index, value) {
                            ret_message += "<br>" + index + ": " + value;
                        });
                        toastr.error(ret_message, 'Error!');
                    }
                },
                error: function(xhr, textStatus, errorThrown) {

                }
            });
        })
    }
</script>
