<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="tab-content tabcontent-border p-20" id="myTabContent">
                    <div role="tabpanel" class="tab-pane fade show active" id="home5" aria-labelledby="home-tab">
                        <div class="row">
                          <div class="col-5 align-self-center">
                              <div class="d-flex align-items-center">
                                  <nav aria-label="breadcrumb">
                                      <ol class="breadcrumb">
                                          <li class="breadcrumb-item">Tax File Result</li>
                                      </ol>
                                  </nav>
                              </div>
                          </div>
                        </div>
                        <hr />
                        <!-- Row -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table style="margin-top:7px;" id="file_export" class="table table-bordered nowrap display table-striped">
                                        <thead>
                                            <tr>
                                                <th><strong>gen_id</strong></th>
                                                <th><strong>k_link_member_id</strong></th>
                                                <th><strong>gross_income</strong></th>
                                                <th><strong>tax</strong></th>
                                                <th><strong>tax_percentage</strong></th>
                                                <th><strong>fti_status</strong></th>
                                                <th><strong>fti_error_id</strong></th>
                                                <th><strong>fti_error_message</strong></th>
                                            </tr>
                                        </thead>
                                        <tbody class="detailfiletax-api-list">

                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <div class="col-md-12" style="margin-top:7px;">
                                <form class="paging_form_detailfiletax" method="GET">
                                    <div class="ikut-table-detailfiletax row" style="display: none;">
                                        <div class="col-md-3">
                                            <div class="total-data-get-detailfiletax form-control"></div>
                                        </div>

                                        <div class="col-md-9">
                                            <ul class="pagination float-right">
                                                <li>
                                                    <select class="form-control change-num-records-detailfiletax" name="item_per_page">
                                                        <option value="10">10</option>
                                                        <option value="20">20</option>
                                                        <option value="50">50</option>
                                                        <option value="100">100</option>
                                                    </select>
                                                </li>
                                                <li style="margin-right: 15px;">
                                                    <div style="padding: .375rem .75rem;">
                                                        Records
                                                    </div>
                                                </li>
                                                <li class="footable-page-arrow page-item first-page-detailfiletax">
                                                    <a data-page="first" class="page-link link-first-page-detailfiletax">«</a>
                                                </li>
                                                <li class="footable-page-arrow page-item prev-page-detailfiletax" style="padding-right: .75rem;">
                                                    <a data-page="prev" class="page-link link-prev-page-detailfiletax">‹</a>
                                                </li>
                                                <li>
                                                    <input type="text" class="form-control this-page-get-detailfiletax" name="page" style="width: 40px;">
                                                </li>
                                                <li>
                                                    <div class="total-page-get-detailfiletax" style="padding: .375rem .75rem;"></div>
                                                </li>
                                                <li>
                                                    <btn type="button" class="btn btn-success go-page-detailfiletax">Go</btn>
                                                </li>
                                                <li class="footable-page-arrow page-item next-page-detailfiletax" style="padding-left: .75rem;">
                                                    <a data-page="next" class="page-link link-next-page-detailfiletax">›</a>
                                                </li>
                                                <li class="footable-page-arrow page-item last-page-detailfiletax">
                                                    <a data-page="last" class="page-link link-last-page-detailfiletax">»</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                        <!--Filter Modal -->
                        <div class="modal fade" id="detailfiletaxmodalfilter" tabindex="-1" role="dialog" aria-labelledby="filterModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <form class="detailfiletax_form_filter" method="POST">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="filterModalLabel">
                                              <i class="fas fa-insurancefile-plus m-r-10"></i> Filter
                                            </h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" style="min-width: 125px;">Date Range</span>
                                                </div>
                                                <input type='text' class="form-control datetime-filter-insurancefile" name="date" value=""/>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <div class="col-md-6">
                                                <button class="btn btn-warning insurancefile-btn-reset-filter">
                                                    <i class="ti-loop"></i> Reset
                                                </button>
                                            </div>

                                            <div class="col-md-6" align="right">
                                                <button type="submit" class="btn btn-success insurancefile-btn-filter-exe">
                                                    <i class="ti-save"></i> Submit
                                                </button>
                                            </div>

                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
      var this_page_detailfiletax = 1;
      var total_page_detailfiletax = 1;
      var id_db_detailcom;
      var type_detailcom;

      function detailfiletax_ready(id,type_result) {
          var data;
          id_db_detailcom = id;
          type_detailcom = type_result;
          table_get_detailfiletax(data,id,type_result);
          paging("detailfiletax");
      }

      function table_get_detailfiletax(data,id,type_result) {
          if(id == undefined){
              id = id_db_detailcom;
          }

          if(type_result == undefined){
              type_result = type_detailcom;
          }

          $('.detailfiletax-api-list').html('')
          $.ajax({
              url: url_api+'tax/detail/'+id+'/'+type_result,
              type: 'GET',
              data: data,
              dataType: 'json',
              beforeSend: function(xhr) {
                xhr.setRequestHeader("Authorization", "Basic " + btoa(usr + ":" + pwd));
                xhr.setRequestHeader("commandid", "detailfiletax");
                xhr.setRequestHeader("requestid", "detailfiletax" + Date.now());
                xhr.setRequestHeader("requestdt", datetime);
                xhr.setRequestHeader("clientid", insurance_username);
                xhr.setRequestHeader("signature", signature);
              },
              success: function (res, textStatus, xhr) {
                  res.body.data.map(function(value) {
                      if(value.status == 1){
                          value.status = "Initail";
                      }else if(value.status == 2){
                          value.status = "Completed";
                      }else if(value.status == 3){
                          value.status = "Failed";
                      }else if(value.status == 4){
                          value.status = "Duplicated";
                      }

                      $('.detailfiletax-api-list').append(`<tr>
                                                  <td>${value.gen_id}</td>
                                                  <td>${value.k_link_member_id}</td>
                                                  <td align="right">${Number(value.gross_income).toLocaleString('id')}</td>
                                                  <td align="right">${Number(value.tax).toLocaleString('id')}</td>
                                                  <td align="right">${Number(value.tax_percentage).toLocaleString('id')}</td>
                                                  <td>${value.fti_status}</td>
                                                  <td>${value.fti_error_id}</td>
                                                  <td>${value.fti_error_message}</td>
                                                  </tr>`)
                  });
                  total_page_detailfiletax = res.body.pagination.total_page;
                  paging_ajax("detailfiletax", res);
                  load_side_menu();
              },
              error: function (xhr, textStatus, errorThrown) {

              }
          });
      }
</script>
