<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="tab-content tabcontent-border p-20" id="myTabContent">
                    <div role="tabpanel" class="tab-pane fade show active" id="home5" aria-labelledby="home-tab">
                        <div class="row">
                          <div class="col-5 align-self-center">
                              <div class="d-flex align-items-center">
                                  <nav aria-label="breadcrumb">
                                      <ol class="breadcrumb">
                                          <li class="breadcrumb-item sub-tax">Tax</li>
                                      </ol>
                                  </nav>
                              </div>
                          </div>
                        </div>
                        <hr />
                        <!-- Row -->
                        <div class="row">
                            <div class="col-md-3">
                                <button type="button" class="btn btn-success tax-btn-new sub-create-new" data-toggle="modal" data-target="#taxmodalcreate">
                                    Create New
                                </button>
                            </div>

                            <div class="col-md-9" style="text-align: right;">
                                <button type="button" class="btn btn-warning tax-btn-filter sub-filter" data-toggle="modal" data-target="#taxmodalfilter">
                                    Filter
                                </button>
                                <button type="button" class="btn btn-info tax-btn-refresh sub-refresh" onclick="refresh_data('tax')">
                                    Refresh
                                </button>
                            </div>

                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table style="margin-top:7px;" id="table-tax" class="table table-bordered nowrap display table-striped">
                                        <thead>
                                            <tr>
                                                <th><strong class="th-tax-id">ID</strong></th>
                                                <th><strong class="th-tax-period">PERIOD</strong></th>
                                                <th><strong class="th-tax-payment">PAYMENTID</strong></th>
                                                <th><strong class="th-tax-file">FILENAME</strong></th>
                                                <th><strong class="th-tax-channel">CHANNEL</strong></th>
                                                <th><strong class="th-tax-save">SAVETIME</strong></th>
                                                <th><strong class="th-tax-status">PROCESS</strong></th>
                                                <th><strong class="th-tax-act">ACTION</strong></th>
                                            </tr>
                                        </thead>
                                        <tbody class="tax-api-list">

                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <div class="col-md-12" style="margin-top:7px;">
                                <form class="paging_form_tax" method="GET">
                                    <div class="ikut-table-tax row" style="display: none;">
                                        <div class="col-md-3">
                                            <div class="total-data-get-tax form-control"></div>
                                        </div>

                                        <div class="col-md-9">
                                            <ul class="pagination float-right">
                                                <li>
                                                    <select class="form-control change-num-records-tax" name="item_per_page">
                                                        <option value="10">10</option>
                                                        <option value="20">20</option>
                                                        <option value="50">50</option>
                                                        <option value="100">100</option>
                                                    </select>
                                                </li>
                                                <li style="margin-right: 15px;">
                                                    <div style="padding: .375rem .75rem;" class="sub-records">
                                                        Records
                                                    </div>
                                                </li>
                                                <li class="footable-page-arrow page-item first-page-tax">
                                                    <a data-page="first" class="page-link link-first-page-tax">«</a>
                                                </li>
                                                <li class="footable-page-arrow page-item prev-page-tax" style="padding-right: .75rem;">
                                                    <a data-page="prev" class="page-link link-prev-page-tax">‹</a>
                                                </li>
                                                <li>
                                                    <input type="text" class="form-control this-page-get-tax" name="page" style="width: 40px;">
                                                </li>
                                                <li>
                                                    <div class="total-page-get-tax" style="padding: .375rem .75rem;"></div>
                                                </li>
                                                <li>
                                                    <btn type="button" class="btn btn-success go-page-tax sub-go">Go</btn>
                                                </li>
                                                <li class="footable-page-arrow page-item next-page-tax" style="padding-left: .75rem;">
                                                    <a data-page="next" class="page-link link-next-page-tax">›</a>
                                                </li>
                                                <li class="footable-page-arrow page-item last-page-tax">
                                                    <a data-page="last" class="page-link link-last-page-tax">»</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                        <!--Create New Modal -->
                        <div class="modal fade" id="taxmodalcreate" tabindex="-1" role="dialog" aria-labelledby="createModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <form class="tax_form" method="POST" enctype="multipart/form-data">
                                        <div class="modal-header">
                                            <h5 class="modal-title sub-upload-csv" id="createModalLabel">
                                              Upload File CSV
                                            </h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text sub-label-ins" style="min-width: 125px;">Insurance</span>
                                                </div>
                                                <select class="form-control insurance-api-list-tax" name="tax_insurance">

                                                </select>
                                            </div>
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text sub-label-file-upload" style="min-width: 125px;">File Upload</span>
                                                </div>
                                                <input type="file" class="form-control" placeholder="Select file CSV" aria-label="name" name="tax_file" id="tax-file" />
                                            </div>
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text sub-label-file-period" style="min-width: 125px;">File Period</span>
                                                </div>
                                                <input class="form-control" type="month" name="tax_period" min="2018-03">
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="submit" class="btn btn-success tax-btn-save sub-save">
                                                Save
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        <!--Filter Modal -->
                        <div class="modal fade" id="taxmodalfilter" tabindex="-1" role="dialog" aria-labelledby="filterModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <form class="tax_form_filter" method="POST">
                                        <div class="modal-header">
                                            <h5 class="modal-title sub-filter" id="filterModalLabel">
                                              Filter
                                            </h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text sub-label-ins" style="min-width: 125px;">Insurance</span>
                                                </div>
                                                <select class="form-control insurance-api-list-tax" name="caller">
                                                    <option>ALL</option>
                                                </select>
                                            </div>
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text sub-label-period" style="min-width: 125px;">Period</span>
                                                </div>
                                                <input type='text' class="form-control datetime-filter-tax" name="date" value=""/>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button class="btn btn-warning tax-btn-reset-filter sub-reset">
                                                Reset
                                            </button>

                                            <button type="submit" class="btn btn-success tax-btn-filter-exe sub-submit">
                                                Submit
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
      var this_page_tax = 1;
      var total_page_tax = 1;

    	$('.datetime-filter-tax').daterangepicker({
    		timePicker: false,
    		timePickerIncrement: 30,
            timePicker24Hour: true,
            autoUpdateInput: true,
    		locale: {
    			format: 'DD-MM-YYYY'
    		}
    	});

        $('.datetime-filter-tax').val('');

      function tax_ready() {
          table_get_tax();
          save_data_tax();
          tax_filter();
          paging("tax");

          $('.tax-btn-new').unbind('click')
          $('.tax-btn-new').click(function() {
              $('.tax_form').trigger("reset");
          })

          $('.tax-btn-reset-filter').unbind('click')
          $('.tax-btn-reset-filter').click(function(e) {
              e.preventDefault();
              $('.tax_form_filter').trigger("reset");
              $('#taxmodalfilter').modal('toggle');
              var data = $('.tax_form_filter').serialize();
              table_get_tax(data);
          })

          $.ajax({
              url: url_api+'insurance',
              type: 'GET',
              dataType: 'json',
              beforeSend: function(xhr) {
                xhr.setRequestHeader("Authorization", "Basic " + btoa(usr + ":" + pwd));
                xhr.setRequestHeader("commandid", "insurance List");
                xhr.setRequestHeader("requestid", "insurancelist" + Date.now());
                xhr.setRequestHeader("requestdt", datetime);
                xhr.setRequestHeader("clientid", insurance_username);
                xhr.setRequestHeader("signature", signature);
              },
              success: function (res, textStatus, xhr) {
                  res.body.data.map(function(value) {
                      $('.insurance-api-list-tax').append(`<option value="${value.id}">${value.name}</option>`);
                  })
              },
              error: function (xhr, textStatus, errorThrown) {

              }
          });
      }

      function table_get_tax(data) {
          $('.tax-api-list').html('');
          $.ajax({
              url: url_api+'tax',
              type: 'GET',
			  data: data,
              dataType: 'json',
              beforeSend: function(xhr) {
                xhr.setRequestHeader("Authorization", "Basic " + btoa(usr + ":" + pwd));
                xhr.setRequestHeader("commandid", "tax");
                xhr.setRequestHeader("requestid", "tax" + Date.now());
                xhr.setRequestHeader("requestdt", datetime);
                xhr.setRequestHeader("clientid", insurance_username);
                xhr.setRequestHeader("signature", signature);
              },
              success: function (res, textStatus, xhr) {
				  $('.tax-api-list').html('');
                  res.body.data.map(function(value) {
                      var detail_button = "";
                      var download_button = "";
                      var delete_button = "";
                      var sp_button = "";

                      if(value.status > 13){
                        detail_button =`<a href="detailtax.html" class="trigger-side-menu menu-detailtax btn btn-primary" side-attr-id="${value.id}"  side-menu-active="tax.html">
                                          <i class="fas fa-list"></i><span class="title-detailtax" attr-id="${value.id}" style="display:none;">Detail Tax File (id : ${value.id})</span>
                                        </a>
                                        <a href="` + url_api + `uploads/tax_success/${value.file_name}" target="_BLANK">
                                          <button type="button" class="btn btn-success tax-btn-download" attr-id="${value.id}">
                                            <i class="fas fa-download"></i>
                                          </button>
                                        </a>
                                        <button
                                            type="button"
                                            class="btn btn-danger tax-btn-delete"
                                            attr-id="${value.id}"
                                            attr-file-name="${value.file_name}"
                                            attr-type="${value.status}"
                                            >
                                            <i class="fas fa-trash-alt"></i>
                                        </button>`;
                      }

                      if(value.status == 16){
                          sp_button = `<button type="button" class="btn btn-warning tax-btn-sp" attr-id="${value.id}">
                                        <i class="fas fa-play"></i>
                                       </button>`;
                      }

                      $('.tax-api-list').append(`<tr>
                                                  <td>${value.id}</td>
                                                  <td>${value.period}</td>
                                                  <td>${value.insurance_name}</td>
                                                  <td>${value.file_name}</td>
                                                  <td>${value.channel}</td>
                                                  <td>${value.save_time}</td>
                                                  <td>${value.process_name}</td>
                                                  <td>
                                                      ${detail_button}
                                                  </td>
                                                  </tr>`)
                  });
                  total_page_tax = res.body.pagination.total_page;
                  paging_ajax("tax", res);
                  delete_data_tax();
                  sp_tax();
                  load_side_menu();
              },
              error: function (xhr, textStatus, errorThrown) {

              }
          }).done(function() {
              if(lang == 2){
                  $('.th-tax-id').text('ID');
                  $('.th-tax-ins').text('Asuransi');
                  $('.th-tax-file').text('Nama Berkas');
                  $('.th-tax-period').text('Periode');
                  $('.th-tax-save').text('Waktu Simpan');
                  $('.th-tax-status').text('Status');
                  $('.th-tax-act').text('Aksi');
              }
          });
      }

      function save_data_tax() {
          var options = {
              url: url_api+'tax',
              type: 'POST',
              beforeSend: function(xhr) {
                  xhr.setRequestHeader("Authorization", "Basic " + btoa(usr + ":" + pwd));
                  xhr.setRequestHeader("commandid", "Agent Bonus Tax File Details");
                  xhr.setRequestHeader("requestid", "tax" + Date.now());
                  xhr.setRequestHeader("requestdt", datetime);
                  xhr.setRequestHeader("requesturl", "http://localhost/netrix.com/receiver.php");
                  xhr.setRequestHeader("clientid", insurance_username);
                  xhr.setRequestHeader("signature", signature);
              },
              success: function (data, textStatus, xhr) {
                  if(data.body.isrecevied){
                      $('#taxmodalcreate').modal('toggle');
                      table_get_tax();
                      toastr.success("Success Saved!", 'Success!');
                  }else{
                      var ret_message = "";
                      $.each(data.body.message, function( index, value ) {
                          ret_message += "<br>" + index + ": " + value;
                      });
                      toastr.error(ret_message, 'Error!');
                  }
              }
          };

          $('.tax_form').submit(function() {
              // inside event callbacks 'this' is the DOM element so we first
              // wrap it in a jQuery object and then invoke ajaxSubmit
              $(this).ajaxSubmit(options);

              // !!! Important !!!
              // always return false to prevent standard browser submit and page navigation
              return false;
          });
      }

      function delete_data_tax() {
          $('.tax-btn-delete').unbind('click')
          $('.tax-btn-delete').click(function() {
              var id_param = $(this).attr('attr-id');
              var file_name = $(this).attr('attr-file-name');
              var type = $(this).attr('attr-type');
              var encodedData = window.btoa(file_name);
              swal({
                  title: "Are you sure?",
                  text: "Are you sure for delete this data ?",
                  icon: "warning",
                  buttons: ["No","Yes"],
                  dangerMode: true
              }).then(function(isConfirm) {
                  if (isConfirm) {
                      $.ajax({
                          url: url_api+'tax/' + id_param + '/' + encodedData + "/" + type,
                          type: 'DELETE',
                          dataType: 'json',
                          beforeSend: function(xhr) {
                            xhr.setRequestHeader("Authorization", "Basic " + btoa(usr + ":" + pwd));
                            xhr.setRequestHeader("commandid", "tax");
                            xhr.setRequestHeader("requestid", "tax" + Date.now());
                            xhr.setRequestHeader("requestdt", datetime);
                            xhr.setRequestHeader("clientid", insurance_username);
                            xhr.setRequestHeader("signature", signature);
                          },
                          success: function (data, textStatus, xhr) {
                              if(data.body.isdelete){
                                  table_get_tax();
                                  toastr.success("Success Deleted!", 'Success!');
                              }else{
                                  var ret_message = "";
                                  $.each(data.body.message, function( index, value ) {
                                      ret_message += "<br>" + index + ": " + value;
                                  });
                                  toastr.error(ret_message, 'Error!');
                              }
                          },
                          error: function (xhr, textStatus, errorThrown) {

                          }
                      });
                  }
              })
          })
      }

      function sp_tax() {
          $('.tax-btn-sp').unbind('click')
          $('.tax-btn-sp').click(function() {
              var id_param = $(this).attr('attr-id');
              $.ajax({
                  url: url_api+'tax/sp/' + id_param,
                  type: 'GET',
                  dataType: 'json',
                  beforeSend: function(xhr) {
                    xhr.setRequestHeader("Authorization", "Basic " + btoa(usr + ":" + pwd));
                    xhr.setRequestHeader("commandid", "tax_sp");
                    xhr.setRequestHeader("requestid", "tax_sp_" + Date.now());
                    xhr.setRequestHeader("requestdt", datetime);
                    xhr.setRequestHeader("clientid", insurance_username);
                    xhr.setRequestHeader("signature", signature);
                  },
                  success: function (data, textStatus, xhr) {
                      if(data.body.issuccess){
                          table_get_tax();
                          toastr.success("Success Process!", 'Success!');
                      }else{
                          var ret_message = "";
                          $.each(data.body.message, function( index, value ) {
                              ret_message += "<br>" + index + ": " + value;
                          });
                          toastr.error(ret_message, 'Error!');
                      }
                  },
                  error: function (xhr, textStatus, errorThrown) {

                  }
              });
          });
      }

      function tax_filter(){
          $('.tax-btn-filter-exe').unbind('click');
          $('.tax-btn-filter-exe').click(function(e){
              e.preventDefault();
              var data = $('.tax_form_filter').serialize();
              table_get_tax(data);
              $('#taxmodalfilter').modal('toggle');
          });
      }
</script>
