<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="tab-content tabcontent-border p-20" id="myTabContent">
                    <div role="tabpanel" class="tab-pane fade show active" id="home5" aria-labelledby="home-tab">
                        <div class="row">
                          <div class="col-5 align-self-center">
                              <div class="d-flex align-items-center">
                                  <nav aria-label="breadcrumb">
                                      <ol class="breadcrumb">
                                          <li class="breadcrumb-item">Bonus Calculation Detail</li>
                                      </ol>
                                  </nav>
                              </div>
                          </div>
                        </div>
                        <hr />
                        <!-- Row -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table style="margin-top:7px;" class="table table-bordered nowrap display table-striped">
                                        <thead>
											<tr>
												<th rowspan="2" align="center"><strong>K-Link Member ID</strong></th>
												<th colspan="3" align="center"><strong>File Commission</strong></th>
												<th colspan="4" align="center"><strong>Middleware Calculation</strong></th>
											</tr>
                                            <tr>
                                                <th align="center"><strong>Total Premium</strong></th>
                                                <th align="center"><strong>Bonus Personal</strong></th>
                                                <th align="center"><strong>Total Gross Commission</strong></th>
                                                <th align="center"><strong>Activion Bonus</strong></th>
                                                <th align="center"><strong>Network Bonus</strong></th>
                                                <th align="center"><strong>K-Link Feebase</strong></th>
                                                <th align="center"><strong>Level</strong></th>
                                            </tr>
                                        </thead>
                                        <tbody class="detailcalc-api-list">

                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <div class="col-md-12" style="margin-top:7px;">
                                <form class="paging_form_detailcalc" method="GET">
                                    <div class="ikut-table-detailcalc row" style="display: none;">
                                        <div class="col-md-3">
                                            <div class="total-data-get-detailcalc form-control"></div>
                                        </div>

                                        <div class="col-md-9">
                                            <ul class="pagination float-right">
                                                <li>
                                                    <select class="form-control change-num-records-detailcalc" name="item_per_page">
                                                        <option value="10">10</option>
                                                        <option value="20">20</option>
                                                        <option value="50">50</option>
                                                        <option value="100">100</option>
                                                    </select>
                                                </li>
                                                <li style="margin-right: 15px;">
                                                    <div style="padding: .375rem .75rem;">
                                                        Records
                                                    </div>
                                                </li>
                                                <li class="footable-page-arrow page-item first-page-detailcalc">
                                                    <a data-page="first" class="page-link link-first-page-detailcalc">«</a>
                                                </li>
                                                <li class="footable-page-arrow page-item prev-page-detailcalc" style="padding-right: .75rem;">
                                                    <a data-page="prev" class="page-link link-prev-page-detailcalc">‹</a>
                                                </li>
                                                <li>
                                                    <input type="text" class="form-control this-page-get-detailcalc" name="page" style="width: 40px;">
                                                </li>
                                                <li>
                                                    <div class="total-page-get-detailcalc" style="padding: .375rem .75rem;"></div>
                                                </li>
                                                <li>
                                                    <btn type="button" class="btn btn-success go-page-detailcalc">Go</btn>
                                                </li>
                                                <li class="footable-page-arrow page-item next-page-detailcalc" style="padding-left: .75rem;">
                                                    <a data-page="next" class="page-link link-next-page-detailcalc">›</a>
                                                </li>
                                                <li class="footable-page-arrow page-item last-page-detailcalc">
                                                    <a data-page="last" class="page-link link-last-page-detailcalc">»</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                        <!--Filter Modal -->
                        <div class="modal fade" id="detailcalcmodalfilter" tabindex="-1" role="dialog" aria-labelledby="filterModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <form class="detailcalc1_form_filter" method="POST">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="filterModalLabel">
                                              <i class="fas fa-insurancefile-plus m-r-10"></i> Filter
                                            </h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" style="min-width: 125px;">Date Range</span>
                                                </div>
                                                <input type='text' class="form-control datetime-filter-insurancefile" name="date" value=""/>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <div class="col-md-6">
                                                <button class="btn btn-warning insurancefile-btn-reset-filter">
                                                    <i class="ti-loop"></i> Reset
                                                </button>
                                            </div>

                                            <div class="col-md-6" align="right">
                                                <button type="submit" class="btn btn-success insurancefile-btn-filter-exe">
                                                    <i class="ti-save"></i> Submit
                                                </button>
                                            </div>

                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
      var this_page_detailcalc = 1;
      var total_page_detailcalc = 1;
      var id_db_detailcalc = '';

      function detailcalc_ready(id) {
          var data;
          id_db_detailcalc = id;
          table_get_detailcalc(data,id_db_detailcalc);
          paging("detailcalc");
      }

      function table_get_detailcalc(data,id) {
          if(id == undefined){
              id = id_db_detailcalc;
          }

          $('.detailcalc-api-list').html('');
          $.ajax({
              url: url_api+'bonuscalc/detail/'+id,
              type: 'GET',
              data: data,
              dataType: 'json',
              beforeSend: function(xhr) {
                xhr.setRequestHeader("Authorization", "Basic " + btoa(usr + ":" + pwd));
                xhr.setRequestHeader("commandid", "detailcalc");
                xhr.setRequestHeader("requestid", "detailcalc" + Date.now());
                xhr.setRequestHeader("requestdt", datetime);
                xhr.setRequestHeader("clientid", insurance_username);
                xhr.setRequestHeader("signature", signature);
              },
              success: function (res, textStatus, xhr) {
				  $('.detailcalc-api-list').html('');
                  res.body.data.map(function(value) {

					  var total_bonus = value.total_bonus_personal + value.sub_br + (value.sub_bn * value.remark) + value.sub_commission;
					  var excess = total_bonus - value.total_gross_commission;
					  var total_feebase = value.sub_commission + excess;

                      $('.detailcalc-api-list').append(`<tr>
                                                  <td>${value.k_link_member_id}</td>
                                                  <td align="right">${Number(value.ins_premium).toLocaleString('id')}</td>
                                                  <td align="right">${Number(value.ins_personal_bonus).toLocaleString('id')}</td>
                                                  <td align="right">${Number(value.ins_gross_commission).toLocaleString('id')}</td>
                                                  <td align="right">${Number(value.feebase_activation_dist).toLocaleString('id')}</td>
                                                  <td align="right">${Number(value.feebase_network_dist).toLocaleString('id')}</td>
                                                  <td align="right">${Number(value.feebase).toLocaleString('id')}</td>
                                                  <td align="right">${Number(value.ins_product).toLocaleString('id')}</td>
                                                  <!--td align="right">${Number(total_bonus).toLocaleString('id')}</td>
                                                  <td align="right">${Number(excess).toLocaleString('id')}</td>
                                                  <td align="right">${Number(total_feebase).toLocaleString('id')}</td-->
                                                  </tr>`)
                  });
                  total_page_detailcalc = res.body.pagination.total_page;
                  paging_ajax("detailcalc", res);
              },
              error: function (xhr, textStatus, errorThrown) {

              }
          });
      }
</script>
