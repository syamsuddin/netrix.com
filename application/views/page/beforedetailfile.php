<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="tab-content tabcontent-border p-20" id="myTabContent">
                    <div role="tabpanel" class="tab-pane fade show active" id="home5" aria-labelledby="home-tab">
                        <div class="row">
                          <div class="col-5 align-self-center">
                              <div class="d-flex align-items-center">
                                  <nav aria-label="breadcrumb">
                                      <ol class="breadcrumb">
                                          <li class="breadcrumb-item">Commission Detail</li>
                                      </ol>
                                  </nav>
                              </div>
                          </div>
                        </div>
                        <hr />
                        <!-- Row -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive">

                                    <table style="margin-top:7px;" id="file_export" class="table table-bordered nowrap display">

                                        <tr style="background-color: rgba(0,0,0,.05);">
                                            <th colspan="4"><strong class="title-before-detail"></strong></th>
                                        </tr>

                                    </table>

                                    <table style="margin-top:7px;" id="file_export" class="table table-bordered nowrap display">

                                        <tr style="background-color: rgba(0,0,0,.05);">
                                            <td colspan="4">Basic Information</td>
                                        </tr>

                                        <tr>
                                            <td>File Name</td>
                                            <td>
                                                <input type="text" class="form-control form-filename-before-detail">
                                            </td>
                                            <td>Save Time</td>
                                            <td>
                                                <input type="text" class="form-control form-savetime-before-detail">
                                            </td>
                                        </tr>

                                    </table>

                                    <table style="margin-top:7px;" id="file_export" class="table table-bordered nowrap display">

                                        <tr style="background-color: rgba(0,0,0,.05);">
                                            <td colspan="4">Execution Information</td>
                                        </tr>

                                        <tr>
                                            <td>Start Time</td>
                                            <td>
                                                <input type="text" class="form-control">
                                            </td>
                                            <td>End Time</td>
                                            <td>
                                                <input type="text" class="form-control">
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>Progress</td>
                                            <td colspan="3">

                                                <table style="margin-top:7px;" id="file_export" class="table table-bordered nowrap display">

                                                    <thead>
                                                        <tr>
                                                            <th colspan="2"><strong>Success Items</strong></th>
                                                            <th colspan="2"><strong>Failed Items</strong></th>
                                                            <!-- <th><strong>Processing Items</strong></th> -->
                                                            <!-- <th><strong>Waiting Items</strong></th> -->
                                                            <!-- <th colspan="2"><strong>Details</strong></th> -->
                                                        </tr>
                                                    </thead>

                                                    <!-- <tbody>
                                                        <tr>
                                                            <td style="vertical-align:middle" class="total-success-before-detail"></td>
                                                            <td style="vertical-align:middle" class="button-success-before-edit"></td>
                                                            <td style="vertical-align:middle" class="total-failed-before-detail"></td>
                                                            <td style="vertical-align:middle" class="button-failed-before-edit"></td>
                                                            <td style="vertical-align:middle">0</td>
                                                            <td style="vertical-align:middle">0</td>
                                                            <td style="vertical-align:middle">
                                                                <div class="progress" style="width:100px">
                                                                    <div class="progress-bar bg-success" style="width: 100%; height:6px;" role="progressbar"> <span class="sr-only">60% Complete</span> </div>
                                                                </div>
                                                            </td>
                                                            <td style="vertical-align:middle">53/53</td>
                                                        </tr>
                                                    </tbody> -->

                                                    <tbody>
                                                        <tr>
                                                            <td style="vertical-align:middle" class="total-success-before-detail"></td>
                                                            <td style="vertical-align:middle" class="button-success-before-edit"></td>
                                                            <td style="vertical-align:middle" class="total-failed-before-detail"></td>
                                                            <td style="vertical-align:middle" class="button-failed-before-edit"></td>
                                                        </tr>
                                                    </tbody>

                                                    <tbody>
                                                        <tr>
                                                            <td colspan="8">
                                                                <!-- <center>
                                                                    <btn type="button" class="btn btn-default ">Return </btn>
                                                                </center> -->
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
	function beforedetailfile_ready(id) {
    $.ajax({
        url: url_api+'insurancefile/'+id,
        type: 'GET',
        dataType: 'json',
        beforeSend: function(xhr) {
          xhr.setRequestHeader("Authorization", "Basic " + btoa(usr + ":" + pwd));
          xhr.setRequestHeader("commandid", "insurancefile");
          xhr.setRequestHeader("requestid", "insurancefile" + Date.now());
          xhr.setRequestHeader("requestdt", datetime);
          xhr.setRequestHeader("clientid", insurance_username);
          xhr.setRequestHeader("signature", signature);
        },
        success: function (res, textStatus, xhr) {
          $('.title-before-detail').text("View File Details " + res.body.data[0].id);
          $('.form-filename-before-detail').val(res.body.data[0].file_name);
          $('.form-savetime-before-detail').val(res.body.data[0].save_time);
          $('.button-success-before-edit').append(`<a href="detailfile.html" class="trigger-side-menu menu-detailfile btn btn-primary" side-attr-id="${res.body.data[0].id}">
                                                    <i class="fas fa-list"></i><span class="title-detailfile" attr-id="${res.body.data[0].id}" style="display:none;">Commission (id : ${res.body.data[0].id})</span>
                                                  </a>`);
          $('.button-failed-before-edit').append(`<a href="detailfile.html" class="trigger-side-menu menu-detailfile btn btn-primary" side-attr-id="${res.body.data[0].id}" side-attr-type="1">
                                                        <i class="fas fa-list"></i><span class="title-detailfile" attr-id="1" style="display:none;">${res.body.data[0].file_name}</span>
                                                  </a>`);
          load_side_menu();
        },
    });
    $.ajax({
        url: url_api+'insurancefile/sum/'+id+"/0",
        type: 'GET',
        dataType: 'json',
        beforeSend: function(xhr) {
          xhr.setRequestHeader("Authorization", "Basic " + btoa(usr + ":" + pwd));
          xhr.setRequestHeader("commandid", "insurancefile");
          xhr.setRequestHeader("requestid", "insurancefile" + Date.now());
          xhr.setRequestHeader("requestdt", datetime);
          xhr.setRequestHeader("clientid", insurance_username);
          xhr.setRequestHeader("signature", signature);
        },
        success: function (res, textStatus, xhr) {
          $('.total-success-before-detail').text(res.body.data[0].total);
        },
    });
    $.ajax({
        url: url_api+'insurancefile/sum/'+id+"/1",
        type: 'GET',
        dataType: 'json',
        beforeSend: function(xhr) {
          xhr.setRequestHeader("Authorization", "Basic " + btoa(usr + ":" + pwd));
          xhr.setRequestHeader("commandid", "insurancefile");
          xhr.setRequestHeader("requestid", "insurancefile" + Date.now());
          xhr.setRequestHeader("requestdt", datetime);
          xhr.setRequestHeader("clientid", insurance_username);
          xhr.setRequestHeader("signature", signature);
        },
        success: function (res, textStatus, xhr) {
          $('.total-failed-before-detail').text(res.body.data[0].total);
        },
    });
  }
</script>
