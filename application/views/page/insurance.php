<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="tab-content tabcontent-border p-20" id="myTabContent">
                    <div role="tabpanel" class="tab-pane fade show active" id="home5" aria-labelledby="home-tab">
                        <div class="row">
                          <div class="col-5 align-self-center">
                              <div class="d-flex align-items-center">
                                  <nav aria-label="breadcrumb">
                                      <ol class="breadcrumb">
                                          <li class="breadcrumb-item sub-insurance">Initiator Key Management</li>
                                      </ol>
                                  </nav>
                              </div>
                          </div>
                        </div>
                        <hr />
                        <!-- Row -->
                        <div class="row">
                            <div class="col-md-9">
                                <button type="button" class="btn btn-success insurance-btn-new sub-create-new" data-toggle="modal" data-target="#insurancemodalcreate">
                                    Create New
                                </button>
                            </div>

                            <div class="col-md-3" align="right">
                                <button type="button" class="btn btn-warning insurance-btn-filter sub-filter" data-toggle="modal" data-target="#insurancemodalfilter">
                                    Filter
                                </button>
                                <button type="button" class="btn btn-info insurance-btn-refresh sub-refresh" onclick="refresh_data('insurance')">
                                    Refresh
                                </button>
                            </div>

                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table style="margin-top:7px;" id="table-insurance" class="table table-bordered nowrap display table-striped">
                                        <thead>
                                            <tr>
                                                <th><strong>Name</strong></th>
                                                <th><strong>Auth Header</strong></th>
                                                <th><strong>Auth Password</strong></th>
                                                <th><strong>Key Secret</strong></th>
                                                <th><strong>Iv</strong></th>
                                                <th><strong>Bit</strong></th>
                                                <th><strong>Outbound</strong></th>
                                                <th><strong>Tax</strong></th>
                                                <th><strong>Type</strong></th>
                                                <th><strong>Status</strong></th>
                                                <th width="10%"><strong>Actions</strong></th>
                                            </tr>
                                        </thead>
                                        <tbody class="insurance-api-list">

                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <div class="col-md-12" style="margin-top:7px;">
                                <form class="paging_form_insurance" method="GET">
                                    <div class="ikut-table-insurance row" style="display: none;">
                                        <div class="col-md-3">
                                            <div class="total-data-get-insurance form-control"></div>
                                        </div>

                                        <div class="col-md-9">
                                            <ul class="pagination float-right">
                                                <li>
                                                    <select class="form-control change-num-records-insurance" name="item_per_page">
                                                        <option value="10">10</option>
                                                        <option value="20">20</option>
                                                        <option value="50">50</option>
                                                        <option value="100">100</option>
                                                    </select>
                                                </li>
                                                <li style="margin-right: 15px;">
                                                    <div style="padding: .375rem .75rem;" class="sub-records">
                                                        Records
                                                    </div>
                                                </li>
                                                <li class="footable-page-arrow page-item first-page-insurance">
                                                    <a data-page="first" class="page-link link-first-page-insurance">«</a>
                                                </li>
                                                <li class="footable-page-arrow page-item prev-page-insurance" style="padding-right: .75rem;">
                                                    <a data-page="prev" class="page-link link-prev-page-insurance">‹</a>
                                                </li>
                                                <li>
                                                    <input type="text" class="form-control this-page-get-insurance" name="page" style="width: 40px;">
                                                </li>
                                                <li>
                                                    <div class="total-page-get-insurance" style="padding: .375rem .75rem;"></div>
                                                </li>
                                                <li>
                                                    <btn type="button" class="btn btn-success go-page-insurance sub-go">Go</btn>
                                                </li>
                                                <li class="footable-page-arrow page-item next-page-insurance" style="padding-left: .75rem;">
                                                    <a data-page="next" class="page-link link-next-page-insurance">›</a>
                                                </li>
                                                <li class="footable-page-arrow page-item last-page-insurance">
                                                    <a data-page="last" class="page-link link-last-page-insurance">»</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                        <!--Create New Modal -->
                        <div class="modal fade" id="insurancemodalcreate" tabindex="-1" role="dialog" aria-labelledby="createModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <form class="insurance_form" method="POST">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="createModalLabel">
                                              Create New insurance
                                            </h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" style="min-width: 125px;">Name</span>
                                                </div>
                                                <input type="text" class="form-control" placeholder="Enter Insurance Name" aria-label="name" name="insurance_name" />
                                            </div>
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" style="min-width: 125px;">Auth Header</span>
                                                </div>
                                                <input type="text" class="form-control" placeholder="Enter Username" aria-label="name" name="insurance_username" />
                                            </div>
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" style="min-width: 125px;">Auth Password</span>
                                                </div>
                                                <input type="text" class="form-control box-pwd" placeholder="Enter Password" aria-label="name" name="insurance_password" />
                                                <!-- <button class="btn btn-show-pwd" type="button"><i class="fas fa-eye"></i></button> -->
                                                <!-- <button class="btn btn-hide-pwd" type="button"><i class="fas fa-eye-slash"></i></button> -->
                                            </div>
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" style="min-width: 125px;">Secret Key</span>
                                                </div>
                                                <input type="text" class="form-control" placeholder="Enter Insurance Secret Key" aria-label="name" name="insurance_secret_key_show" disabled />
                                                <input type="hidden" class="form-control" placeholder="Enter Insurance Secret Key" aria-label="name" name="insurance_secret_key" />
                                            </div>
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" style="min-width: 125px;">iv</span>
                                                </div>
                                                <input type="text" class="form-control" placeholder="Enter Insurance iv" aria-label="name" name="insurance_iv_show" disabled />
                                                <input type="hidden" class="form-control" placeholder="Enter Insurance iv" aria-label="name" name="insurance_iv" />
                                            </div>
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" style="min-width: 125px;">bit</span>
                                                </div>
                                                <input type="text" class="form-control" placeholder="Enter Insurance Bit" aria-label="name" name="insurance_bit_show" value="8" disabled />
                                                <input type="hidden" class="form-control" placeholder="Enter Insurance bit" aria-label="name" name="insurance_bit" value="8" />
                                            </div>
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" style="min-width: 125px;">Outbound</span>
                                                </div>
                                                <input type="text" class="form-control" placeholder="Enter URL" aria-label="name" name="insurance_outbound" />
                                            </div>
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <label class="input-group-text" for="inputGroupSelect01" style="min-width: 125px;">Tax</label>
                                                </div>
                                                <select class="custom-select" id="inputGroupSelect01" name="insurance_tax">
                                                    <option value="INTERNAL">INTERNAL</option>
                                                    <option value="EXTERNAL">EXTERNAL</option>
                                                </select>
                                            </div>
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <label class="input-group-text" for="inputGroupSelect01" style="min-width: 125px;">Type</label>
                                                </div>
                                                <select class="custom-select" name="insurance_type">
                                                    <option value="1">INSURANCE</option>
                                                    <option value="2">OTHER</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="submit" class="btn btn-success insurance-btn-save sub-save">
                                                Save
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        <!--Edit Modal -->
                        <div class="modal fade" id="insurancemodaledit" tabindex="-1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <form class="insurance_form_update" method="POST">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="editModalLabel">
                                              Edit insurance
                                            </h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" style="min-width: 125px;">Name</span>
                                                </div>
                                                <input type="hidden" class="form-control insurance_id_edit" placeholder="Enter Email" aria-label="name" name="insurance_id_edit" />
                                                <input type="text" class="form-control insurance_name_edit" placeholder="Enter Insurance Name" aria-label="name" name="insurance_name_edit" />
                                            </div>
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" style="min-width: 125px;">Auth Header</span>
                                                </div>
                                                <input type="text" class="form-control insurance_username_edit" placeholder="Enter Insurance Username" aria-label="name" name="insurance_username_edit" />
                                            </div>
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" style="min-width: 125px;">Auth Password</span>
                                                </div>
                                                <input type="text" class="form-control insurance_password_edit" placeholder="Enter Password" aria-label="name" name="insurance_password_edit" />
                                            </div>
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" style="min-width: 125px;">Secret Key</span>
                                                </div>
                                                <input type="text" class="form-control insurance_secret_key_show_edit" placeholder="Enter Insurance Secret Key" aria-label="name" name="insurance_secret_key_show_edit" disabled />
                                                <input type="hidden" class="form-control insurance_secret_key_show_edit" placeholder="Enter Insurance Secret Key" aria-label="name" name="insurance_secret_key_edit" />
                                            </div>
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" style="min-width: 125px;">iv</span>
                                                </div>
                                                <input type="text" class="form-control insurance_iv_show_edit" placeholder="Enter Insurance iv" aria-label="name" name="insurance_iv_show_edit" disabled />
                                                <input type="hidden" class="form-control insurance_iv_show_edit" placeholder="Enter Insurance iv" aria-label="name" name="insurance_iv_edit" />
                                            </div>
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" style="min-width: 125px;">bit</span>
                                                </div>
                                                <input type="text" class="form-control insurance_bit_show_edit" placeholder="Enter Insurance Bit" aria-label="name" name="insurance_bit_show_edit" value="8" disabled />
                                                <input type="hidden" class="form-control insurance_bit_show_edit" placeholder="Enter Insurance bit" aria-label="name" name="insurance_bit_edit" value="8" />
                                            </div>
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" style="min-width: 125px;">Outbound</span>
                                                </div>
                                                <input type="text" class="form-control insurance_outbound_edit" placeholder="Enter URL" aria-label="name" name="insurance_outbound_edit" />
                                            </div>
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <label class="input-group-text" for="inputGroupSelect02" style="min-width: 125px;">Options</label>
                                                </div>
                                                <select class="custom-select insurance_tax_edit" id="inputGroupSelect02" name="insurance_tax_edit">
                                                    <option value="INTERNAL">INTERNAL</option>
                                                    <option value="EXTERNAL">EXTERNAL</option>
                                                </select>
                                            </div>
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <label class="input-group-text" for="inputGroupSelect01" style="min-width: 125px;">Type</label>
                                                </div>
                                                <select class="custom-select insurance_type_edit" name="insurance_type_edit">
                                                    <option value="1">INSURANCE</option>
                                                    <option value="2">OTHER</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="submit" class="btn btn-success insurance-btn-update">
                                                <i class="ti-save"></i> Update
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        <!--change password -->
                        <div class="modal fade" id="insurancemodalpassword" tabindex="-1" role="dialog" aria-labelledby="passwordModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <form class="insurance_form_password" method="POST">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="passwordModalLabel">
                                              <i class="fas fa-insurance-plus m-r-10"></i> Change password insurance
                                            </h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="input-group mb-3">
                                                <input type="hidden" class="form-control insurance_id_edit" placeholder="Enter New Password" aria-label="name" name="insurance_id_edit" />
                                                <input type="password" class="form-control insurance_password_edit" placeholder="New Password" aria-label="name" name="insurance_password_edit" />
                                                <button class="btn btn-show-edit-pwd" type="button"><i class="fas fa-eye"></i></button>
                                                <button class="btn btn-hide-edit-pwd" type="button"><i class="fas fa-eye-slash"></i></button>
                                            </div>
                                            <div class="input-group mb-3">
                                                <input type="password" class="form-control insurance_password_confirm_edit" placeholder="Confirm New Password" aria-label="name" name="insurance_password_confirm_edit" />
                                                <button class="btn btn-show-conf-pwd" type="button"><i class="fas fa-eye"></i></button>
                                                <button class="btn btn-hide-conf-pwd" type="button"><i class="fas fa-eye-slash"></i></button>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="submit" class="btn btn-success insurance-btn-update-password">
                                                <i class="ti-save"></i> Update
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        <!-- list api access -->
                        <div class="modal fade" id="insuranceAccessModal" tabindex="-1" role="dialog" aria-labelledby="insuranceAccessModal" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <form class="insurance_form_access" method="POST">
                                        <div class="modal-header">
                                            <h5 class="modal-title">
                                              <i class="fas fa-user-plus m-r-10"></i> Edit Access
                                            </h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                            <input type="hidden" name="caller_id" class="caller_id_edit" />
                                            <input type="hidden" name="caller_name" class="caller_name_edit" />
                                        </div>
                                        <div class="modal-body">
                                            <table style="margin-top:7px;" id="file_export" class="table table-bordered nowrap display table-striped">
                                                <thead>
                                                    <tr>
                                                        <th><strong>Name</strong></th>
                                                        <th width="10%"><strong>Access</strong></th>
                                                    </tr>
                                                </thead>
                                                <tbody class="insurance-list-access">

                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="submit" class="btn btn-success insurance-access-btn-update">
                                                <i class="ti-save"></i> Update
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        <!--Filter Modal -->
                        <div class="modal fade" id="insurancemodalfilter" tabindex="-1" role="dialog" aria-labelledby="filterModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <form class="insurance_form_filter" method="POST">
                                        <div class="modal-header">
                                            <h5 class="modal-title sub-filter" id="filterModalLabel">
                                              Filter
                                            </h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <label class="input-group-text" style="width: 90px;">Name</label>
                                                </div>
                                                <input type="text" class="form-control" name="name">
                                            </div>

                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <label class="input-group-text" style="width: 90px;">Header</label>
                                                </div>
                                                <input type="text" class="form-control" name="header">
                                            </div>

                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <label class="input-group-text" style="width: 90px;">Password</label>
                                                </div>
                                                <input type="text" class="form-control" name="password">
                                            </div>

                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <label class="input-group-text" style="width: 90px;">Key Scret</label>
                                                </div>
                                                <input type="text" class="form-control" name="key_secret">
                                            </div>

                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <label class="input-group-text" style="width: 90px;">IV</label>
                                                </div>
                                                <input type="text" class="form-control" name="iv">
                                            </div>

                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <label class="input-group-text" style="width: 90px;">Bit</label>
                                                </div>
                                                <input type="text" class="form-control" name="bit">
                                            </div>

                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <label class="input-group-text" style="width: 90px;">Outbound</label>
                                                </div>
                                                <input type="text" class="form-control" name="outbound">
                                            </div>

                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <label class="input-group-text" style="width: 90px;">Tax</label>
                                                </div>
                                                <select class="form-control" name="tax">
                                                    <option value="ALL">ALL</option>
                                                    <option value="INTERNAL">INTERNAL</option>
                                                    <option value="EXTERNAL">EXTERNAL</option>
                                                </select>
                                            </div>

                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <label class="input-group-text" style="width: 90px;">Type</label>
                                                </div>
                                                <select class="form-control" name="type">
                                                    <option value="ALL">ALL</option>
                                                    <option value="1">INSURANCE</option>
                                                    <option value="2">OTHER</option>
                                                </select>
                                            </div>

                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <label class="input-group-text" style="width: 90px;">Status</label>
                                                </div>
                                                <select class="form-control" name="status">
                                                    <option>ALL</option>
                                                    <option value="ACTIVE">Active</option>
                                                    <option value="DEACTIVE">DEACTIVE</option>
                                                </select>
                                            </div>

                                        </div>
                                        <div class="modal-footer">
                                            <button class="btn btn-warning insurance-btn-reset-filter sub-reset">
                                                Reset
                                            </button>
                                            <button type="submit" class="btn btn-success insurance-btn-filter-exe sub-submit">
                                                Submit
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    var this_page_insurance = 1;
    var total_page_insurance = 1;

    function makesecretkey(length) {
       var result           = '';
       var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
       var charactersLength = characters.length;
       for ( var i = 0; i < length; i++ ) {
          result += characters.charAt(Math.floor(Math.random() * charactersLength));
       }
       return result;
    }

    function insurance_ready() {
        table_get_insurance();
        save_data_insurance();
        update_password_insurance();
        insurance_filter();
        paging("insurance");

        $('.insurance-btn-new').unbind('click')
        $('.insurance-btn-new').click(function() {
            $('.insurance_form').trigger("reset");
            $('input[name="insurance_secret_key_show"]').val(makesecretkey(24));
            var sk = $('input[name="insurance_secret_key_show"]').val();
            $('input[name="insurance_secret_key"]').val(sk);
            $('input[name="insurance_iv_show"]').val(makesecretkey(8));
            var iv = $('input[name="insurance_iv_show"]').val();
            $('input[name="insurance_iv"]').val(iv);
        })

        $('.insurance-btn-reset-filter').unbind('click')
        $('.insurance-btn-reset-filter').click(function(e) {
            e.preventDefault();
            $('.insurance_form_filter').trigger("reset");
            $('#insurancemodalfilter').modal('toggle');
            var data = $('.insurance_form_filter').serialize();
            table_get_insurance(data);
        })

        $(".btn-hide-pwd").hide();
        $(".btn-show-pwd").click(function(e) {
          $(".box-pwd").get(0).type = "text";
          $(".btn-show-pwd").hide();
          $(".btn-hide-pwd").show();
        });

        $(".btn-hide-pwd").click(function(e) {
          $(".box-pwd").get(0).type = "password";
          $(".btn-show-pwd").show();
          $(".btn-hide-pwd").hide();
        });

        $(".btn-hide-edit-pwd").hide();
        $(".btn-show-edit-pwd").click(function(e) {
          $(".insurance_password_edit").get(0).type = "text";
          $(".btn-show-edit-pwd").hide();
          $(".btn-hide-edit-pwd").show();
        });

        $(".btn-hide-edit-pwd").click(function(e) {
          $(".insurance_password_edit").get(0).type = "password";
          $(".btn-show-edit-pwd").show();
          $(".btn-hide-edit-pwd").hide();
        });

        $(".btn-hide-conf-pwd").hide();
        $(".btn-show-conf-pwd").click(function(e) {
          $(".insurance_password_confirm_edit").get(0).type = "text";
          $(".btn-show-conf-pwd").hide();
          $(".btn-hide-conf-pwd").show();
        });

        $(".btn-hide-conf-pwd").click(function(e) {
          $(".insurance_password_confirm_edit").get(0).type = "password";
          $(".btn-show-conf-pwd").show();
          $(".btn-hide-conf-pwd").hide();
        });
    }

    function table_get_insurance(data) {
        $('.insurance-api-list').html('')
        $.ajax({
            url: url_api+'insurance/list/',
            type: 'GET',
	        data: data,
            dataType: 'json',
            beforeSend: function(xhr) {
              xhr.setRequestHeader("Authorization", "Basic " + btoa(usr + ":" + pwd));
              xhr.setRequestHeader("commandid", "insurance");
              xhr.setRequestHeader("requestid", "insurance" + Date.now());
              xhr.setRequestHeader("requestdt", datetime);
              xhr.setRequestHeader("clientid", insurance_username);
              xhr.setRequestHeader("signature", signature);
            },
            success: function (res, textStatus, xhr) {
                res.body.data.map(function(value) {

                    if(value.type == 1){
                        var type_show = "INSURANCE";
                    }else if(value.type == 2){
                        var type_show = "OTHER";
                    }else{
                        var type_show = "NULL";
                    }

                    $('.insurance-api-list').append(`<tr>
                                                <td>${value.name}</td>
                                                <td>${value.auth_header}</td>
                                                <td>${value.auth_password}</td>
                                                <td>${value.secret_key}</td>
                                                <td>${value.iv}</td>
                                                <td>${value.bit}</td>
                                                <td>${value.outbound}</td>
                                                <td>${value.tax}</td>
                                                <td>${type_show}</td>
                                                <td>${value.status}</td>
                                                <td>
                                                    <button
                                                    type="button"
                                                    class="btn btn-warning insurance-btn-edit"
                                                    attr-id="${value.id}"
                                                    data-toggle="modal"
                                                    data-target="#insurancemodaledit"
                                                    >
                                                    <i class="fas fa-pencil-alt"></i>
                                                    </button>

                                                    <button
                                                    type="button"
                                                    class="btn btn-info insurance-btn-access"
                                                    attr-id="${value.id}"
                                                    data-toggle="modal"
                                                    data-target="#insuranceAccessModal"
                                                    >
                                                    <i class="fas fa-list"></i>
                                                    </button>

                                                    <button
                                                    type="button"
                                                    class="btn btn-danger insurance-btn-delete"
                                                    attr-id="${value.id}"
                                                    >
                                                    <i class="fas fa-trash-alt"></i>
                                                    </button>
                                                </td>
                                                </tr>`)
                });
                total_page_insurance = res.body.pagination.total_page;
                paging_ajax("insurance", res);
                delete_data_insurance();
                insurance_access();
                edit_data_insurance();
            },
            error: function (xhr, textStatus, errorThrown) {

            }
        });
    }

    function insurance_access(){
      $('.insurance-btn-access').unbind('click');
      $('.insurance_form_access').unbind('serialize');
      $('.insurance-btn-access').click(function(e) {
        var id_param = $(this).attr('attr-id');
        $('.caller_id_edit').val(id_param);
        var caller_name = $('.sess-fullname').text();
        $('.caller_name_edit').val(caller_name);
        e.preventDefault();
        $.ajax({
            url: url_api+'insurance/access/'+id_param,
            type: 'GET',
            dataType: 'json',
            beforeSend: function(xhr) {
                xhr.setRequestHeader("Authorization", "Basic " + btoa(usr + ":" + pwd));
                xhr.setRequestHeader("commandid", "Inusrance Access");
                xhr.setRequestHeader("requestid", "InsuranceAccess" + Date.now());
                xhr.setRequestHeader("requestdt", datetime);
                xhr.setRequestHeader("clientid", insurance_username);
                xhr.setRequestHeader("signature", signature);
            },
            success: function (res, textStatus, xhr) {
                $('.insurance-list-access').html('');
                res.body.data.map(function(value) {
                    if(value.access != null){
                      var check_post = "checked";
                    }

                    $('.insurance-list-access').append(`<tr>
                                                <td>
                                                  ${value.name}
                                                  <input type="hidden" name="api_id[${value.id}]" value="${value.id}">
                                                </td>
                                                <td>
                                                  <div class="custom-control custom-checkbox">
                                                      <input type="checkbox" class="custom-control-input" id="post${value.id}" name="post[${value.id}]" value="1" ${check_post}>
                                                      <label class="custom-control-label" for="post${value.id}"></label>
                                                  </div>
                                                </td>
                                            </tr>`);
                })
                save_insurance_access();
            },
            error: function (xhr, textStatus, errorThrown) {

            }
          });
      });
    }

    function save_insurance_access(){
        $('.insurance-access-btn-update').unbind('click');
        $('.insurance_form_access').unbind('serialize');
        $('.insurance-access-btn-update').click(function(e) {
            e.preventDefault();
            var data = $('.insurance_form_access').serialize();
            $.ajax({
                url: url_api+'insurance/access/',
                type: 'POST',
                dataType: 'json',
                data:data,
                beforeSend: function(xhr) {
                    xhr.setRequestHeader("Authorization", "Basic " + btoa(usr + ":" + pwd));
                    xhr.setRequestHeader("commandid", "insurance");
                    xhr.setRequestHeader("requestid", "insurance" + Date.now());
                    xhr.setRequestHeader("requestdt", datetime);
                    xhr.setRequestHeader("clientid", insurance_username);
                    xhr.setRequestHeader("signature", signature);
                },
                success: function (data, textStatus, xhr) {
                    if(data.body.isupdate){
                        $('#insuranceAccessModal').modal('toggle');
                        table_get_insurance();
                        toastr.success("Success Updated!", 'Success!');
                    }else{
                        var ret_message = "";
                        $.each(data.body.message, function( index, value ) {
                            ret_message += "<br>" + index + ": " + value;
                        });
                        toastr.error(ret_message, 'Error!');
                    }
                },
                error: function (xhr, textStatus, errorThrown) {

                }
            });
        });
    }

    function save_data_insurance() {
        $('.insurance-btn-save').unbind('click');
        $('.insurance_form').unbind('serialize');
        $('.insurance-btn-save').click(function(e) {
            e.preventDefault();
            var data = $('.insurance_form').serialize();
            $.ajax({
                url: url_api+'insurance',
                type: 'POST',
                dataType: 'json',
                data:data,
                beforeSend: function(xhr) {
                    xhr.setRequestHeader("Authorization", "Basic " + btoa(usr + ":" + pwd));
                    xhr.setRequestHeader("commandid", "insurance");
                    xhr.setRequestHeader("requestid", "insurance" + Date.now());
                    xhr.setRequestHeader("requestdt", datetime);
                    xhr.setRequestHeader("clientid", insurance_username);
                    xhr.setRequestHeader("signature", signature);
                },
                success: function (data, textStatus, xhr) {
                    if(data.body.isinsert){
                        $('#insurancemodalcreate').modal('toggle');
                        $('input[name="insurance_name"]').val('')
                        table_get_insurance();
                        toastr.success("Success Saved!", 'Success!');
                    }else{
                        var ret_message = "";
                        $.each(data.body.message, function( index, value ) {
                            ret_message += "<br>" + index + ": " + value;
                        });
                        toastr.error(ret_message, 'Error!');
                    }
                },
                error: function (xhr, textStatus, errorThrown) {

                }
            });
        })
    }

    function delete_data_insurance() {
        $('.insurance-btn-delete').unbind('click')
        $('.insurance-btn-delete').click(function() {
            var id_param = $(this).attr('attr-id');
            swal({
                title: "Are you sure?",
                text: "Are you sure for delete this data ?",
                icon: "warning",
                buttons: ["No","Yes"],
                dangerMode: true
            }).then(function(isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        url: url_api+'insurance/' + id_param,
                        type: 'DELETE',
                        dataType: 'json',
                        beforeSend: function(xhr) {
                          xhr.setRequestHeader("Authorization", "Basic " + btoa(usr + ":" + pwd));
                          xhr.setRequestHeader("commandid", "insurance");
                          xhr.setRequestHeader("requestid", "insurance" + Date.now());
                          xhr.setRequestHeader("requestdt", datetime);
                          xhr.setRequestHeader("clientid", insurance_username);
                          xhr.setRequestHeader("signature", signature);
                        },
                        success: function (data, textStatus, xhr) {
                            if(data.body.isdelete){
                                table_get_insurance();
                                toastr.success("Success Changed!", 'Success!');
                            }else{
                                var ret_message = "";
                                $.each(data.body.message, function( index, value ) {
                                    ret_message += "<br>" + index + ": " + value;
                                });
                                toastr.error(ret_message, 'Error!');
                            }
                        },
                        error: function (xhr, textStatus, errorThrown) {

                        }
                    });
                }
            })
        })
    }

    function edit_data_insurance() {
        $('.insurance-btn-edit').unbind('click');
        $('.insurance-btn-edit').click(function() {
            $('.insurance_id_edit').val('');
            $('.insurance_name_edit').val('');
            var id_param = $(this).attr('attr-id');
            $.ajax({
            url: url_api+'insurance/list/' + id_param,
            type: 'GET',
            dataType: 'json',
            beforeSend: function(xhr) {
              xhr.setRequestHeader("Authorization", "Basic " + btoa(usr + ":" + pwd));
              xhr.setRequestHeader("commandid", "insurance");
              xhr.setRequestHeader("requestid", "insurance" + Date.now());
              xhr.setRequestHeader("requestdt", datetime);
              xhr.setRequestHeader("clientid", insurance_username);
              xhr.setRequestHeader("signature", signature);
            },
            success: function (res, textStatus, xhr) {
                $('.insurance_id_edit').val(res.body.data[0].id);
                $('.insurance_name_edit').val(res.body.data[0].name);
                $('.insurance_username_edit').val(res.body.data[0].auth_header);
                $('.insurance_password_edit').val(res.body.data[0].auth_password);
                $('.insurance_secret_key_show_edit').val(res.body.data[0].secret_key);
                $('.insurance_iv_show_edit').val(res.body.data[0].iv);
                $('.insurance_bit_show_edit').val(res.body.data[0].bit);
                $('.insurance_outbound_edit').val(res.body.data[0].outbound);
                $('.insurance_tax_edit').val(res.body.data[0].tax);
                $('.insurance_type_edit').val(res.body.data[0].type);
                update_data_insurance();
            },
            error: function (xhr, textStatus, errorThrown) {

            }
        });
        })
    }

    function update_data_insurance() {
        $('.insurance-btn-update').unbind('click');
        $('.insurance_form_update').unbind('serialize');
        $('.insurance-btn-update').click(function(e) {
            e.preventDefault();
            var data = $('.insurance_form_update').serialize();
            $.ajax({
                url: url_api+'insurance/update',
                type: 'POST',
                dataType: 'json',
                data:data,
                beforeSend: function(xhr) {
                  xhr.setRequestHeader("Authorization", "Basic " + btoa(usr + ":" + pwd));
                  xhr.setRequestHeader("commandid", "insurance");
                  xhr.setRequestHeader("requestid", "insurance" + Date.now());
                  xhr.setRequestHeader("requestdt", datetime);
                  xhr.setRequestHeader("clientid", insurance_username);
                  xhr.setRequestHeader("signature", signature);
                },
                success: function (data, textStatus, xhr) {
                    if(data.body.isupdate){
                        $('#insurancemodaledit').modal('toggle');
                        table_get_insurance();
                        toastr.success("Success Updated!", 'Success!');
                    }else{
                        var ret_message = "";
                        $.each(data.body.message, function( index, value ) {
                            ret_message += "<br>" + index + ": " + value;
                        });
                        toastr.error(ret_message, 'Error!');
                    }
                },
                error: function (xhr, textStatus, errorThrown) {

                }
            });
        })
    }

    function update_password_insurance() {
        $('.insurance-btn-update-password').unbind('click');
        $('.insurance_form_password').unbind('serialize');
        $('.insurance-btn-update-password').click(function(e) {
            e.preventDefault();
            var data = $('.insurance_form_password').serialize();
            $.ajax({
                url: url_api+'insurance/update_password',
                type: 'POST',
                dataType: 'json',
                data:data,
                beforeSend: function(xhr) {
                  xhr.setRequestHeader("Authorization", "Basic " + btoa(usr + ":" + pwd));
                  xhr.setRequestHeader("commandid", "insurance");
                  xhr.setRequestHeader("requestid", "insurance" + Date.now());
                  xhr.setRequestHeader("requestdt", datetime);
                  xhr.setRequestHeader("clientid", insurance_username);
                  xhr.setRequestHeader("signature", signature);
                },
                success: function (data, textStatus, xhr) {
                    if(data.body.isupdate){
                        $('#insurancemodalpassword').modal('toggle');
                        table_get_insurance();
                        toastr.success("Success Updated!", 'Success!');
                    }else{
                        var ret_message = "";
                        $.each(data.body.message, function( index, value ) {
                            ret_message += "<br>" + index + ": " + value;
                        });
                        toastr.error(ret_message, 'Error!');
                    }
                },
                error: function (xhr, textStatus, errorThrown) {

                }
            });
        })
    }

    function insurance_filter(){
      $('.insurance-btn-filter-exe').unbind('click');
      $('.insurance-btn-filter-exe').click(function(e){
          e.preventDefault();
          var data = $('.insurance_form_filter').serialize();
          table_get_insurance(data);
          $('#insurancemodalfilter').modal('toggle');
      });
    }
</script>
