<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="tab-content tabcontent-border p-20" id="myTabContent">
                    <div role="tabpanel" class="tab-pane fade show active" id="home5" aria-labelledby="home-tab">
                        <div class="row">
                          <div class="col-5 align-self-center">
                              <div class="d-flex align-items-center">
                                  <nav aria-label="breadcrumb">
                                      <ol class="breadcrumb">
                                          <li class="breadcrumb-item">Encrypt & Descrypt</li>
                                      </ol>
                                  </nav>
                              </div>
                          </div>
                        </div>
                        <hr />
                        <!-- Row -->
                        <div class="row">
                            <div class="col-md-6 offset-md-3">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" style="min-width: 125px;">Insurance</span>
                                    </div>
                                    <select class="form-control insurance-api-list-encrypt" name="insurancefile_insurance">

                                    </select>
                                </div>
                            </div>

                            <div class="col-md-6" align="center">
                                <form class="encrypt_form" method="POST" enctype="multipart/form-data">
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" style="min-width: 125px;">File Encrypt</span>
                                        </div>
                                        <input type="file" class="form-control" placeholder="Select file CSV" aria-label="name" name="file_upload"/>
                                        <input type="hidden" class="form-control caller_id_encrypt" name="caller_id"/>
                                        <input type="hidden" class="form-control type_encrypt" name="type" value="encrypt"/>
                                    </div>
                                    <button type="submit" class="btn btn-success btn-encrypt-file">
                                        Encrypt
                                    </button>
                                </form>
                            </div>

                            <div class="col-md-6" align="center">
                                <form class="descrypt_form" method="POST" enctype="multipart/form-data">
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" style="min-width: 125px;">File Descrypt</span>
                                        </div>
                                        <input type="file" class="form-control" placeholder="Select file CSV" aria-label="name" name="file_upload"/>
                                        <input type="hidden" class="form-control caller_id_descrypt" name="caller_id"/>
                                        <input type="hidden" class="form-control type_encrypt" name="type" value="descrypt"/>
                                    </div>
                                    <button type="submit" class="btn btn-success btn-descrypt-file">
                                        Descrypt
                                    </button>
                                </form>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function encrypt_ready() {
        encrypt_process();

        $.ajax({
            url: url_api+'insurance',
            type: 'GET',
            dataType: 'json',
            beforeSend: function(xhr) {
              xhr.setRequestHeader("Authorization", "Basic " + btoa(usr + ":" + pwd));
              xhr.setRequestHeader("commandid", "insurance List");
              xhr.setRequestHeader("requestid", "insurancelist" + Date.now());
              xhr.setRequestHeader("requestdt", datetime);
              xhr.setRequestHeader("clientid", insurance_username);
              xhr.setRequestHeader("signature", signature);
            },
            success: function (res, textStatus, xhr) {
                res.body.data.map(function(value) {
                    $('.insurance-api-list-encrypt').append(`<option value="${value.id}">${value.name}</option>`);
                })
            },
            error: function (xhr, textStatus, errorThrown) {

            }
        });
    }

    function encrypt_process() {
        $('.btn-encrypt-file').click(function(e){
            var caller_id = $('.insurance-api-list-encrypt').val();
            $('.caller_id_encrypt').val(caller_id);
        });

        $('.btn-descrypt-file').click(function(e){
            var caller_id = $('.insurance-api-list-encrypt').val();
            $('.caller_id_descrypt').val(caller_id);
        });

        var options = {
            url: url_api+'encrypt',
            type: 'POST',
            beforeSend: function(xhr) {
                xhr.setRequestHeader("Authorization", "Basic " + btoa(usr + ":" + pwd));
                xhr.setRequestHeader("commandid", "Encrypt");
                xhr.setRequestHeader("requestid", "encrypt" + Date.now());
                xhr.setRequestHeader("requestdt", datetime);
                xhr.setRequestHeader("clientid", insurance_username);
                xhr.setRequestHeader("signature", signature);
            },
            success: function (res, textStatus, xhr) {
                if(res.body.isfinish){
                    var text_csv = "";
                    var date = new Date();
                    var year = date.getFullYear();
                    var month = date.getMonth() + 1;
                    var day = date.getDate();
                    var hours = date.getHours();
                    var minutes = date.getMinutes();
                    var seconds = date.getSeconds();

                    var newdate = year + "-" + month + "-" + day + " " + hours + ":" + minutes + ":" + seconds;

                    res.body.data.map(function(value) {
                        text_csv += value.trim() + "\r\n";
                    })

                    let csvContent = "data:text/csv;charset=utf-8," + text_csv;

                    var encodedUri = encodeURI(csvContent);
                    var link = document.createElement("a");
                    link.setAttribute("href", encodedUri);
                    link.setAttribute("download", newdate + ".csv");
                    document.body.appendChild(link);

                    link.click();

                    // var encodedUri = encodeURI(csvContent);
                    // window.open(encodedUri);
                }else{
                    var ret_message = "";
                    $.each(res.body.message, function( index, value ) {
                        ret_message += "<br>" + index + ": " + value;
                    });
                    toastr.error(ret_message, 'Error!');
                }
            }
        };

        $('.encrypt_form').submit(function() {
            $(this).ajaxSubmit(options);
            return false;
        });

        $('.descrypt_form').submit(function() {
            $(this).ajaxSubmit(options);
            return false;
        });
    }
</script>
