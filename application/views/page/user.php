<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="tab-content tabcontent-border p-20" id="myTabContent">
                    <div role="tabpanel" class="tab-pane fade show active" id="home5" aria-labelledby="home-tab">
                        <div class="row">
                          <div class="col-5 align-self-center">
                              <div class="d-flex align-items-center">
                                  <nav aria-label="breadcrumb">
                                      <ol class="breadcrumb">
                                          <li class="breadcrumb-item sub-user">User</li>
                                      </ol>
                                  </nav>
                              </div>
                          </div>
                        </div>
                        <hr />
                        <!-- Row -->
                        <div class="row">
                            <div class="col-md-9">
                                <button type="button" class="btn btn-success user-btn-new sub-create-new" data-toggle="modal" data-target="#usermodalcreate">
                                    Create New
                                </button>
                                <button type="button" class="btn btn-danger user-btn-delete-checked sub-delete">
                                    Delete
                                </button>
                            </div>

                            <div class="col-md-3" align="right">
                                <button type="button" class="btn btn-warning user-btn-filter sub-filter" data-toggle="modal" data-target="#usermodalfilter">
                                    Filter
                                </button>
                                <button type="button" class="btn btn-info user-btn-refresh sub-refresh" onclick="refresh_data('user')">
                                    Refresh
                                </button>
                            </div>

                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table style="margin-top:7px;" id="file_export" class="table table-bordered nowrap display table-striped">
                                        <thead>
                                            <tr>
                                                <th align="center">
                                                    <div class="custom-control custom-checkbox" align="center">
                                                        <input type="checkbox" class="custom-control-input checkAll" value="1" id="checkboxTop" style="display:none;">
                                                        <label class="custom-control-label" for="checkboxTop"></label>
                                                    </div>
                                                </th>
                                                <th><strong class="th-user-fullname">Full Name</strong></th>
                                                <th><strong class="th-user-username">User Name</strong></th>
                                                <th><strong class="th-user-phone">Phone</strong></th>
                                                <th><strong class="th-user-role">Role</strong></th>
                                                <th><strong class="th-user-caller">Caller</strong></th>
                                                <th><strong class="th-user-status">Status</strong></th>
                                                <th width="5%"><strong class="th-user-act">Actions</strong></th>
                                            </tr>
                                        </thead>
                                        <tbody class="user-api-list">

                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <form class="paging_form_user" method="GET">
                                    <div class="ikut-table-user row" style="display: none;">
                                        <div class="col-md-3">
                                            <div class="total-data-get-user form-control"></div>
                                        </div>

                                        <div class="col-md-9">
                                            <ul class="pagination float-right">
                                                <li>
                                                    <select class="form-control change-num-records-user" name="item_per_page">
                                                        <option value="10">10</option>
                                                        <option value="20">20</option>
                                                        <option value="50">50</option>
                                                        <option value="100">100</option>
                                                    </select>
                                                </li>
                                                <li style="margin-right: 15px;">
                                                    <div style="padding: .375rem .75rem;" class="sub-records">
                                                        Records
                                                    </div>
                                                </li>
                                                <li class="footable-page-arrow page-item first-page-user">
                                                    <a data-page="first" class="page-link link-first-page-user">«</a>
                                                </li>
                                                <li class="footable-page-arrow page-item prev-page-user" style="padding-right: .75rem;">
                                                    <a data-page="prev" class="page-link link-prev-page-user">‹</a>
                                                </li>
                                                <li>
                                                    <input type="text" class="form-control this-page-get-user" name="page" style="width: 40px;">
                                                </li>
                                                <li>
                                                    <div class="total-page-get-user" style="padding: .375rem .75rem;"></div>
                                                </li>
                                                <li>
                                                    <btn type="button" class="btn btn-success go-page-user sub-go">Go</btn>
                                                </li>
                                                <li class="footable-page-arrow page-item next-page-user" style="padding-left: .75rem;">
                                                    <a data-page="next" class="page-link link-next-page-user">›</a>
                                                </li>
                                                <li class="footable-page-arrow page-item last-page-user">
                                                    <a data-page="last" class="page-link link-last-page-user">»</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                        <!--Create New Modal -->
                        <div class="modal fade" id="usermodalcreate" tabindex="-1" role="dialog" aria-labelledby="createModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <form class="user_create_form" method="POST" enctype="multipart/form-data">
                                        <div class="modal-header">
                                            <h5 class="modal-title sub-create-new-user" id="createModalLabel">
                                              Create New User
                                            </h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                  <span class="input-group-text th-user-username" style="min-width: 125px;">Username</span>
                                                </div>
                                                <input type="text" class="form-control" placeholder="Enter Email" aria-label="name" name="username" />
                                            </div>
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                  <span class="input-group-text th-user-fullname" style="min-width: 125px;">Fullname</span>
                                                </div>
                                                <input type="text" class="form-control" placeholder="Enter Full Name" aria-label="name" name="fullname" />
                                            </div>
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                  <span class="input-group-text th-user-phone" style="min-width: 125px;">Phone</span>
                                                </div>
                                                <input type="text" class="form-control" placeholder="Enter Phone" aria-label="name" name="phone" />
                                            </div>
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                  <span class="input-group-text" style="min-width: 125px;">Photo</span>
                                                </div>
                                                <input type="file" class="form-control" placeholder="Select Image" aria-label="name" name="user_file" id="user-file" />
                                            </div>
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                  <span class="input-group-text" style="min-width: 125px;">Password</span>
                                                </div>
                                                <input type="password" class="form-control box-pwd" placeholder="Enter Password" aria-label="name" name="password" />
                                                <button class="btn btn-show-pwd" type="button"><i class="fas fa-eye"></i></button>
                                                <button class="btn btn-hide-pwd" type="button"><i class="fas fa-eye-slash"></i></button>
                                            </div>
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                  <span class="input-group-text  th-user-role" style="min-width: 125px;">Role</span>
                                                </div>
                                                <select class="form-control user-access-api-list" name="role_id">

                                                </select>
                                            </div>
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                  <span class="input-group-text th-user-caller" style="min-width: 125px;">Caller</span>
                                                </div>
                                                <select class="form-control user-caller-api-list" name="caller_id">

                                                </select>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="submit" class="btn btn-success user-btn-save">
                                                <i class="ti-save"></i> Save
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        <!--Filter Modal -->
                        <div class="modal fade" id="usermodalfilter" tabindex="-1" role="dialog" aria-labelledby="filterModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <form class="user_form_filter" method="POST">
                                        <div class="modal-header">
                                            <h5 class="modal-title sub-filter" id="filterModalLabel">
                                              Filter
                                            </h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <label class="input-group-text th-user-fullname" style="width: 90px;">Fullname</label>
                                                </div>
                                                <input type="text" class="form-control" name="fullname">
                                            </div>

                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <label class="input-group-text th-user-username" style="width: 90px;">Username</label>
                                                </div>
                                                <input type="text" class="form-control" name="username">
                                            </div>

                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <label class="input-group-text th-user-phone" style="width: 90px;">Phone</label>
                                                </div>
                                                <input type="text" class="form-control" name="phone">
                                            </div>

                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <label class="input-group-text th-user-role" style="width: 90px;">Role</label>
                                                </div>
                                                <select class="form-control user-access-api-list" name="role">
                                                    <option>ALL</option>
                                                </select>
                                            </div>

                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <label class="input-group-text th-user-caller" style="width: 90px;">Caller</label>
                                                </div>
                                                <select class="form-control user-caller-api-list" name="caller">
                                                    <option>ALL</option>
                                                </select>
                                            </div>

                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <label class="input-group-text" style="width: 90px;">Status</label>
                                                </div>
                                                <select class="form-control" name="status">
                                                    <option>ALL</option>
                                                    <option value="1">Active</option>
                                                    <option value="2">Non Active</option>
                                                </select>
                                            </div>

                                        </div>
                                        <div class="modal-footer">
                                            <button class="btn btn-warning user-btn-reset-filter sub-reset">
                                                Reset
                                            </button>
                                            <button type="submit" class="btn btn-success user-btn-filter-exe sub-submit">
                                                Submit
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
      var this_page_user = 1;
      var total_page_user = 1;

      function user_ready() {
          table_get_user();
          save_data_user();
          user_filter();
          paging("user");

          $('.user-btn-new').unbind('click')
          $('.user-btn-new').click(function() {
              $('.user_create_form').trigger("reset");
          })

          $('.user-btn-reset-filter').unbind('click')
          $('.user-btn-reset-filter').click(function(e) {
              e.preventDefault();
              $('.user_form_filter').trigger("reset");
              $('#usermodalfilter').modal('toggle');
              var data = $('.role_form_filter').serialize();
              table_get_user(data);
          })

          $('.user-btn-delete-checked').unbind('click')
          $('.user-btn-delete-checked').click(function() {
              var arr_del = new Array();
              $('input[name="user_checked"]:checked').each(function() {
                  arr_del.push(this.value);
              }).promise().done(function () {
                  swal({
                      title: "Are you sure?",
                      text: "Are you sure for delete this data ?",
                      icon: "warning",
                      buttons: ["No","Yes"],
                      dangerMode: true
                  }).then(function(isConfirm) {
                      if (isConfirm) {
                          $.ajax({
                              url: url_api+'users/deleteall',
                              type: 'POST',
                              data: {data: arr_del},
                              beforeSend: function(xhr) {
                                xhr.setRequestHeader("Authorization", "Basic " + btoa(usr + ":" + pwd));
                                xhr.setRequestHeader("commandid", "Users Delete");
                                xhr.setRequestHeader("requestid", "usersdelete" + Date.now());
                                xhr.setRequestHeader("requestdt", datetime);
                                xhr.setRequestHeader("clientid", insurance_username);
                                xhr.setRequestHeader("signature", signature);
                              },
                              success: function (data, textStatus, xhr) {
                                  if(data.body.isdelete){
                                      table_get_user();
                                      toastr.success("Success Deleted!", 'Success!');
                                  }else{
                                      var ret_message = "";
                                      $.each(data.body.message, function( index, value ) {
                                          ret_message += "<br>" + index + ": " + value;
                                      });
                                      toastr.error(ret_message, 'Error!');
                                  }
                              },
                              error: function (xhr, textStatus, errorThrown) {

                              }
                          });
                      }
                  })
              });
          })

          $('#checkboxTop').click(function() {
              if($(this).is(':checked')){
                  $('.checkAll').prop('checked', true);
              }else{
                  $('.checkAll').prop('checked', false);
              }
          })

          $.ajax({
              url: url_api+'role',
              type: 'GET',
              dataType: 'json',
              beforeSend: function(xhr) {
                xhr.setRequestHeader("Authorization", "Basic " + btoa(usr + ":" + pwd));
                xhr.setRequestHeader("commandid", "User List");
                xhr.setRequestHeader("requestid", "userlist" + Date.now());
                xhr.setRequestHeader("requestdt", datetime);
                xhr.setRequestHeader("clientid", insurance_username);
                xhr.setRequestHeader("signature", signature);
              },
              success: function (res, textStatus, xhr) {
                  res.body.data.map(function(value) {
                      $('.user-access-api-list').append(`<option value="${value.id}">${value.name}</option>`);
                  })
              },
              error: function (xhr, textStatus, errorThrown) {

              }
          });

          $.ajax({
              url: url_api+'insurance',
              type: 'GET',
              dataType: 'json',
              beforeSend: function(xhr) {
                xhr.setRequestHeader("Authorization", "Basic " + btoa(usr + ":" + pwd));
                xhr.setRequestHeader("commandid", "User List");
                xhr.setRequestHeader("requestid", "userlist" + Date.now());
                xhr.setRequestHeader("requestdt", datetime);
                xhr.setRequestHeader("clientid", insurance_username);
                xhr.setRequestHeader("signature", signature);
              },
              success: function (res, textStatus, xhr) {
                  res.body.data.map(function(value) {
                      $('.user-caller-api-list').append(`<option value="${value.id}">${value.name}</option>`);
                  })
              },
              error: function (xhr, textStatus, errorThrown) {

              }
          });

          $(".btn-hide-pwd").hide();
          $(".btn-show-pwd").click(function(e) {
            $(".box-pwd").get(0).type = "text";
            $(".btn-show-pwd").hide();
            $(".btn-hide-pwd").show();
          });

          $(".btn-hide-pwd").click(function(e) {
            $(".box-pwd").get(0).type = "password";
            $(".btn-show-pwd").show();
            $(".btn-hide-pwd").hide();
          });
      }

      function table_get_user(data) {
          $('.user-api-list').html('')
          $.ajax({
              url: url_api+'users',
              type: 'GET',
	          data: data,
              dataType: 'json',
              beforeSend: function(xhr) {
                xhr.setRequestHeader("Authorization", "Basic " + btoa(usr + ":" + pwd));
                xhr.setRequestHeader("commandid", "Users");
                xhr.setRequestHeader("requestid", "users" + Date.now());
                xhr.setRequestHeader("requestdt", datetime);
                xhr.setRequestHeader("clientid", insurance_username);
                xhr.setRequestHeader("signature", signature);
              },
              success: function (res, textStatus, xhr) {
                  res.body.data.map(function(value) {

                      if(value.picture === null){
                        value.picture = direc + "/uploads/profile/def.jpg";
                      }else{
                        value.picture = direc + value.picture;
                      }

                      $('.user-api-list').append(`<tr>
                                                  <td align="canter">
                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" name="user_checked" class="custom-control-input checkAll" value="${value.id}" id="checkbox${value.id}" style="display:none;">
                                                        <label class="custom-control-label" for="checkbox${value.id}"></label>
                                                    </div>
                                                  </td>
                                                  <td>
                                                      <a href="userdetail.html" class="trigger-side-menu menu-userdetail-${value.id}" side-attr-id="${value.id}">
                                                        <div class="row">
                                                            <div class="circular--landscape" style="height: 30px;width: 30px;">
                                                                <img src="${value.picture}" alt="user" class="rounded-circle" style="margin-left: -5px;">
                                                            </div>
                                                            <div class="title-userdetail" attr-id="${value.id}" style="padding: 0.4em;">
                                                                ${value.fullname}
                                                            </div>
                                                        </div>
                                                      </a>
                                                  </td>
                                                  <td>${value.username}</td>
                                                  <td>${value.phone}</td>
                                                  <td>${value.role_name}</td>
                                                  <td>${value.caller_name}</td>
                                                  <td class="bt-switch" align="center">
                                                    <div class="bootstrap-switch-mini bootstrap-switch bootstrap-switch-wrapper bootstrap-switch-animate" style="width: 48px;">
                                                      <div class="bootstrap-switch-container" style="width: 75.3333px;" attr-id="${value.id}" attr-status="${value.status}">
                                                        <span class="bootstrap-switch-handle-on bootstrap-switch-success" style="width: 33px;">ON</span>
                                                        <span class="bootstrap-switch-label" style="width: 33px;">&nbsp;</span>
                                                        <span class="bootstrap-switch-handle-off bootstrap-switch-danger" style="width: 33px;">OFF</span>
                                                        <input data-size="mini" type="checkbox" checked="" data-on-color="success" data-off-color="danger"></div>
                                                      </div>
                                                  </td>
                                                  <td align="center">
                                                      <button
                                                      type="button"
                                                      class="btn btn-danger btn-sm user-btn-delete"
                                                      attr-id="${value.id}"
                                                      >
                                                      <i class="fas fa-trash-alt"></i>
                                                      </button>
                                                  </td>
                                                  </tr>`)
                      if(value.status == 0){
                         $('.bootstrap-switch-container[attr-id="'+value.id+'"]').css('margin-left',-29.3333);
                      }

                  });
                  paging_ajax("user", res);
                  delete_data_user();
                  load_side_menu();

                  $('.checkAll').click(function() {
                      if(!$(this).is(':checked')){
                          $('#checkboxTop').prop('checked', false);
                      }
                  })

                  $(".bootstrap-switch-container").click(function(e) {
                      var id = $(this).attr('attr-id');
                      var status = $(this).attr('attr-status');

                      if(status == 1){
                          $('.bootstrap-switch-container[attr-id="'+id+'"]').css('margin-left',-29.3333);
                          $(this).attr('attr-status',0);

                          $.ajax({
                              url: url_api+'users/status/' + id + "/" + 0,
                              type: 'POST',
                              dataType: 'json',
                              beforeSend: function(xhr) {
                                xhr.setRequestHeader("Authorization", "Basic " + btoa(usr + ":" + pwd));
                                xhr.setRequestHeader("commandid", "Users");
                                xhr.setRequestHeader("requestid", "users" + Date.now());
                                xhr.setRequestHeader("requestdt", datetime);
                                xhr.setRequestHeader("clientid", insurance_username);
                                xhr.setRequestHeader("signature", signature);
                              },
                              success: function (data, textStatus, xhr) {
                                  if(data.body.isupdate){
                                      toastr.success("Success Updated!", 'Success!');
                                  }else{
                                      var ret_message = "";
                                      $.each(data.body.message, function( index, value ) {
                                          ret_message += "<br>" + index + ": " + value;
                                      });
                                      toastr.error(ret_message, 'Error!');
                                  }
                              },
                              error: function (xhr, textStatus, errorThrown) {

                              }
                          });
                      }else{
                          $('.bootstrap-switch-container[attr-id="'+id+'"]').css('margin-left',0);
                          $(this).attr('attr-status',1);

                          $.ajax({
                              url: url_api+'users/status/' + id + "/" + 1,
                              type: 'POST',
                              dataType: 'json',
                              beforeSend: function(xhr) {
                                xhr.setRequestHeader("Authorization", "Basic " + btoa(usr + ":" + pwd));
                                xhr.setRequestHeader("commandid", "Users");
                                xhr.setRequestHeader("requestid", "users" + Date.now());
                                xhr.setRequestHeader("requestdt", datetime);
                                xhr.setRequestHeader("clientid", insurance_username);
                                xhr.setRequestHeader("signature", signature);
                              },
                              success: function (data, textStatus, xhr) {
                                  if(data.body.isupdate){
                                      toastr.success("Success Updated!", 'Success!');
                                  }else{
                                      var ret_message = "";
                                      $.each(data.body.message, function( index, value ) {
                                          ret_message += "<br>" + index + ": " + value;
                                      });
                                      toastr.error(ret_message, 'Error!');
                                  }
                              },
                              error: function (xhr, textStatus, errorThrown) {

                              }
                          });
                      }
                  });

              },
              error: function (xhr, textStatus, errorThrown) {

              }
          }).done(function() {
              if(lang == 2){
                  $('.th-user-fullname').text('Nama Lengkap');
                  $('.th-user-username').text('Nama Pengguna');
                  $('.th-user-phone').text('Telepon');
                  $('.th-user-role').text('Peran');
                  $('.th-user-caller').text('Asuransi');
                  $('.th-user-act').text('Aksi');
              }
          });
      }

      function save_data_user() {
          var options = {
              url: url_api+'users',
              type: 'POST',
              beforeSend: function(xhr) {
                  xhr.setRequestHeader("Authorization", "Basic " + btoa(usr + ":" + pwd));
                  xhr.setRequestHeader("commandid", "users");
                  xhr.setRequestHeader("requestid", "users" + Date.now());
                  xhr.setRequestHeader("requestdt", datetime);
                  xhr.setRequestHeader("clientid", "insurancefile");
                  xhr.setRequestHeader("signature", signature);
              },
              success: function (data, textStatus, xhr) {
                  if(data.body.isinsert){
                      $('#usermodalcreate').modal('toggle');
                      table_get_user();
                      toastr.success("Success Saved!", 'Success!');
                  }else{
                      var ret_message = "";
                      $.each(data.body.message, function( index, value ) {
                          ret_message += "<br>" + index + ": " + value;
                      });
                      toastr.error(ret_message, 'Error!');
                  }
              }
          };

          $('.user_create_form').submit(function() {
              // inside event callbacks 'this' is the DOM element so we first
              // wrap it in a jQuery object and then invoke ajaxSubmit
              $(this).ajaxSubmit(options);

              // !!! Important !!!
              // always return false to prevent standard browser submit and page navigation
              return false;
          });
      }

      function delete_data_user() {
          $('.user-btn-delete').unbind('click')
          $('.user-btn-delete').click(function() {
              var id_param = $(this).attr('attr-id');
              swal({
                  title: "Are you sure?",
                  text: "Are you sure for delete this data ?",
                  icon: "warning",
                  buttons: ["No","Yes"],
                  dangerMode: true
              }).then(function(isConfirm) {
                  if (isConfirm) {
                      $.ajax({
                          url: url_api+'users/' + id_param,
                          type: 'DELETE',
                          dataType: 'json',
                          beforeSend: function(xhr) {
                            xhr.setRequestHeader("Authorization", "Basic " + btoa(usr + ":" + pwd));
                            xhr.setRequestHeader("commandid", "Users");
                            xhr.setRequestHeader("requestid", "users" + Date.now());
                            xhr.setRequestHeader("requestdt", datetime);
                            xhr.setRequestHeader("clientid", insurance_username);
                            xhr.setRequestHeader("signature", signature);
                          },
                          success: function (data, textStatus, xhr) {
                              if(data.body.isdelete){
                                  table_get_user();
                                  toastr.success("Success Deleted!", 'Success!');
                              }else{
                                  var ret_message = "";
                                  $.each(data.body.message, function( index, value ) {
                                      ret_message += "<br>" + index + ": " + value;
                                  });
                                  toastr.error(ret_message, 'Error!');
                              }
                          },
                          error: function (xhr, textStatus, errorThrown) {

                          }
                      });
                  }
              })
          })
      }

      function user_filter(){
          $('.user-btn-filter-exe').unbind('click');
          $('.user-btn-filter-exe').click(function(e){
              e.preventDefault();
              var data = $('.user_form_filter').serialize();
              table_get_user(data);
              $('#usermodalfilter').modal('toggle');
          });
      }
</script>
