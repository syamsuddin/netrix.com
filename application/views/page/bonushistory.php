<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="tab-content tabcontent-border p-20" id="myTabContent">
                    <div role="tabpanel" class="tab-pane fade show active" id="home5" aria-labelledby="home-tab">
                        <div class="row">
                          <div class="col-5 align-self-center">
                              <div class="d-flex align-items-center">
                                  <nav aria-label="breadcrumb">
                                      <ol class="breadcrumb">
                                          <li class="breadcrumb-item sub-bonushistory">Bonus History</li>
                                      </ol>
                                  </nav>
                              </div>
                          </div>
                        </div>
                        <hr />
                        <!-- Row -->
                        <div class="row">
                            <div class="col-md-12" align="right">
                                <button type="button" class="btn btn-warning bonushistory-btn-filter sub-filter" data-toggle="modal" data-target="#bonushistorymodalfilter">
                                    Filter
                                </button>
                                <button type="button" class="btn btn-info bonushistory-btn-refresh sub-refresh" onclick="refresh_data('bonushistory')">
                                    Refresh
                                </button>
                            </div>

                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table style="margin-top:7px;" id="table-bonushistory" class="table table-bordered nowrap display table-striped">
                                        <thead>
                                            <tr>
                                                <th><strong>K-Link Member ID</strong></th>
                                                <th><strong>Total Premium</strong></th>
                                                <th><strong>Bonus Personal</strong></th>
                                                <th><strong>Activation Bonus</strong></th>
                                                <th><strong>Network Bonus</strong></th>
                                                <th><strong>Total Gross Bonus</strong></th>
                                                <th><strong>Tax Percentage</strong></th>
                                                <th><strong>Tax Amount</strong></th>
                                                <th><strong>Total Nett Bonus</strong></th>
                                                <th><strong>PBV</strong></th>
                                                <th><strong>Min PBV</strong></th>
                                                <th><strong>PBV Diff</strong></th>
                                                <th><strong>Conversion Rate</strong></th>
                                                <th><strong>Amount PBV Diff</strong></th>
                                                <th><strong>Bonus Deviation</strong></th>
                                                <th><strong>Transfer Amount</strong></th>
                                                <th><strong>E-Voucher</strong></th>
                                            </tr>
                                        </thead>
                                        <tbody class="bonushistory-api-list">

                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <div class="col-md-12" style="margin-top:7px;">
                                <form class="paging_form_bonushistory" method="GET">
                                    <div class="ikut-table-bonushistory row" style="display: none;">
                                        <div class="col-md-3">
                                            <div class="total-data-get-bonushistory form-control"></div>
                                        </div>

                                        <div class="col-md-9">
                                            <ul class="pagination float-right">
                                                <li>
                                                    <select class="form-control change-num-records-bonushistory" name="item_per_page">
                                                        <option value="10">10</option>
                                                        <option value="20">20</option>
                                                        <option value="50">50</option>
                                                        <option value="100">100</option>
                                                    </select>
                                                </li>
                                                <li style="margin-right: 15px;">
                                                    <div style="padding: .375rem .75rem;" class="sub-records">
                                                        Records
                                                    </div>
                                                </li>
                                                <li class="footable-page-arrow page-item first-page-bonushistory">
                                                    <a data-page="first" class="page-link link-first-page-bonushistory">«</a>
                                                </li>
                                                <li class="footable-page-arrow page-item prev-page-bonushistory" style="padding-right: .75rem;">
                                                    <a data-page="prev" class="page-link link-prev-page-bonushistory">‹</a>
                                                </li>
                                                <li>
                                                    <input type="text" class="form-control this-page-get-bonushistory" name="page" style="width: 40px;">
                                                </li>
                                                <li>
                                                    <div class="total-page-get-bonushistory" style="padding: .375rem .75rem;"></div>
                                                </li>
                                                <li>
                                                    <btn type="button" class="btn btn-success go-page-bonushistory sub-go">Go</btn>
                                                </li>
                                                <li class="footable-page-arrow page-item next-page-bonushistory" style="padding-left: .75rem;">
                                                    <a data-page="next" class="page-link link-next-page-bonushistory">›</a>
                                                </li>
                                                <li class="footable-page-arrow page-item last-page-bonushistory">
                                                    <a data-page="last" class="page-link link-last-page-bonushistory">»</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                        <!--Filter Modal -->
                        <div class="modal fade" id="bonushistorymodalfilter" tabindex="-1" role="dialog" aria-labelledby="filterModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <form class="bonushistory_form_filter" method="POST">
                                        <div class="modal-header">
                                            <h5 class="modal-title sub-filter" id="filterModalLabel">
                                              Filter
                                            </h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text sub-label-ins" style="min-width: 125px;">Insurance</span>
                                                </div>
                                                <select class="form-control insurance-api-list-bonushistory" name="caller">
                                                    <option>ALL</option>
                                                </select>
                                            </div>
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" style="min-width: 125px;">Month</span>
                                                </div>
                                                <input class="form-control" type="month" id="start" name="start" min="2018-03" value="">
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button class="btn btn-warning bonushistory-btn-reset-filter sub-reset">
                                                Reset
                                            </button>

                                            <button type="submit" class="btn btn-success bonushistory-btn-filter-exe sub-submit">
                                                Submit
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
  <script>
        var this_page_bonushistory = 1;
        var total_page_bonushistory = 1;

        function bonushistory_ready() {
            table_get_bonushistory();
            bonushistory_filter();
            paging("bonushistory");

            $('.bonushistory-btn-reset-filter').unbind('click')
            $('.bonushistory-btn-reset-filter').click(function(e) {
                e.preventDefault();
                $('.bonushistory_form_filter').trigger("reset");
                $('#bonushistorymodalfilter').modal('toggle');
                var data = $('.bonushistory_form_filter').serialize();
                table_get_bonushistory(data);
            })

            $.ajax({
                url: url_api+'insurance',
                type: 'GET',
                dataType: 'json',
                beforeSend: function(xhr) {
                  xhr.setRequestHeader("Authorization", "Basic " + btoa(usr + ":" + pwd));
                  xhr.setRequestHeader("commandid", "insurance List");
                  xhr.setRequestHeader("requestid", "insurancelist" + Date.now());
                  xhr.setRequestHeader("requestdt", datetime);
                  xhr.setRequestHeader("clientid", insurance_username);
                  xhr.setRequestHeader("signature", signature);
                },
                success: function (res, textStatus, xhr) {
                    res.body.data.map(function(value) {
                        $('.insurance-api-list-bonushistory').append(`<option value="${value.id}">${value.name}</option>`);
                    })
                },
                error: function (xhr, textStatus, errorThrown) {

                }
            });
        }

        function table_get_bonushistory(data) {
            $('.bonushistory-api-list').html('');
            $.ajax({
                url: url_api+'bonushistory',
                type: 'GET',
				data: data,
                dataType: 'json',
                beforeSend: function(xhr) {
                  xhr.setRequestHeader("Authorization", "Basic " + btoa(usr + ":" + pwd));
                  xhr.setRequestHeader("commandid", "bonushistory");
                  xhr.setRequestHeader("requestid", "bonushistory" + Date.now());
                  xhr.setRequestHeader("requestdt", datetime);
                  xhr.setRequestHeader("clientid", insurance_username);
                  xhr.setRequestHeader("signature", signature);
                },
                success: function (res, textStatus, xhr) {
					$('.bonushistory-api-list').html('');
                    res.body.data.map(function(value) {
						var detail_button=`<a href="detailcalc.html" class="trigger-side-menu menu-detailcalc btn btn-primary" side-attr-id="${value.id}">
							<i class="fas fa-list"></i><span class="title-detailcalc" attr-id="${value.id}" style="display:none;">Bonus Calculation Detail - ${value.id}</span>
						  </a>`;

						var detail_summary_button = `<a href="detailsummary.html" class="trigger-side-menu menu-detailsummary btn btn-success" side-attr-id="${value.id}">
							<i class="fas fa-list"></i><span class="title-detailsummary" attr-id="${value.id}" style="display:none;">Summary Detail - ${value.id}</span>
						  </a>`;

						var detail_history_button = `<a href="detailhistory.html" class="trigger-side-menu menu-detailhistory btn btn-warning" side-attr-id="${value.id}">
							<i class="fas fa-list"></i><span class="title-detailhistory" attr-id="${value.id}" style="display:none;">History Detail - ${value.id}</span>
						  </a>`;

                        $('.bonushistory-api-list').append(`<tr>
                                                    <td>${value.k_link_member_id}</td>
                                                    <td align="right">${Number(value.total_premium).toLocaleString('id')}</td>
                                                    <td align="right">${Number(value.total_bonus_personal).toLocaleString('id')}</td>
                                                    <td align="right">${Number(value.total_bonus_activation).toLocaleString('id')}</td>
                                                    <td align="right">${Number(value.total_bonus_network).toLocaleString('id')}</td>
                                                    <td align="right">${Number(value.total_bonus_gross).toLocaleString('id')}</td>
                                                    <td align="right">${Number(value.tax_percentage).toLocaleString('id')}</td>
                                                    <td align="right">${Number(value.tax_amount).toLocaleString('id')}</td>
                                                    <td align="right">${Number(value.nett_bonus).toLocaleString('id')}</td>
                                                    <td align="right">${Number(value.pbv).toLocaleString('id')}</td>
                                                    <td align="right">${Number(value.pbv_min).toLocaleString('id')}</td>
                                                    <td align="right">${Number(value.pbv_diff).toLocaleString('id')}</td>
                                                    <td align="right">${Number(value.conversion_rate).toLocaleString('id')}</td>
                                                    <td align="right">${Number(value.conversion_rate).toLocaleString('id')}</td>
                                                    <td align="right">${Number(value.bonus_deviation).toLocaleString('id')}</td>
                                                    <td align="right">${Number(value.transfer_amount).toLocaleString('id')}</td>
                                                    <td align="right">${Number(value.evoucher).toLocaleString('id')}</td>
                                                    <td>${value.status}</td>
                                                    <td>
														${detail_button}
														${detail_summary_button}
														${detail_history_button}
                                                    </td>
                                                    </tr>`)
                    });
                    total_page_bonushistory = res.body.pagination.total_page;
                    paging_ajax("bonushistory", res);
                    delete_data_bonushistory();
					approved_data_bonushistory();
					void_data_bonushistory();
					load_side_menu();
                },
                error: function (xhr, textStatus, errorThrown) {

                }
            });
        }

        function delete_data_bonushistory() {
            $('.bonushistory-btn-delete').unbind('click')
            $('.bonushistory-btn-delete').click(function() {
                var id_param = $(this).attr('attr-id');
                swal({
                    title: "Are you sure?",
                    text: "Are you sure for delete this data ?",
                    icon: "warning",
                    buttons: ["No","Yes"],
                    dangerMode: true
                }).then(function(isConfirm) {
                    if (isConfirm) {
                        $.ajax({
                            url: url_api+'bonushistory/' + id_param,
                            type: 'DELETE',
                            dataType: 'json',
                            beforeSend: function(xhr) {
                              xhr.setRequestHeader("Authorization", "Basic " + btoa(usr + ":" + pwd));
                              xhr.setRequestHeader("commandid", "bonushistory");
                              xhr.setRequestHeader("requestid", "bonushistory" + Date.now());
                              xhr.setRequestHeader("requestdt", datetime);
                              xhr.setRequestHeader("clientid", insurance_username);
                              xhr.setRequestHeader("signature", signature);
                            },
                            success: function (data, textStatus, xhr) {
                                if(data.body.isdelete){
                                    table_get_bonushistory();
                                }else{
                                    var ret_message = "";
                                    $.each(data.body.message, function( index, value ) {
                                        ret_message += "<br>" + index + ": " + value;
                                    });
                                    toastr.error(ret_message, 'Error!');
                                }
                            },
                            error: function (xhr, textStatus, errorThrown) {

                            }
                        });
                    }
                })
            })
        }

        function approved_data_bonushistory() {
            $('.bonushistory-btn-approved').unbind('click');
            $('.bonushistory-btn-approved').click(function(e) {
                e.preventDefault();
				var id_param = $(this).attr('attr-id');
                var data = $('.bonushistory_form_update').serialize();
                $.ajax({
                    url: url_api+'bonushistory/update/'+id_param+'/8',
                    type: 'GET',
                    dataType: 'json',
                    data:data,
                    beforeSend: function(xhr) {
                      xhr.setRequestHeader("Authorization", "Basic " + btoa(usr + ":" + pwd));
                      xhr.setRequestHeader("commandid", "bonushistory");
                      xhr.setRequestHeader("requestid", "bonushistory" + Date.now());
                      xhr.setRequestHeader("requestdt", datetime);
                      xhr.setRequestHeader("clientid", insurance_username);
                      xhr.setRequestHeader("signature", signature);
                    },
                    success: function (data, textStatus, xhr) {
                        if(data.body.isupdate){
                            table_get_bonushistory();
                            toastr.success("Success Approved!", 'Success!');
                        }else{
                            var ret_message = "";
                            $.each(data.body.message, function( index, value ) {
                                ret_message += "<br>" + index + ": " + value;
                            });
                            toastr.error(ret_message, 'Error!');
                        }
                    },
                    error: function (xhr, textStatus, errorThrown) {

                    }
                });
            })
        }

		function void_data_bonushistory() {
            $('.bonushistory-btn-void').unbind('click');
            $('.bonushistory-btn-void').click(function(e) {
                e.preventDefault();
				var id_param = $(this).attr('attr-id');
                var data = $('.bonushistory_form_update').serialize();
                $.ajax({
                    url: url_api+'bonushistory/update/'+id_param+'/9',
                    type: 'GET',
                    dataType: 'json',
                    data:data,
                    beforeSend: function(xhr) {
                      xhr.setRequestHeader("Authorization", "Basic " + btoa(usr + ":" + pwd));
                      xhr.setRequestHeader("commandid", "bonushistory");
                      xhr.setRequestHeader("requestid", "bonushistory" + Date.now());
                      xhr.setRequestHeader("requestdt", datetime);
                      xhr.setRequestHeader("clientid", insurance_username);
                      xhr.setRequestHeader("signature", signature);
                    },
                    success: function (data, textStatus, xhr) {
                        if(data.body.isupdate){
                            table_get_bonushistory();
                            toastr.success("Success Void!", 'Success!');
                        }else{
                            var ret_message = "";
                            $.each(data.body.message, function( index, value ) {
                                ret_message += "<br>" + index + ": " + value;
                            });
                            toastr.error(ret_message, 'Error!');
                        }
                    },
                    error: function (xhr, textStatus, errorThrown) {

                    }
                });
            })
        }

        function bonushistory_filter(){
            $('.bonushistory-btn-filter-exe').unbind('click');
            $('.bonushistory-btn-filter-exe').click(function(e){
                e.preventDefault();
                var data = $('.bonushistory_form_filter').serialize();
                table_get_bonushistory(data);
                $('#bonushistorymodalfilter').modal('toggle');
            });
        }
  </script>
