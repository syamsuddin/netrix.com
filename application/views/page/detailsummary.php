<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="tab-content tabcontent-border p-20" id="myTabContent">
                    <div role="tabpanel" class="tab-pane fade show active" id="home5" aria-labelledby="home-tab">
                        <div class="row">
                          <div class="col-5 align-self-center">
                              <div class="d-flex align-items-center">
                                  <nav aria-label="breadcrumb">
                                      <ol class="breadcrumb">
                                          <li class="breadcrumb-item">Summary Detail</li>
                                      </ol>
                                  </nav>
                              </div>
                          </div>
                        </div>
                        <hr />
                        <!-- Row -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table style="margin-top:7px;" class="table table-bordered nowrap display table-striped">
                                        <thead>
                                            <tr>
                                                <th><strong>K-Link Member ID</strong></th>
                                                <th><strong>k-Link Member Name</strong></th>
                                                <th><strong>Total Premium</strong></th>
                                                <th><strong>Bonus Personal</strong></th>
                                                <th><strong>Total Gross Commission</strong></th>
                                                <th><strong>Total Commission</strong></th>
                                                <th><strong>Activition Bonus</strong></th>
                                                <th><strong>Network Bonus Level 1</strong></th>
                                                <th><strong>Network Bonus Level 2</strong></th>
                                                <th><strong>Network Bonus Level 3</strong></th>
                                                <th><strong>Network Bonus Level 4</strong></th>
                                                <th><strong>Network Bonus Level 5</strong></th>
                                                <th><strong>Network Bonus Level 6</strong></th>
                                                <th><strong>Network Bonus Level 7</strong></th>
                                                <th><strong>Network Bonus Level 8</strong></th>
                                                <th><strong>Network Bonus Level 9</strong></th>
                                                <th><strong>Total Bonus</strong></th>
                                            </tr>
                                        </thead>
                                        <tbody class="detailsummary-api-list">

                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <div class="col-md-12" style="margin-top:7px;">
                                <form class="paging_form_detailsummary" method="GET">
                                    <div class="ikut-table-detailsummary row" style="display: none;">
                                        <div class="col-md-3">
                                            <div class="total-data-get-detailsummary form-control"></div>
                                        </div>

                                        <div class="col-md-9">
                                            <ul class="pagination float-right">
                                                <li>
                                                    <select class="form-control change-num-records-detailsummary" name="item_per_page">
                                                        <option value="10">10</option>
                                                        <option value="20">20</option>
                                                        <option value="50">50</option>
                                                        <option value="100">100</option>
                                                    </select>
                                                </li>
                                                <li style="margin-right: 15px;">
                                                    <div style="padding: .375rem .75rem;">
                                                        Records
                                                    </div>
                                                </li>
                                                <li class="footable-page-arrow page-item first-page-detailsummary">
                                                    <a data-page="first" class="page-link link-first-page-detailsummary">«</a>
                                                </li>
                                                <li class="footable-page-arrow page-item prev-page-detailsummary" style="padding-right: .75rem;">
                                                    <a data-page="prev" class="page-link link-prev-page-detailsummary">‹</a>
                                                </li>
                                                <li>
                                                    <input type="text" class="form-control this-page-get-detailsummary" name="page" style="width: 40px;">
                                                </li>
                                                <li>
                                                    <div class="total-page-get-detailsummary" style="padding: .375rem .75rem;"></div>
                                                </li>
                                                <li>
                                                    <btn type="button" class="btn btn-success go-page-detailsummary">Go</btn>
                                                </li>
                                                <li class="footable-page-arrow page-item next-page-detailsummary" style="padding-left: .75rem;">
                                                    <a data-page="next" class="page-link link-next-page-detailsummary">›</a>
                                                </li>
                                                <li class="footable-page-arrow page-item last-page-detailsummary">
                                                    <a data-page="last" class="page-link link-last-page-detailsummary">»</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                        <!--Filter Modal -->
                        <div class="modal fade" id="detailsummarymodalfilter" tabindex="-1" role="dialog" aria-labelledby="filterModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <form class="detailsummary1_form_filter" method="POST">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="filterModalLabel">
                                              <i class="fas fa-insurancefile-plus m-r-10"></i> Filter
                                            </h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" style="min-width: 125px;">Date Range</span>
                                                </div>
                                                <input type='text' class="form-control datetime-filter-insurancefile" name="date" value=""/>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <div class="col-md-6">
                                                <button class="btn btn-warning insurancefile-btn-reset-filter">
                                                    <i class="ti-loop"></i> Reset
                                                </button>
                                            </div>

                                            <div class="col-md-6" align="right">
                                                <button type="submit" class="btn btn-success insurancefile-btn-filter-exe">
                                                    <i class="ti-save"></i> Submit
                                                </button>
                                            </div>

                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
      var this_page_detailsummary = 1;
      var total_page_detailsummary = 1;
      var id_db_detailsummary = '';

      function detailsummary_ready(id) {
          var data;
          id_db_detailsummary = id;
          table_get_detailsummary(data,id_db_detailsummary);
          paging("detailsummary");
      }

      function table_get_detailsummary(data,id) {
          if(id == undefined){
              id = id_db_detailsummary;
          }

          $('.detailsummary-api-list').html('')
          $.ajax({
              url: url_api+'bonuscalc/detailsummary/'+id,
              type: 'GET',
              data: data,
              dataType: 'json',
              beforeSend: function(xhr) {
                xhr.setRequestHeader("Authorization", "Basic " + btoa(usr + ":" + pwd));
                xhr.setRequestHeader("commandid", "detailsummary");
                xhr.setRequestHeader("requestid", "detailsummary" + Date.now());
                xhr.setRequestHeader("requestdt", datetime);
                xhr.setRequestHeader("clientid", insurance_username);
                xhr.setRequestHeader("signature", signature);
              },
              success: function (res, textStatus, xhr) {
				  $('.detailsummary-api-list').html('');
                  res.body.data.map(function(value) {

					  var x;
					  for(x = 1; x < 10; x++){
						  if(eval("value.total_bn_" + x) == null){
							  eval("value.total_bn_" + x + " = 0");
						  }
					  }

					  var total_bonus = parseInt(value.total_bn_1) + parseInt(value.total_bn_2) + parseInt(value.total_bn_3) + parseInt(value.total_bn_4) + parseInt(value.total_bn_5) + parseInt(value.total_bn_6) + parseInt(value.total_bn_7) + parseInt(value.total_bn_8) + parseInt(value.total_bn_9);

					  $('.detailsummary-api-list').append(`<tr>
                                                  <td>${value.k_link_member_id}</td>
                                                  <td>${value.k_link_member_name}</td>
                                                  <td align="right">${Number(value.ins_premium).toLocaleString('id')}</td>
                                                  <td align="right">${Number(value.ins_personal_bonus).toLocaleString('id')}</td>
                                                  <td align="right">${Number(value.ins_premium).toLocaleString('id')}</td>
                                                  <td align="right">${Number(value.ins_gross_commission).toLocaleString('id')}</td>
                                                  <td align="right">${Number(value.total_br).toLocaleString('id')}</td>
                                                  <td align="right">${Number(value.network_bonus_level_1).toLocaleString('id')}</td>
                                                  <td align="right">${Number(value.network_bonus_level_2).toLocaleString('id')}</td>
                                                  <td align="right">${Number(value.network_bonus_level_3).toLocaleString('id')}</td>
                                                  <td align="right">${Number(value.network_bonus_level_4).toLocaleString('id')}</td>
                                                  <td align="right">${Number(value.network_bonus_level_5).toLocaleString('id')}</td>
                                                  <td align="right">${Number(value.network_bonus_level_6).toLocaleString('id')}</td>
                                                  <td align="right">${Number(value.network_bonus_level_7).toLocaleString('id')}</td>
                                                  <td align="right">${Number(value.network_bonus_level_8).toLocaleString('id')}</td>
                                                  <td align="right">${Number(value.network_bonus_level_9).toLocaleString('id')}</td>
                                                  <td align="right">${Number(total_bonus).toLocaleString('id')}</td>
                                                  </tr>`)
                  });
                  total_page_detailsummary = res.body.pagination.total_page;
                  paging_ajax("detailsummary", res);
              },
              error: function (xhr, textStatus, errorThrown) {

              }
          });
      }
</script>
