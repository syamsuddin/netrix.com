<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="tab-content tabcontent-border p-20" id="myTabContent">
                    <div role="tabpanel" class="tab-pane fade show active" id="home5" aria-labelledby="home-tab">
                        <div class="row">
                          <div class="col-5 align-self-center">
                              <div class="d-flex align-items-center">
                                  <nav aria-label="breadcrumb">
                                      <ol class="breadcrumb">
                                          <li class="breadcrumb-item sub-bonuspay">Bonus Payment</li>
                                      </ol>
                                  </nav>
                              </div>
                          </div>
                        </div>
                        <hr />
                        <!-- Row -->
                        <div class="row">
                            <div class="col-md-3">
                                <button type="button" class="btn btn-success bonuspay-btn-new sub-generate" data-toggle="modal" data-target="#bonuspaymodalcreate">
                                    Generate
                                </button>
                            </div>

                            <div class="col-md-9" align="right">
                                <button type="button" class="btn btn-warning bonuspay-btn-filter sub-filter" data-toggle="modal" data-target="#bonuspaymodalfilter">
                                    Filter
                                </button>
                                <button type="button" class="btn btn-info bonuspay-btn-refresh sub-refresh" onclick="refresh_data('bonuspay')">
                                    Refresh
                                </button>
                            </div>

                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table style="margin-top:7px;" id="table-bonuspay" class="table table-bordered nowrap display table-striped">
                                        <thead>
                                            <tr>
                                                <th><strong>ID</strong></th>
                                                <th><strong>PERIOD</strong></th>
                                                <th><strong>INSURANCE #</strong></th>
                                                <th><strong>MEMBER #</strong></th>
                                                <th><strong>TOTAL PREMIUM</strong></th>
                                                <th><strong>BONUS PERSONAL</strong></th>
                                                <th><strong>ACTIVATION BONUS</strong></th>
                                                <th><strong>NETWORK BONUS</strong></th>
                                                <th><strong>TOTAL GROSS BONUS</strong></th>
                                                <th><strong>PROCESS</strong></th>
                                                <th width="20%"><strong>ACTION</strong></th>
                                            </tr>
                                        </thead>
                                        <tbody class="bonuspay-api-list">

                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <div class="col-md-12" style="margin-top:7px;">
                                <form class="paging_form_bonuspay" method="GET">
                                    <div class="ikut-table-bonuspay row" style="display: none;">
                                        <div class="col-md-3">
                                            <div class="total-data-get-bonuspay form-control"></div>
                                        </div>

                                        <div class="col-md-9">
                                            <ul class="pagination float-right">
                                                <li>
                                                    <select class="form-control change-num-records-bonuspay" name="item_per_page">
                                                        <option value="10">10</option>
                                                        <option value="20">20</option>
                                                        <option value="50">50</option>
                                                        <option value="100">100</option>
                                                    </select>
                                                </li>
                                                <li style="margin-right: 15px;">
                                                    <div style="padding: .375rem .75rem;" class="sub-records">
                                                        Records
                                                    </div>
                                                </li>
                                                <li class="footable-page-arrow page-item first-page-bonuspay">
                                                    <a data-page="first" class="page-link link-first-page-bonuspay">«</a>
                                                </li>
                                                <li class="footable-page-arrow page-item prev-page-bonuspay" style="padding-right: .75rem;">
                                                    <a data-page="prev" class="page-link link-prev-page-bonuspay">‹</a>
                                                </li>
                                                <li>
                                                    <input type="text" class="form-control this-page-get-bonuspay" name="page" style="width: 40px;">
                                                </li>
                                                <li>
                                                    <div class="total-page-get-bonuspay" style="padding: .375rem .75rem;"></div>
                                                </li>
                                                <li>
                                                    <btn type="button" class="btn btn-success go-page-bonuspay sub-go">Go</btn>
                                                </li>
                                                <li class="footable-page-arrow page-item next-page-bonuspay" style="padding-left: .75rem;">
                                                    <a data-page="next" class="page-link link-next-page-bonuspay">›</a>
                                                </li>
                                                <li class="footable-page-arrow page-item last-page-bonuspay">
                                                    <a data-page="last" class="page-link link-last-page-bonuspay">»</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                        <!--Create New Modal -->
                        <div class="modal fade" id="bonuspaymodalcreate" tabindex="-1" role="dialog" aria-labelledby="createModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <form class="bonuspay_form" method="POST" enctype="multipart/form-data" role="form">
                                        <div class="modal-header">
                                            <h5 class="modal-title sub-generate" id="createModalLabel">
                                               Generate
                                            </h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text sub-label-file-period" style="min-width: 125px;">File Period</span>
                                                </div>
                                                <input class="form-control" id="bonuspay_period_select" type="month" name="start" min="2018-03" value="<?php echo date(" Y-m ");?>">
                                            </div>
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text sub-label-ins" style="min-width: 125px;">Type</span>
                                                </div>
                                                <select class="form-control" name="tax">
                                                    <option value="INTERNAL">INTERNAL</option>
                                                    <option value="EXTERNAL">EXTERNAL</option>
                                                </select>
                                            </div>
                                            <div class="form-group file-calc-api-bonuspay" align="center">

                                            </div>

                                        </div>
                                        <div class="modal-footer">
                                            <button type="submit" class="btn btn-success bonuspay-btn-save sub-save">
                                                Save
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        <!--Filter Modal -->
                        <div class="modal fade" id="bonuspaymodalfilter" tabindex="-1" role="dialog" aria-labelledby="filterModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <form class="bonuspay_form_filter" method="POST">
                                        <div class="modal-header">
                                            <h5 class="modal-title sub-filter" id="filterModalLabel">
                                              Filter
                                            </h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" style="min-width: 125px;">Period</span>
                                                </div>
                                                <input class="form-control" type="month" id="start" name="start" min="2018-03" value="">
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button class="btn btn-warning bonuspay-btn-reset-filter sub-reset">
                                                Reset
                                            </button>

                                            <button type="submit" class="btn btn-success bonuspay-btn-filter-exe sub-submit">
                                                Submit
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
  <script>
        var this_page_bonuspay = 1;
        var total_page_bonuspay = 1;

        function bonuspay_ready() {
            table_get_bonuspay();
            bonuspay_filter();
            save_data_bonuspay();
            paging("bonuspay");

            $('.bonuspay-btn-reset-filter').unbind('click')
            $('.bonuspay-btn-reset-filter').click(function(e) {
                e.preventDefault();
                $('.bonuspay_form_filter').trigger("reset");
                $('#bonuspaymodalfilter').modal('toggle');
                var data = $('.bonuspay_form_filter').serialize();
                table_get_bonuspay(data);
            })

            $('.bonuspay-btn-new').click(function(e) {
                $('.bonuspay_form').trigger("reset");
                $('.file-calc-api-bonuspay').html('');
            });

            $('#bonuspay_period_select').change(function(e) {
                var data = $('.bonuspay_form').serialize();

                $.ajax({
                    url: url_api+'bonuspay/listcalc',
                    type: 'GET',
                    data: data,
                    dataType: 'json',
                    beforeSend: function(xhr) {
                      xhr.setRequestHeader("Authorization", "Basic " + btoa(usr + ":" + pwd));
                      xhr.setRequestHeader("commandid", "List Calculation");
                      xhr.setRequestHeader("requestid", "listcalculation" + Date.now());
                      xhr.setRequestHeader("requestdt", datetime);
                      xhr.setRequestHeader("clientid", insurance_username);
                      xhr.setRequestHeader("signature", signature);
                    },
                    success: function (res, textStatus, xhr) {
      				  $('.file-calc-api-bonuspay').html('');
                        if(res.body.data.length == 0){
                            if(lang == 2){
                                var res_fc = "Berkas komisi untuk asuransi dan periode tersebut tidak ada";
                            }else{
                                var res_fc = "File comission not found for this insurance and period";
                            }

                            $('.file-calc-api-bonuspay').append(`
                               <p>${res_fc}</p>
                            `);
                        }else{
                            res.body.data.map(function(value) {
                                $('.file-calc-api-bonuspay').append(`
                                    <div class="checkbox">
                                      <label>
                                        <input type="checkbox" value="${value.gen_id}" name="calc_file[]">
                                      </label>
                                      ${value.caller_name} - ${value.gen_period}
                                    </div>
                                `);
                            })
                        }
                    },
                    error: function (xhr, textStatus, errorThrown) {

                    }
                });
            });
        }

        function table_get_bonuspay(data) {
            $('.bonuspay-api-list').html('');
            $.ajax({
                url: url_api+'bonuspay',
                type: 'GET',
				data: data,
                dataType: 'json',
                beforeSend: function(xhr) {
                  xhr.setRequestHeader("Authorization", "Basic " + btoa(usr + ":" + pwd));
                  xhr.setRequestHeader("commandid", "bonuspay");
                  xhr.setRequestHeader("requestid", "bonuspay" + Date.now());
                  xhr.setRequestHeader("requestdt", datetime);
                  xhr.setRequestHeader("clientid", insurance_username);
                  xhr.setRequestHeader("signature", signature);
                },
                success: function (res, textStatus, xhr) {
					$('.bonuspay-api-list').html('');
                    res.body.data.map(function(value) {
						var detail_button=`<a href="detailpayment.html" class="trigger-side-menu menu-detailpayment btn btn-primary" side-attr-id="${value.id}">
							<i class="fas fa-list"></i><span class="title-detailpayment" attr-id="${value.id}" style="display:none;">Bonus Payment Detail - ${value.id}</span>
						  </a>`;

                        $('.bonuspay-api-list').append(`<tr>
                                                    <td>${value.payment_id}</td>
                                                    <td>${value.payment_period}</td>
                                                    <td align="right">${Number(value.count_insurance).toLocaleString('id')}</td>
                                                    <td align="right">${Number(value.count_k_link_member_id).toLocaleString('id')}</td>
                                                    <td align="right">${Number(value.total_premium).toLocaleString('id')}</td>
                                                    <td align="right">${Number(value.total_bonus_personal).toLocaleString('id')}</td>
                                                    <td align="right">${Number(value.total_bonus_activation).toLocaleString('id')}</td>
                                                    <td align="right">${Number(value.total_bonus_network).toLocaleString('id')}</td>
                                                    <td align="right">${Number(value.total_bonus_gross).toLocaleString('id')}</td>
                                                    <td>${value.status}</td>
                                                    <td>
														${detail_button}
                                                    </td>
                                                    </tr>`)
                    });
                    total_page_bonuspay = res.body.pagination.total_page;
                    paging_ajax("bonuspay", res);
                    delete_data_bonuspay();
					load_side_menu();
                },
                error: function (xhr, textStatus, errorThrown) {

                }
            });
        }

        function save_data_bonuspay() {
            var options = {
                url: url_api+'bonuspay',
                type: 'POST',
                beforeSend: function(xhr) {
                    xhr.setRequestHeader("Authorization", "Basic " + btoa(usr + ":" + pwd));
                    xhr.setRequestHeader("commandid", "bonuspay");
                    xhr.setRequestHeader("requestid", "bonuspay" + Date.now());
                    xhr.setRequestHeader("requestdt", datetime);
                    xhr.setRequestHeader("clientid", insurance_username);
                    xhr.setRequestHeader("signature", signature);
                },
                success: function (data, textStatus, xhr) {
                    if(data.body.isinsert){
                        $('#bonuspaymodalcreate').modal('toggle');
                        table_get_bonuspay();
                        toastr.success("Success Saved!", 'Success!');
                    }else{
                        var ret_message = "";
                        $.each(data.body.message, function( index, value ) {
                            ret_message += "<br>" + index + ": " + value;
                        });
                        toastr.error(ret_message, 'Error!');
                    }
                }
            };

            $('.bonuspay_form').submit(function() {
                // inside event callbacks 'this' is the DOM element so we first
                // wrap it in a jQuery object and then invoke ajaxSubmit
                $(this).ajaxSubmit(options);

                // !!! Important !!!
                // always return false to prevent standard browser submit and page navigation
                return false;
            });
        }

        function delete_data_bonuspay() {
            $('.bonuspay-btn-delete').unbind('click')
            $('.bonuspay-btn-delete').click(function() {
                var id_param = $(this).attr('attr-id');
                swal({
                    title: "Are you sure?",
                    text: "Are you sure for delete this data ?",
                    icon: "warning",
                    buttons: ["No","Yes"],
                    dangerMode: true
                }).then(function(isConfirm) {
                    if (isConfirm) {
                        $.ajax({
                            url: url_api+'bonuspay/' + id_param,
                            type: 'DELETE',
                            dataType: 'json',
                            beforeSend: function(xhr) {
                              xhr.setRequestHeader("Authorization", "Basic " + btoa(usr + ":" + pwd));
                              xhr.setRequestHeader("commandid", "bonuspay");
                              xhr.setRequestHeader("requestid", "bonuspay" + Date.now());
                              xhr.setRequestHeader("requestdt", datetime);
                              xhr.setRequestHeader("clientid", insurance_username);
                              xhr.setRequestHeader("signature", signature);
                            },
                            success: function (data, textStatus, xhr) {
                                if(data.body.isdelete){
                                    table_get_bonuspay();
                                }else{
                                    var ret_message = "";
                                    $.each(data.body.message, function( index, value ) {
                                        ret_message += "<br>" + index + ": " + value;
                                    });
                                    toastr.error(ret_message, 'Error!');
                                }
                            },
                            error: function (xhr, textStatus, errorThrown) {

                            }
                        });
                    }
                })
            })
        }

        function bonuspay_filter(){
            $('.bonuspay-btn-filter-exe').unbind('click');
            $('.bonuspay-btn-filter-exe').click(function(e){
                e.preventDefault();
                var data = $('.bonuspay_form_filter').serialize();
                table_get_bonuspay(data);
                $('#bonuspaymodalfilter').modal('toggle');
            });
        }
  </script>
