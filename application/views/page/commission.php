<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="tab-content tabcontent-border p-20" id="myTabContent">
                    <div role="tabpanel" class="tab-pane fade show active" id="home5" aria-labelledby="home-tab">
                        <div class="row">
                          <div class="col-5 align-self-center">
                              <div class="d-flex align-items-center">
                                  <nav aria-label="breadcrumb">
                                      <ol class="breadcrumb">
                                          <li class="breadcrumb-item sub-commission">Commission</li>
                                      </ol>
                                  </nav>
                              </div>
                          </div>
                        </div>
                        <hr />
                        <!-- Row -->
                        <div class="row">
                            <div class="col-md-3">
                                <button type="button" class="btn btn-success commission-btn-new sub-create-new" data-toggle="modal" data-target="#commissionmodalcreate">
                                    Create New
                                </button>
                            </div>

                            <div class="col-md-9" style="text-align: right;">
                                <button type="button" class="btn btn-warning commission-btn-filter sub-filter" data-toggle="modal" data-target="#commissionmodalfilter">
                                    Filter
                                </button>
                                <button type="button" class="btn btn-info commission-btn-refresh sub-refresh" onclick="refresh_data('commission','commission_form_filter')">
                                    Refresh
                                </button>
                            </div>

                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table style="margin-top:7px;" id="table-commission" class="table table-bordered nowrap display table-striped">
                                        <thead>
                                            <tr>
                                                <th><strong class="th-comm-id">ID</strong></th>
                                                <th><strong class="th-comm-period">PERIOD</strong></th>
                                                <th><strong class="th-comm-insurance">INSURANCE</strong></th>
                                                <th><strong class="th-comm-file">FILENAME</strong></th>
                                                <th><strong class="th-comm-channel">CHANNEL</strong></th>
                                                <th><strong class="th-comm-save">SAVETIME</strong></th>
                                                <th><strong class="th-comm-process">PROCESS</strong></th>
                                                <th><strong class="th-comm-act">ACTION</strong></th>
                                            </tr>
                                        </thead>
                                        <tbody class="commission-api-list">

                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <div class="col-md-12" style="margin-top:7px;">
                                <form class="paging_form_commission" method="GET">
                                    <div class="ikut-table-commission row" style="display: none;">
                                        <div class="col-md-3">
                                            <div class="total-data-get-commission form-control"></div>
                                        </div>

                                        <div class="col-md-9">
                                            <ul class="pagination float-right">
                                                <li>
                                                    <select class="form-control change-num-records-commission" name="item_per_page">
                                                        <option value="10">10</option>
                                                        <option value="20">20</option>
                                                        <option value="50">50</option>
                                                        <option value="100">100</option>
                                                    </select>
                                                </li>
                                                <li style="margin-right: 15px;">
                                                    <div style="padding: .375rem .75rem;" class="sub-records">
                                                        Records
                                                    </div>
                                                </li>
                                                <li class="footable-page-arrow page-item first-page-commission">
                                                    <a data-page="first" class="page-link link-first-page-commission">«</a>
                                                </li>
                                                <li class="footable-page-arrow page-item prev-page-commission" style="padding-right: .75rem;">
                                                    <a data-page="prev" class="page-link link-prev-page-commission">‹</a>
                                                </li>
                                                <li>
                                                    <input type="text" class="form-control this-page-get-commission" name="page" style="width: 40px;">
                                                </li>
                                                <li>
                                                    <div class="total-page-get-commission" style="padding: .375rem .75rem;"></div>
                                                </li>
                                                <li>
                                                    <btn type="button" class="btn btn-success go-page-commission sub-go">Go</btn>
                                                </li>
                                                <li class="footable-page-arrow page-item next-page-commission" style="padding-left: .75rem;">
                                                    <a data-page="next" class="page-link link-next-page-commission">›</a>
                                                </li>
                                                <li class="footable-page-arrow page-item last-page-commission">
                                                    <a data-page="last" class="page-link link-last-page-commission">»</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                        <!--Create New Modal -->
                        <div class="modal fade" id="commissionmodalcreate" tabindex="-1" role="dialog" aria-labelledby="createModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <form class="commission_form" method="POST" enctype="multipart/form-data">
                                        <div class="modal-header">
                                            <h5 class="modal-title sub-upload-csv" id="createModalLabel">
                                              Upload File CSV
                                            </h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text sub-label-ins" style="min-width: 125px;">Insurance</span>
                                                </div>
                                                <select class="form-control insurance-api-list-commission" name="insurancefile_insurance">

                                                </select>
                                            </div>
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text sub-label-file-upload" style="min-width: 125px;">File Upload</span>
                                                </div>
                                                <input type="file" class="form-control" placeholder="Select file CSV" aria-label="name" name="insurancefile_file" id="commission-file" />
                                            </div>
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text sub-label-file-period" style="min-width: 125px;">File Period</span>
                                                </div>
                                                <input class="form-control" type="month" name="insurancefile_period" min="2018-03">
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="submit" class="btn btn-success commission-btn-save sub-save">
                                                Save
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        <!--Filter Modal -->
                        <div class="modal fade" id="commissionmodalfilter" tabindex="-1" role="dialog" aria-labelledby="filterModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <form class="commission_form_filter" method="POST">
                                        <div class="modal-header">
                                            <h5 class="modal-title sub-filter" id="filterModalLabel">
                                              Filter
                                            </h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text sub-label-ins" style="min-width: 125px;">Insurance</span>
                                                </div>
                                                <select class="form-control insurance-api-list-commission" name="caller">
                                                    <option>ALL</option>
                                                </select>
                                            </div>
											<div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text sub-label-file-name" style="min-width: 125px;">File Upload</span>
                                                </div>
                                                <input class="form-control" type="text" name="file_upload">
                                            </div>
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text sub-label-file-period" style="min-width: 125px;">File Period</span>
                                                </div>
                                                <input type="month" class="form-control" name="date" value=""/>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button class="btn btn-warning commission-btn-reset-filter sub-reset">
                                                Reset
                                            </button>

                                            <button type="submit" class="btn btn-success commission-btn-filter-exe sub-submit">
                                                Submit
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
      var this_page_commission = 1;
      var total_page_commission = 1;

    	$('.datetime-filter-commission').daterangepicker({
    		timePicker: false,
    		timePickerIncrement: 30,
            timePicker24Hour: true,
            autoUpdateInput: true,
    		locale: {
    			format: 'DD-MM-YYYY'
    		}
    	});

        $('.datetime-filter-commission').val('');

      function commission_ready() {
          table_get_commission();
          save_data_commission();
          commission_filter();
          paging("commission");

          $('.commission-btn-new').unbind('click')
          $('.commission-btn-new').click(function() {
              $('.commission_form').trigger("reset");
          })

          $('.commission-btn-reset-filter').unbind('click')
          $('.commission-btn-reset-filter').click(function(e) {
              e.preventDefault();
              $('.commission_form_filter').trigger("reset");
              $('#commissionmodalfilter').modal('toggle');
              var data = $('.commission_form_filter').serialize();
              table_get_commission(data);
          })

          $.ajax({
              url: url_api+'insurance',
              type: 'GET',
              dataType: 'json',
              beforeSend: function(xhr) {
                xhr.setRequestHeader("Authorization", "Basic " + btoa(usr + ":" + pwd));
                xhr.setRequestHeader("commandid", "insurance List");
                xhr.setRequestHeader("requestid", "insurancelist" + Date.now());
                xhr.setRequestHeader("requestdt", datetime);
                xhr.setRequestHeader("clientid", insurance_username);
                xhr.setRequestHeader("signature", signature);
              },
              success: function (res, textStatus, xhr) {
                  res.body.data.map(function(value) {
                      $('.insurance-api-list-commission').append(`<option value="${value.id}">${value.name}</option>`);
                  })
              },
              error: function (xhr, textStatus, errorThrown) {

              }
          });
      }

      function table_get_commission(data) {
          $('.commission-api-list').html('');
          $.ajax({
              url: url_api+'insurancefile',
              type: 'GET',
			  data: data,
              dataType: 'json',
              beforeSend: function(xhr) {
                xhr.setRequestHeader("Authorization", "Basic " + btoa(usr + ":" + pwd));
                xhr.setRequestHeader("commandid", "commission");
                xhr.setRequestHeader("requestid", "commission" + Date.now());
                xhr.setRequestHeader("requestdt", datetime);
                xhr.setRequestHeader("clientid", insurance_username);
                xhr.setRequestHeader("signature", signature);
              },
              success: function (res, textStatus, xhr) {
                  $('.commission-api-list').html('');
                  res.body.data.map(function(value) {
                      var detail_button = "";
                      var download_button = "";
                      var delete_button = "";
                      var sp_button = "";

                      if(value.status > 2){
                        detail_button =`<a href="detailcommission.html" class="trigger-side-menu menu-detailcommission btn btn-primary" side-attr-id="${value.id}" side-menu-active="commission.html">
                                          <i class="fas fa-list"></i><span class="title-detailcommission" attr-id="${value.id}" style="display:none;">Commission File (id : ${value.id})</span>
                                        </a>`;
                      }

                      if(value.status > 2 && value.status < 8){
                          download_button =`<a href="` + url_api + `uploads/success/${value.file_name}" target="_BLANK">
                                        <button type="button" class="btn btn-success commission-btn-download" attr-id="${value.id}">
                                          <i class="fas fa-download"></i>
                                        </button>
                                      </a>`;
                      }

                      if(value.status > 2 && value.status < 7){
                          delete_button =`<button
                                          type="button"
                                          class="btn btn-danger commission-btn-delete"
                                          attr-id="${value.id}"
                                          attr-file-name="${value.file_name}"
                                          attr-type="${value.status}"
                                          >
                                          <i class="fas fa-trash-alt"></i>
                                          </button>`;
                      }

                      if(value.status == 6){
                          sp_button = `<button type="button" class="btn btn-warning commission-btn-sp" attr-id="${value.id}">
                                        <i class="fas fa-play"></i>
                                       </button>`;
                      }

                      $('.commission-api-list').append(`<tr>
                                                  <td>${value.id}</td>
                                                  <td>${value.period}</td>
                                                  <td>${value.insurance_name}</td>
                                                  <td>${value.file_name}</td>
                                                  <td>${value.channel}</td>
                                                  <td>${value.save_time}</td>
                                                  <td>${value.process_name}</td>
                                                  <td>${sp_button}
                                                      ${detail_button}
                                                      ${download_button}
                                                      ${delete_button}
                                                  </td>
                                                  </tr>`)
                  });
                  total_page_commission = res.body.pagination.total_page;
                  paging_ajax("commission", res);
                  delete_data_commission();
                  sp_commission();
                  load_side_menu();
              },
              error: function (xhr, textStatus, errorThrown) {

              }
          }).done(function() {
              if(lang == 2){
                  $('.th-comm-id').text('ID');
                  $('.th-comm-insurance').text('Asuransi');
                  $('.th-comm-file').text('Nama Berkas');
                  $('.th-comm-period').text('Periode');
                  $('.th-comm-save').text('Waktu Simpan');
                  $('.th-comm-process').text('Proses');
                  $('.th-comm-act').text('Aksi');
              }
          });
      }

      function save_data_commission() {
          var options = {
              url: url_api+'insurancefile',
              type: 'POST',
              beforeSend: function(xhr) {
                  xhr.setRequestHeader("Authorization", "Basic " + btoa(usr + ":" + pwd));
                  xhr.setRequestHeader("commandid", "Agent Bonus File Details");
                  xhr.setRequestHeader("requestid", "commission" + Date.now());
                  xhr.setRequestHeader("requestdt", datetime);
                  xhr.setRequestHeader("requesturl", "http://localhost/netrix.com/receiver.php");
                  xhr.setRequestHeader("clientid", insurance_username);
                  xhr.setRequestHeader("signature", signature);
              },
              success: function (data, textStatus, xhr) {
                  if(data.body.isrecevied){
                      $('#commissionmodalcreate').modal('toggle');
                      table_get_commission();
                      toastr.success("Success Saved!", 'Success!');
                  }else{
                      var ret_message = "";
                      $.each(data.body.message, function( index, value ) {
                          ret_message += "<br>" + index + ": " + value;
                      });
                      toastr.error(ret_message, 'Error!');
                  }
              }
          };

          $('.commission_form').submit(function() {
              // inside event callbacks 'this' is the DOM element so we first
              // wrap it in a jQuery object and then invoke ajaxSubmit
              $(this).ajaxSubmit(options);

              // !!! Important !!!
              // always return false to prevent standard browser submit and page navigation
              return false;
          });
      }

      function delete_data_commission() {
          $('.commission-btn-delete').unbind('click')
          $('.commission-btn-delete').click(function() {
              var id_param = $(this).attr('attr-id');
              var file_name = $(this).attr('attr-file-name');
              var type = $(this).attr('attr-type');
              // var encodedData = window.btoa(file_name);
              var file_name = file_name.replace(".csv", "");;
              swal({
                  title: "Are you sure?",
                  text: "Are you sure for delete this data ?",
                  icon: "warning",
                  buttons: ["No","Yes"],
                  dangerMode: true
              }).then(function(isConfirm) {
                  if (isConfirm) {
                      $.ajax({
                          url: url_api+'insurancefile/' + id_param + '/' + file_name + "/" + type,
                          type: 'DELETE',
                          dataType: 'json',
                          beforeSend: function(xhr) {
                            xhr.setRequestHeader("Authorization", "Basic " + btoa(usr + ":" + pwd));
                            xhr.setRequestHeader("commandid", "commission");
                            xhr.setRequestHeader("requestid", "commission" + Date.now());
                            xhr.setRequestHeader("requestdt", datetime);
                            xhr.setRequestHeader("clientid", insurance_username);
                            xhr.setRequestHeader("signature", signature);
                          },
                          success: function (data, textStatus, xhr) {
                              if(data.body.isdelete){
                                  table_get_commission();
                                  toastr.success("Success Deleted!", 'Success!');
                              }else{
                                  var ret_message = "";
                                  $.each(data.body.message, function( index, value ) {
                                      ret_message += "<br>" + index + ": " + value;
                                  });
                                  toastr.error(ret_message, 'Error!');
                              }
                          },
                          error: function (xhr, textStatus, errorThrown) {

                          }
                      });
                  }
              })
          })
      }

      function sp_commission() {
          $('.commission-btn-sp').unbind('click')
          $('.commission-btn-sp').click(function() {
              var id_param = $(this).attr('attr-id');
              $.ajax({
                  url: url_api+'insurancefile/sp/' + id_param,
                  type: 'GET',
                  dataType: 'json',
                  beforeSend: function(xhr) {
                    xhr.setRequestHeader("Authorization", "Basic " + btoa(usr + ":" + pwd));
                    xhr.setRequestHeader("commandid", "commission_sp");
                    xhr.setRequestHeader("requestid", "commission_sp_" + Date.now());
                    xhr.setRequestHeader("requestdt", datetime);
                    xhr.setRequestHeader("clientid", insurance_username);
                    xhr.setRequestHeader("signature", signature);
                  },
                  success: function (data, textStatus, xhr) {
                      if(data.body.issuccess){
                          table_get_commission();
                          $.each(data.body.message, function( index, value ) {
                              ret_message += "<br>" + index + ": " + value;
                          });
                          toastr.success("Success Process!", 'Success!');
                      }else{
                          var ret_message = "";
                          $.each(data.body.message, function( index, value ) {
                              ret_message += "<br>" + index + ": " + value;
                          });
                          toastr.error(ret_message, 'Error!');
                      }
                  },
                  error: function (xhr, textStatus, errorThrown) {

                  }
              });
          });
      }

      function commission_filter(){
          $('.commission-btn-filter-exe').unbind('click');
          $('.commission-btn-filter-exe').click(function(e){
              e.preventDefault();
              var data = $('.commission_form_filter').serialize();
              table_get_commission(data);
              $('#commissionmodalfilter').modal('toggle');
          });
      }
</script>
