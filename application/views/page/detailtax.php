<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="tab-content tabcontent-border p-20" id="myTabContent">
                    <div role="tabpanel" class="tab-pane fade show active" id="home5" aria-labelledby="home-tab">
                        <div class="row">
                          <div class="col-5 align-self-center">
                              <div class="d-flex align-items-center">
                                  <nav aria-label="breadcrumb">
                                      <ol class="breadcrumb">
                                          <li class="breadcrumb-item">Tax Detail</li>
                                      </ol>
                                  </nav>
                              </div>
                          </div>
                        </div>
                        <hr />
                        <!-- Row -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table style="margin-top:7px;" class="table table-bordered nowrap display table-striped">
                                        <thead>
                                            <tr>
                                                <th><strong>K-Link Member ID</strong></th>
                                                <th><strong>Gross Income</strong></th>
                                                <th><strong>Tax</strong></th>
                                                <th><strong>Tax Percentage</strong></th>
                                                <th><strong>fti_status</strong></th>
                                                <th><strong>fti_error_id</strong></th>
                                                <th><strong>fti_error_message</strong></th>
                                            </tr>
                                        </thead>
                                        <tbody class="detailtax-api-list">

                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <div class="col-md-12" style="margin-top:7px;">
                                <form class="paging_form_detailtax" method="GET">
                                    <div class="ikut-table-detailtax row" style="display: none;">
                                        <div class="col-md-3">
                                            <div class="total-data-get-detailtax form-control"></div>
                                        </div>

                                        <div class="col-md-9">
                                            <ul class="pagination float-right">
                                                <li>
                                                    <select class="form-control change-num-records-detailtax" name="item_per_page">
                                                        <option value="10">10</option>
                                                        <option value="20">20</option>
                                                        <option value="50">50</option>
                                                        <option value="100">100</option>
                                                    </select>
                                                </li>
                                                <li style="margin-right: 15px;">
                                                    <div style="padding: .375rem .75rem;">
                                                        Records
                                                    </div>
                                                </li>
                                                <li class="footable-page-arrow page-item first-page-detailtax">
                                                    <a data-page="first" class="page-link link-first-page-detailtax">«</a>
                                                </li>
                                                <li class="footable-page-arrow page-item prev-page-detailtax" style="padding-right: .75rem;">
                                                    <a data-page="prev" class="page-link link-prev-page-detailtax">‹</a>
                                                </li>
                                                <li>
                                                    <input type="text" class="form-control this-page-get-detailtax" name="page" style="width: 40px;">
                                                </li>
                                                <li>
                                                    <div class="total-page-get-detailtax" style="padding: .375rem .75rem;"></div>
                                                </li>
                                                <li>
                                                    <btn type="button" class="btn btn-success go-page-detailtax">Go</btn>
                                                </li>
                                                <li class="footable-page-arrow page-item next-page-detailtax" style="padding-left: .75rem;">
                                                    <a data-page="next" class="page-link link-next-page-detailtax">›</a>
                                                </li>
                                                <li class="footable-page-arrow page-item last-page-detailtax">
                                                    <a data-page="last" class="page-link link-last-page-detailtax">»</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                        <!--Filter Modal -->
                        <div class="modal fade" id="detailtaxmodalfilter" tabindex="-1" role="dialog" aria-labelledby="filterModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <form class="detailtax1_form_filter" method="POST">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="filterModalLabel">
                                              <i class="fas fa-insurancefile-plus m-r-10"></i> Filter
                                            </h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" style="min-width: 125px;">Date Range</span>
                                                </div>
                                                <input type='text' class="form-control datetime-filter-insurancefile" name="date" value=""/>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <div class="col-md-6">
                                                <button class="btn btn-warning insurancefile-btn-reset-filter">
                                                    <i class="ti-loop"></i> Reset
                                                </button>
                                            </div>

                                            <div class="col-md-6" align="right">
                                                <button type="submit" class="btn btn-success insurancefile-btn-filter-exe">
                                                    <i class="ti-save"></i> Submit
                                                </button>
                                            </div>

                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
      var this_page_detailtax = 1;
      var total_page_detailtax = 1;
      var id_db_detailtax = '';

      function detailtax_ready(id) {
          var data;
          id_db_detailtax = id;
          table_get_detailtax(data,id_db_detailtax);
          paging("detailtax");
      }

      function table_get_detailtax(data,id) {
          if(id == undefined){
              id = id_db_detailtax;
          }

          $('.detailtax-api-list').html('');
          $.ajax({
              url: url_api+'tax/detailall/'+id,
              type: 'GET',
              data: data,
              dataType: 'json',
              beforeSend: function(xhr) {
                xhr.setRequestHeader("Authorization", "Basic " + btoa(usr + ":" + pwd));
                xhr.setRequestHeader("commandid", "detailtax");
                xhr.setRequestHeader("requestid", "detailtax" + Date.now());
                xhr.setRequestHeader("requestdt", datetime);
                xhr.setRequestHeader("clientid", insurance_username);
                xhr.setRequestHeader("signature", signature);
              },
              success: function (res, textStatus, xhr) {
                  $('.detailtax-api-list').html('');
                  res.body.data.map(function(value) {

                      $('.detailtax-api-list').append(`<tr>
                                                      <td>${value.k_link_member_id}</td>
                                                      <td align="right">${Number(value.gross_income).toLocaleString('id')}</td>
                                                      <td align="right">${Number(value.tax).toLocaleString('id')}</td>
                                                      <td align="right">${Number(value.tax_percentage).toLocaleString('id')}</td>
                                                      <td>${value.fti_status}</td>
                                                      <td>${value.fti_error_id}</td>
                                                      <td>${value.fti_error_message}</td>
                                                  </tr>`)
                  });
                  total_page_detailtax = res.body.pagination.total_page;
                  paging_ajax("detailtax", res);
              },
              error: function (xhr, textStatus, errorThrown) {

              }
          });
      }
</script>
