<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="tab-content tabcontent-border p-20" id="myTabContent">
                    <div role="tabpanel" class="tab-pane fade show active" id="home5" aria-labelledby="home-tab">
                        <div class="row">
                          <div class="col-5 align-self-center">
                              <div class="d-flex align-items-center">
                                  <nav aria-label="breadcrumb">
                                      <ol class="breadcrumb">
                                          <li class="breadcrumb-item">Reconciliation History</li>
                                      </ol>
                                  </nav>
                              </div>
                          </div>
                        </div>
                        <hr />
                        <!-- Row -->
                        <div class="row">
                            <div class="col-md-12" align="right">
                                <button type="button" class="btn btn-warning acm-btn-filter" data-toggle="modal" data-target="#acmmodalfilter">
                                    Filter
                                </button>
                                <button type="button" class="btn btn-info acm-btn-refresh" onclick="refresh_data('acm')">
                                    Refresh
                                </button>
                            </div>

                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table style="margin-top:7px;" id="table-acm" class="table table-bordered nowrap display table-striped">
                                        <thead>
                                            <tr>
                                                <th><strong>API</strong></th>
                                                <th><strong>Request ID</strong></th>
                                                <th><strong>Request Date</strong></th>
                                                <th><strong>Caller</strong></th>
                                                <th><strong>Request URL</strong></th>
                                                <th><strong>Signature</strong></th>
                                                <th><strong>Filesend</strong></th>
                                                <th><strong>Method</strong></th>
                                            </tr>
                                        </thead>
                                        <tbody class="acm-api-list">

                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <div class="col-md-12" style="margin-top:7px;">
                                <form class="paging_form_acm" method="GET">
                                    <div class="ikut-table-acm row" style="display: none;">
                                        <div class="col-md-3">
                                            <div class="total-data-get-acm form-control"></div>
                                        </div>

                                        <div class="col-md-9">
                                            <ul class="pagination float-right">
                                                <li>
                                                    <select class="form-control change-num-records-acm" name="item_per_page">
                                                        <option value="10">10</option>
                                                        <option value="1">1</option>
                                                        <option value="20">20</option>
                                                        <option value="50">50</option>
                                                        <option value="100">100</option>
                                                    </select>
                                                </li>
                                                <li style="margin-right: 15px;">
                                                    <div style="padding: .375rem .75rem;">
                                                        Records
                                                    </div>
                                                </li>
                                                <li class="footable-page-arrow page-item first-page-acm">
                                                    <a data-page="first" class="page-link link-first-page-acm">«</a>
                                                </li>
                                                <li class="footable-page-arrow page-item prev-page-acm" style="padding-right: .75rem;">
                                                    <a data-page="prev" class="page-link link-prev-page-acm">‹</a>
                                                </li>
                                                <li>
                                                    <input type="text" class="form-control this-page-get-acm" name="page" style="width: 40px;">
                                                </li>
                                                <li>
                                                    <div class="total-page-get-acm" style="padding: .375rem .75rem;"></div>
                                                </li>
                                                <li>
                                                    <btn type="button" class="btn btn-success go-page-acm">Go</btn>
                                                </li>
                                                <li class="footable-page-arrow page-item next-page-acm" style="padding-left: .75rem;">
                                                    <a data-page="next" class="page-link link-next-page-acm">›</a>
                                                </li>
                                                <li class="footable-page-arrow page-item last-page-acm">
                                                    <a data-page="last" class="page-link link-last-page-acm">»</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                        <!--Filter Modal -->
                        <div class="modal fade" id="acmmodalfilter" tabindex="-1" role="dialog" aria-labelledby="filterModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <form class="acm_form_filter" method="POST">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="filterModalLabel">
                                              <i class="fas fa-insurancefile-plus m-r-10"></i> Filter
                                            </h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" style="min-width: 125px;">Date</span>
                                                </div>
                                                <input type='text' class="form-control datetime-filter-acm" name="date" value=""/>
                                            </div>

                                        </div>
                                        <div class="modal-footer">
                                            <div class="col-md-6">
                                                <button class="btn btn-warning acm-btn-reset-filter">
                                                    <i class="ti-loop"></i> Reset
                                                </button>
                                            </div>

                                            <div class="col-md-6" align="right">
                                                <button type="submit" class="btn btn-success acm-btn-filter-exe">
                                                    <i class="ti-save"></i> Submit
                                                </button>
                                            </div>

                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    var this_page_acm = 1;
    var total_page_acm = 1;

    function acm_ready() {
        table_get_acm();
        acm_filter();
        paging("acm");

        $('.datetime-filter-acm').daterangepicker({
            timePicker: false,
            timePickerIncrement: 30,
            timePicker24Hour: true,
            autoUpdateInput: true,
            locale: {
                format: 'DD-MM-YYYY'
            }
        });

        $('.datetime-filter-acm').val('');
    }

    function table_get_acm(data) {
        $('.acm-api-list').html('')
        $.ajax({
            url: url_api+'acm',
            type: 'GET',
	        data: data,
            dataType: 'json',
            beforeSend: function(xhr) {
              xhr.setRequestHeader("Authorization", "Basic " + btoa(usr + ":" + pwd));
              xhr.setRequestHeader("commandid", "acm");
              xhr.setRequestHeader("requestid", "acm" + Date.now());
              xhr.setRequestHeader("requestdt", datetime);
              xhr.setRequestHeader("clientid", "acm");
              xhr.setRequestHeader("signature", signature);
            },
            success: function (res, textStatus, xhr) {
                res.body.data.map(function(value) {
                    $('.acm-api-list').append(`<tr>
                                                <td>${value.command_id}</td>
                                                <td>${value.request_id}</td>
                                                <td>${value.request_dt}</td>
                                                <td>${value.client_id}</td>
                                                <td>${value.request_url}</td>
                                                <td>${value.signature}</td>
                                                <td>${value.filesend}</td>
                                                <td>${value.method}</td>
                                                </tr>`)
                });
                total_page_acm = res.body.pagination.total_page;
                paging_ajax("acm", res);

            },
            error: function (xhr, textStatus, errorThrown) {

            }
        });
    }

    function acm_filter(){
        $('.acm-btn-filter-exe').unbind('click');
        $('.acm-btn-filter-exe').click(function(e){
            e.preventDefault();
            var data = $('.acm_form_filter').serialize();
            table_get_acm(data);
            $('#acmmodalfilter').modal('toggle');
        });
    }
</script>
