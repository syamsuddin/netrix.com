<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="tab-content tabcontent-border p-20" id="myTabContent">
                    <div role="tabpanel" class="tab-pane fade show active" id="home5" aria-labelledby="home-tab">
                        <div class="row">
                          <div class="col-5 align-self-center">
                              <div class="d-flex align-items-center">
                                  <nav aria-label="breadcrumb">
                                      <ol class="breadcrumb">
                                          <li class="breadcrumb-item">Insurance File Result</li>
                                      </ol>
                                  </nav>
                              </div>
                          </div>
                        </div>
                        <hr />
                        <!-- Row -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table style="margin-top:7px;" id="file_export" class="table table-bordered nowrap display table-striped">
                                        <thead>
                                            <tr>
                                                <th><strong>policy_number</strong></th>
                                                <th><strong>policy_holder_name</strong></th>
                                                <th><strong>product_code</strong></th>
                                                <th><strong>due_date</strong></th>
                                                <th><strong>frequency</strong></th>
                                                <th><strong>premium</strong></th>
                                                <th><strong>bonus_personal</strong></th>
                                                <th><strong>percentage</strong></th>
                                                <th><strong>currency</strong></th>
                                                <th><strong>k_link_member_id</strong></th>
                                                <th><strong>agent_code</strong></th>
                                                <th><strong>agent_name/strong></th>
                                                <th><strong>commission_period</strong></th>
                                                <th><strong>issued_date</strong></th>
                                                <th><strong>product_name</strong></th>
                                                <th><strong>assured_name</strong></th>
                                                <th><strong>premium_period_years</strong></th>
                                                <th><strong>premium_period_month</strong></th>
                                                <th><strong>gross_commission</strong></th>
                                                <th><strong>gross_commission_percentage</strong></th>
                                                <th><strong>fci_status</strong></th>
                                                <th><strong>fci_error_id</strong></th>
                                                <th><strong>fci_error_message</strong></th>
                                                <th><strong>sub_br</strong></th>
                                                <th><strong>sub_br_percentage</strong></th>
                                                <th><strong>sub_bn</strong></th>
                                                <th><strong>sub_bn_percentage</strong></th>
                                                <th><strong>commission</strong></th>
                                                <th><strong>commission_percentage</strong></th>
                                                <th><strong>extraction_status</strong></th>
                                                <th><strong>extraction_period</strong></th>
                                            </tr>
                                        </thead>
                                        <tbody class="detailfile-api-list">

                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <div class="col-md-12" style="margin-top:7px;">
                                <form class="paging_form_detailfile" method="GET">
                                    <div class="ikut-table-detailfile row" style="display: none;">
                                        <div class="col-md-3">
                                            <div class="total-data-get-detailfile form-control"></div>
                                        </div>

                                        <div class="col-md-9">
                                            <ul class="pagination float-right">
                                                <li>
                                                    <select class="form-control change-num-records-detailfile" name="item_per_page">
                                                        <option value="10">10</option>
                                                        <option value="20">20</option>
                                                        <option value="50">50</option>
                                                        <option value="100">100</option>
                                                    </select>
                                                </li>
                                                <li style="margin-right: 15px;">
                                                    <div style="padding: .375rem .75rem;">
                                                        Records
                                                    </div>
                                                </li>
                                                <li class="footable-page-arrow page-item first-page-detailfile">
                                                    <a data-page="first" class="page-link link-first-page-detailfile">«</a>
                                                </li>
                                                <li class="footable-page-arrow page-item prev-page-detailfile" style="padding-right: .75rem;">
                                                    <a data-page="prev" class="page-link link-prev-page-detailfile">‹</a>
                                                </li>
                                                <li>
                                                    <input type="text" class="form-control this-page-get-detailfile" name="page" style="width: 40px;">
                                                </li>
                                                <li>
                                                    <div class="total-page-get-detailfile" style="padding: .375rem .75rem;"></div>
                                                </li>
                                                <li>
                                                    <btn type="button" class="btn btn-success go-page-detailfile">Go</btn>
                                                </li>
                                                <li class="footable-page-arrow page-item next-page-detailfile" style="padding-left: .75rem;">
                                                    <a data-page="next" class="page-link link-next-page-detailfile">›</a>
                                                </li>
                                                <li class="footable-page-arrow page-item last-page-detailfile">
                                                    <a data-page="last" class="page-link link-last-page-detailfile">»</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                        <!--Filter Modal -->
                        <div class="modal fade" id="detailfilemodalfilter" tabindex="-1" role="dialog" aria-labelledby="filterModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <form class="detailfile_form_filter" method="POST">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="filterModalLabel">
                                              <i class="fas fa-insurancefile-plus m-r-10"></i> Filter
                                            </h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" style="min-width: 125px;">Date Range</span>
                                                </div>
                                                <input type='text' class="form-control datetime-filter-insurancefile" name="date" value=""/>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <div class="col-md-6">
                                                <button class="btn btn-warning insurancefile-btn-reset-filter">
                                                    <i class="ti-loop"></i> Reset
                                                </button>
                                            </div>

                                            <div class="col-md-6" align="right">
                                                <button type="submit" class="btn btn-success insurancefile-btn-filter-exe">
                                                    <i class="ti-save"></i> Submit
                                                </button>
                                            </div>

                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
      var this_page_detailfile = 1;
      var total_page_detailfile = 1;
      var id_db_detailcom;
      var type_detailcom;

      function detailfile_ready(id,type_result) {
          var data;
          id_db_detailcom = id;
          type_detailcom = type_result;
          table_get_detailfile(data,id,type_result);
          paging("detailfile");
      }

      function table_get_detailfile(data,id,type_result) {
          if(id == undefined){
              id = id_db_detailcom;
          }

          if(type_result == undefined){
              type_result = type_detailcom;
          }

          $('.detailfile-api-list').html('')
          $.ajax({
              url: url_api+'insurancefileresult/'+id+'/'+type_result,
              type: 'GET',
              data: data,
              dataType: 'json',
              beforeSend: function(xhr) {
                xhr.setRequestHeader("Authorization", "Basic " + btoa(usr + ":" + pwd));
                xhr.setRequestHeader("commandid", "detailfile");
                xhr.setRequestHeader("requestid", "detailfile" + Date.now());
                xhr.setRequestHeader("requestdt", datetime);
                xhr.setRequestHeader("clientid", insurance_username);
                xhr.setRequestHeader("signature", signature);
              },
              success: function (res, textStatus, xhr) {
                  res.body.data.map(function(value) {
                      if(value.status == 1){
                          value.status = "Initail";
                      }else if(value.status == 2){
                          value.status = "Completed";
                      }else if(value.status == 3){
                          value.status = "Failed";
                      }else if(value.status == 4){
                          value.status = "Duplicated";
                      }


                      $('.detailfile-api-list').append(`<tr>
                                                  <td>${value.policy_number}</td>
                                                  <td>${value.policy_holder_name}</td>
                                                  <td>${value.product_code}</td>
                                                  <td>${value.due_date}</td>
                                                  <td>${value.frequency}</td>
                                                  <td align="right">${Number(value.premium).toLocaleString('id')}</td>
                                                  <td align="right">${Number(value.bonus_personal).toLocaleString('id')}</td>
                                                  <td align="right">${Number(value.bonus_personal_percentage).toLocaleString('id')}</td>
                                                  <td>${value.currency}</td>
                                                  <td>${value.k_link_member_id}</td>
                                                  <td>${value.agent_code}</td>
                                                  <td>${value.agent_name}</td>
                                                  <td>${value.commission_period}</td>
                                                  <td>${value.issued_date}</td>
                                                  <td>${value.product_name}</td>
                                                  <td>${value.assured_name}</td>
                                                  <td>${value.premium_period_years}</td>
                                                  <td>${value.premium_period_month}</td>
                                                  <td align="right">${Number(value.gross_commission).toLocaleString('id')}</td>
                                                  <td align="right">${Number(value.gross_commission_percentage).toLocaleString('id')}</td>
                                                  <td>${value.fci_status}</td>
                                                  <td>${value.fci_error_id}</td>
                                                  <td>${value.fci_error_message}</td>
                                                  <td>${value.sub_br}</td>
                                                  <td>${value.sub_br_percentage}</td>
                                                  <td>${value.sub_bn}</td>
                                                  <td>${value.sub_bn_percentage}</td>
                                                  <td>${value.commission}</td>
                                                  <td>${value.commission_percentage}</td>
                                                  <td>${value.extraction_status}</td>
                                                  <td>${value.extraction_period}</td>
                                                  </tr>`)
                  });
                  total_page_detailfile = res.body.pagination.total_page;
                  paging_ajax("detailfile", res);
                  load_side_menu();
              },
              error: function (xhr, textStatus, errorThrown) {

              }
          });
      }
</script>
