<?php
$host = 'SYAMSUDDIN';
$username = 'dgsi'; // Username
$password = 'dgsiadmin'; // Password (Isi jika menggunakan password)
$database = 'budget'; // Nama databasenya

$connectionInfo = array( "UID"=>$username,"PWD"=>$password,"Database"=>$database);
$conn = sqlsrv_connect( $host, $connectionInfo);

if($conn){
     echo "Connection established.<br />";
     echo "\n";
}else{
     echo "Connection could not be established.<br />";
     echo "\n";
     die( print_r( sqlsrv_errors(), true));
}

//Example is triple_des and cbc with self key and iv for storing in base64
// $key = "e4hd9h4dhs23dyfhhemks3nf";// 24 bit Key
// $iv = "fYfhHeDm";// 8 bit IV
// $bit_check=8;// bit amount for diff algor.

function encrypt($text,$key,$iv,$bit_check) {
	$text_num =str_split($text,$bit_check);
	$text_num = $bit_check-strlen($text_num[count($text_num)-1]);
	for ($i=0;$i<$text_num; $i++) {$text = $text . chr($text_num);}
	// $cipher = mcrypt_module_open(MCRYPT_TRIPLEDES,'','cbc','');
	// mcrypt_generic_init($cipher, $key, $iv);
	// $decrypted = mcrypt_generic($cipher,$text);
	// mcrypt_generic_deinit($cipher);
	// return base64_encode($decrypted);
	$hasil = openssl_encrypt($text,'des-ede3-cbc',$key,OPENSSL_RAW_DATA | OPENSSL_NO_PADDING, $iv);
	return base64_encode($hasil);
}

function decrypt($encrypted_text,$key,$iv,$bit_check){
	// $cipher = mcrypt_module_open(MCRYPT_TRIPLEDES,'','cbc','');
	// mcrypt_generic_init($cipher, $key, $iv);
	// $decrypted = mdecrypt_generic($cipher,base64_decode($encrypted_text));
	// mcrypt_generic_deinit($cipher);
	$decrypted = openssl_decrypt(base64_decode($encrypted_text),'des-ede3-cbc',$key,OPENSSL_RAW_DATA | OPENSSL_NO_PADDING, $iv);
	$last_char=substr($decrypted,-1);
	for($i=0;$i<$bit_check-1; $i++){
		if(chr($i)==$last_char){

			$decrypted=substr($decrypted,0,strlen($decrypted)-$i);
			break;
		}
	}
	return $decrypted;
}
