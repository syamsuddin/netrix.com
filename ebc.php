<?php
include("config.php");

getcwd();
chdir("../");
chdir("netrix-api.com/uploads/tax/");
$dir = getcwd();
chdir("../");
chdir("tax_success/");
$success = getcwd();

foreach (glob($dir.'/*') as $file) {
    $file_name = basename($file);

    $sql = "SELECT TOP 1 t_file_tax.*, t_caller.caller_key_secret, t_caller.caller_key_iv, t_caller.caller_key_bit FROM [t_file_tax] LEFT JOIN t_caller ON t_file_tax.insurance_id = t_caller.caller_id WHERE ft_file = '$file_name' ORDER BY ft_id DESC";
    $stmt = sqlsrv_query($conn, $sql);
    if ($stmt === false) {
        echo "satu";
        die(print_r(sqlsrv_errors(), true));
    }

    $id_db;
    $file_db;
    while ($row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC)) {
        $id_db = $row['ft_id'];
        $file_db = $row['ft_file'];
        $insurance = $row['insurance_id'];
        $key = $row['caller_key_secret'];
        $iv = $row['caller_key_iv'];
        $bit_check = $row['caller_key_bit'];
    }

    sqlsrv_free_stmt($stmt);

    $sql_process = "UPDATE [t_file_tax] SET ft_process = 5 WHERE ft_id = " . $id_db;
    $stmt_process = sqlsrv_prepare($conn, $sql_process);

    if (sqlsrv_execute($stmt_process) === false) {
        echo "proses";
        die(print_r(sqlsrv_errors(), true));
    }

    $data_success = array();
    $msg_error = array();
    if (($handle = fopen($file, "r")) !== false) {
        $num = 1;
        $error = 0;
        while (($data_csv = fgetcsv($handle)) !== false) {
			$error_line = 0;
            $data_decrypt = decrypt($data_csv[0], $key, $iv, $bit_check);
            $data_decrypt = explode(";", $data_decrypt);

            // $sql_cal_sum = "SELECT TOP 1 * FROM [t_cal_summary] WHERE gen_id = '$data_decrypt[0]' AND k_link_member_id = '$data_decrypt[1]' AND total_gross_commission = '$data_decrypt[2]'";
            $sql_cal_sum = "SELECT *
							FROM
								(
									select gen_id,k_link_member_id,(
									isnull(total_bonus_personal,0)+
									isnull(total_br,0)+
									isnull(total_bn_1,0)+
									isnull(total_bn_2,0)+
									isnull(total_bn_3,0)+
									isnull(total_bn_4,0)+
									isnull(total_bn_5,0)+
									isnull(total_bn_6,0)+
									isnull(total_bn_7,0)+
									isnull(total_bn_8,0)+
									isnull(total_bn_9,0) )as total_income,(
									isnull(total_bonus_personal,0)+
									isnull(total_br,0)+
									isnull(total_bn_1,0)+
									isnull(total_bn_2,0)+
									isnull(total_bn_3,0)+
									isnull(total_bn_4,0)+
									isnull(total_bn_5,0)+
									isnull(total_bn_6,0)+
									isnull(total_bn_7,0)+
									isnull(total_bn_8,0)+
									isnull(total_bn_9,0) )*10 as total_income_10
									from t_cal_summary
								) s
							where gen_id = '$data_decrypt[0]'
							AND k_link_member_id = '$data_decrypt[1]'
							AND total_income = '$data_decrypt[2]'";
            $stmt_cal_sum = sqlsrv_query($conn, $sql_cal_sum);
            if ($stmt_cal_sum === false) {
                echo "search cal summary";
                die(print_r(sqlsrv_errors(), true));
            }

            $data = array();
            while ($row = sqlsrv_fetch_array($stmt_cal_sum, SQLSRV_FETCH_ASSOC)) {
                $data = $row;
            }

            if (empty($data)) {
                $error = 1;
				$error_line = 1;
                $table = 't_file_tax_exception';
				$data_decrypt['error'] = "Not found in t_cal_summary";
				// print_r($num . " tak ada" . PHP_EOL);
            } else {
                $table = 't_file_tax_extraction';
				$data_decrypt['error'] = "";
				// print_r($num . " ada" . PHP_EOL);
            }

            $sql_insert = "INSERT INTO $table (ft_id,
            gen_id,
            k_link_member_id,
            gross_income,
            tax,
            tax_percentage,
			fti_error_message) VALUES (?,?,?, ?,?,?, ?)";

            $params = array($id_db //0
            , $data_decrypt[0] //1
            , $data_decrypt[1] //2
            , $data_decrypt[2] //3
            , $data_decrypt[3] //4
            , $data_decrypt[4] //5
            , $data_decrypt['error'] //5
            ); //4

            $stmt_insert = sqlsrv_query($conn, $sql_insert, $params);
            if ($stmt_insert === false) {
				$sql_process = "UPDATE [t_file_tax] SET ft_process = 16 WHERE ft_id = " . $id_db;
				$stmt_process = sqlsrv_prepare($conn, $sql_process);

				if (sqlsrv_execute($stmt_process) === false) {
					echo "proses";
					die(print_r(sqlsrv_errors(), true));
				}

				rename($file, $success . "\\" . $file_name);
                echo "insert file tax extraction";
                die(print_r(sqlsrv_errors(), true));
                sqlsrv_rollback($conn);
            }

            $num++;
        }

		// exit();

        if ($error == 1) {
            $process = 16;
        } else {
            $process = 14;
        }

        $sql_process = "UPDATE [t_file_tax] SET ft_process = '$process' WHERE ft_id = " . $id_db;
        $stmt_process = sqlsrv_prepare($conn, $sql_process);

        if (sqlsrv_execute($stmt_process) === false) {
            echo "proses";
            die(print_r(sqlsrv_errors(), true));
        }
    }
    fclose($handle);

	$requestid = explode(".", $file_name);
    $requestid = $requestid[0];

    list($requestid, $file_name_format) = explode('_', $requestid);

	$sql_log = "SELECT TOP 1 * FROM [api_logs] where request_id = '$requestid' ORDER BY id DESC";
	$stmt_log = sqlsrv_query($conn, $sql_log);
	if ($stmt_log === false) {
		echo "lima";
		die(print_r(sqlsrv_errors(), true));
	}

	while ($row = sqlsrv_fetch_array($stmt_log, SQLSRV_FETCH_ASSOC)) {
		$url_db = $row['request_url'];
		$command_id = $row['command_id'];
		$request_id = $row['request_id'];
	}

    $ch      = curl_init($url_db);
    $options = array(
        CURLOPT_RETURNTRANSFER      => true,
        CURLOPT_HEADER              => false,
        CURLOPT_FOLLOWLOCATION      => false,
        CURLOPT_AUTOREFERER         => true,
        CURLOPT_CONNECTTIMEOUT      => 20,
        CURLOPT_TIMEOUT             => 20,
        CURLOPT_POST                => 1,
        CURLOPT_POSTFIELDS          => array(
          'requestid' => $id_db,
          'isreceiced' => "true",
          'attachmentfile' => new CurlFile($file, 'text/csv', $file_name)
        ),
        CURLOPT_SSL_VERIFYHOST      => 0,
        CURLOPT_SSL_VERIFYPEER      => false,
        CURLOPT_VERBOSE             => 1,
        CURLOPT_HTTPHEADER          => array(
           'responseid' => "file" . DATE("YmdHis"),
           'responsedt' => DATE("YmdHis"),
        )

      );
    curl_setopt_array($ch, $options);
    $data       = curl_exec($ch);
    $curl_errno = curl_errno($ch);
    $curl_error = curl_error($ch);

    print_r($data);
    // print_r($curl_errno);
    // print_r($curl_error);
    curl_close($ch);
    sqlsrv_commit($conn);
    rename($file, $success . "\\" . $file_name);
}
