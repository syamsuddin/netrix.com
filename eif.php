<?php
include("config.php");

getcwd();
chdir("../");
chdir("netrix-api.com/uploads/temporary/");
$dir = getcwd();
chdir("../");
chdir("success/");
$success = getcwd();
chdir("../");
chdir("failed/");
$failed = getcwd();

$arr_column = array(
    "policy_number", //0
    "policy_holder_name", //1
    "product_code", //2
    "due_date", //3
    "frequency", //4
    "premium", //5
    "bonus_personal", //6
    "percentage", //7
    "currency", //8
    "k_link_member_id", //9
    "agent_code", //10
    "agent_name", //11
    "commission_period", //12
    "issued_date", //13
    "product_name", //14
    "assured_name", //15
    "premium_period_years", //16
    "premium_period_month", //17
    "gross_commission", //18
    "gross_commission_percentage", //19
);

$arr_format = array(
    array("numeric", 13), //0
    array("string", 256), //1
    array("string", 80), //2
    array("string", 8), //3
    array("numeric", 8), //4
    array("numeric", 18), //5
    array("numeric", 18), //6
    array("numeric", 3), //7
    array("string", 3), //8
    array("string", 50), //9
    array("string", 50), //10
    array("string", 256), //11
    array("numeric", 8), //12
    array("string", 8), //13
    array("string", 80), //14
    array("string", 256), //15
    array("numeric", 3), //16
    array("numeric", 3), //17
    array("numeric", 18), //18
    array("numeric", 3) //19
);

function syams_func($type, $val, $length)
{
    if ($type == "numeric") {
        $val = (int)$val;
        if (is_numeric($val) && (strlen($val) <= $length)) {
            return true;
        } else {
            return false;
        }
    } elseif ($type == "string") {
        if (is_string($val) && (strlen($val) <= $length)) {
            return true;
        } else {
            return false;
        }
    } elseif ($type == "date") {
        if (DateTime::createFromFormat('Y-m-d', $val) !== false) {
            return true;
        } else {
            return false;
        }
    }
}

foreach (glob($dir.'/*.*') as $file) {
    $file_name = basename($file);

    $sql = "SELECT TOP 1 t_file_commission.*, t_caller.caller_key_secret, t_caller.caller_key_iv, t_caller.caller_key_bit FROM [t_file_commission] LEFT JOIN t_caller ON t_file_commission.insurance_id = t_caller.caller_id WHERE fc_file = '$file_name' ORDER BY fc_id DESC";
    $stmt = sqlsrv_query($conn, $sql);
    if ($stmt === false) {
        echo "satu";
        die(print_r(sqlsrv_errors(), true));
    }

    $id_db;
    $file_db;
    while ($row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC)) {
        $id_db = $row['fc_id'];
        $file_db = $row['fc_file'];
        $insurance = $row['insurance_id'];
        $key = $row['caller_key_secret'];
        $iv = $row['caller_key_iv'];
        $bit_check = $row['caller_key_bit'];
    }
    sqlsrv_free_stmt($stmt);

    $sql_process = "UPDATE [t_file_commission] SET fc_process = 5 WHERE fc_id = " . $id_db;
    $stmt_process = sqlsrv_prepare($conn, $sql_process);

    if (sqlsrv_execute($stmt_process) === false) {
        echo "proses";
        die(print_r(sqlsrv_errors(), true));
    }

    $data_success = array();
    $msg_error = array();
    if (($handle = fopen($file, "r")) !== false) {
        $row = 1;
        $error = 0;
        $error_all = 0;
        while (($data_csv = fgetcsv($handle)) !== false) {
			$error = 0;
            $data_decrypt = decrypt($data_csv[0], $key, $iv, $bit_check);
			$data_decrypt = preg_replace('/[\x00-\x1F\x7F]/', '', $data_decrypt);
			if(preg_match('/[^\x20-\x7e]/', $data_decrypt)){
				$sql_success = "UPDATE [t_file_commission] SET fc_process = 4 WHERE fc_id = " . $id_db;
				$stmt_success = sqlsrv_prepare($conn, $sql_success);

				if (sqlsrv_execute($stmt_success) === false) {
					echo "tiga";
					die(print_r(sqlsrv_errors(), true));
				}

				sqlsrv_commit($conn);
				rename($file, $success . "\\" . $file_name);

				return $stmt_success;
			}
            $data_decrypt = explode(";", $data_decrypt);
            $num = 0;
            $msg = "";
            foreach ($data_decrypt as $val) {
                if (!syams_func($arr_format[$num][0], $val, $arr_format[$num][1])) {
                    $error = 1;
                    $error_all = 1;
                    $msg .= $arr_column[$num] . " must be " . $arr_format[$num][0] . " max length " . $arr_format[$num][1] . ", ";
                }
                $num++;
            }

            if ($error == 0) {
                $data_decrypt['status'] = 0;
            } else {
                $data_decrypt['status'] = 1;
            }

            $data_decrypt[21] = $msg;

            array_push($data_success, $data_decrypt);
            $row++;
        }
    }
    fclose($handle);
    $requestid = explode(".", $file_name);
    $requestid = $requestid[0];

    list($requestid, $file_name_format) = explode('_', $requestid);

    /* Begin the transaction. */
    if (sqlsrv_begin_transaction($conn) === false) {
        echo "dua";
        die(print_r(sqlsrv_errors(), true));
    }

    if ($error_all == 0) {
        $task_status = 3;
    } else {
        $task_status = 4;
    }

    $sql_success = "UPDATE [t_file_commission] SET fc_process = ".$task_status." WHERE fc_id = " . $id_db;
    $stmt_success = sqlsrv_prepare($conn, $sql_success);

    if (sqlsrv_execute($stmt_success) === false) {
        echo "tiga";
        die(print_r(sqlsrv_errors(), true));
    }

    foreach ($data_success as $v) {
        if ($v['status'] == 0) {
            $table = 't_file_commission_extraction';
        } else {
            $table = 't_file_commission_exception';
        }

        $sql_insert = "INSERT INTO $table (fc_id,
        policy_number,
    	policy_holder_name,
    	product_code,
    	due_date,
    	frequency,
    	premium,
    	bonus_personal,
    	bonus_personal_percentage,
    	currency,
    	k_link_member_id,
    	agent_code,
    	agent_name,
    	commission_period,
    	issued_date,
    	product_name,
    	assured_name,
    	premium_period_years,
    	premium_period_month,
        gross_commission,
        gross_commission_percentage,
        fci_status,
        fci_error_message) VALUES (?,?,?,?,?,?,?,?,?,?, ?,?,?,?,?,?,?,?,?,? ,?,?,?)";

        $params = array($id_db //fc_id
        , $v[0] //policy_number
        , $v[1] //policy_holder_name
        , $v[2] //product_code
        , DATE("Y-m-d", strtotime($v[3])) //due_date
        , $v[4] //frequency
        , $v[5] //premium
        , $v[6] //bonus_personal
        , $v[7] //bonus_personal_percentage
        , $v[8] //currency
        , $v[9] //k_link_member_id
        , $v[10] //agent_code
        , $v[11] //agent_name
        , $v[12] //commission_period
        , DATE("Y-m-d",strtotime($v[13])) //issued_date
        , $v[14] //product_name
        , $v[15] //assured_name
        , $v[16] //premium_period_years
        , $v[17] //premium_period_month
        , $v[18] //gross_commission
        , $v[19] //gross_commission_percentage
        , 'NULL' //fci_status
        , $v[21]); //fci_error_message

        $stmt_insert = sqlsrv_query($conn, $sql_insert, $params);
        if ($stmt_insert === false) {
            echo "empat";
            sqlsrv_rollback($conn);

			$sql_success = "UPDATE [t_file_commission] SET fc_process = 4 WHERE fc_id = " . $id_db;
			$stmt_success = sqlsrv_prepare($conn, $sql_success);

			if (sqlsrv_execute($stmt_success) === false) {
				echo "tiga";
				die(print_r(sqlsrv_errors(), true));
			}

			sqlsrv_commit($conn);
			rename($file, $success . "\\" . $file_name);

			return print_r(sqlsrv_errors(), true);
        }
    }
    sqlsrv_commit($conn);

    if ($task_status == 3) {
        if (sqlsrv_begin_transaction($conn) === false) {
            echo "trans sp";
            die(print_r(sqlsrv_errors(), true));
        }
        $qr_sp = "DECLARE @EXEC_RESULT nvarchar(max) EXEC callBonusExtraction @fc_id ={$id_db}, @EXEC_RESULT=@EXEC_RESULT OUTPUT SELECT @EXEC_RESULT";
        $stmt_sp = sqlsrv_query($conn, $qr_sp);
        $data_sp = sqlsrv_errors();

        $pesan = '';
        while ($dt_call_sp = sqlsrv_fetch_array($stmt_sp)) {
            $pesan = $dt_call_sp[0];
        }

        list($code, $message) = explode(':', $pesan);

        if ($code=='00') {
            $sql_log = "SELECT TOP 1 * FROM [api_logs] where request_id = '$requestid' ORDER BY id DESC";
            $stmt_log = sqlsrv_query($conn, $sql_log);
            if ($stmt_log === false) {
                echo "lima";
                die(print_r(sqlsrv_errors(), true));
            }

            while ($row = sqlsrv_fetch_array($stmt_log, SQLSRV_FETCH_ASSOC)) {
                $url_db = $row['request_url'];
                $command_id = $row['command_id'];
                $request_id = $row['request_id'];
            }

            if ($file_name == $file_db) {
                $ch      = curl_init($url_db);
                $options = array(
                    CURLOPT_RETURNTRANSFER      => true,
                    CURLOPT_HEADER              => false,
                    CURLOPT_FOLLOWLOCATION      => false,
                    CURLOPT_AUTOREFERER         => true,
                    CURLOPT_CONNECTTIMEOUT      => 20,
                    CURLOPT_TIMEOUT             => 20,
                    CURLOPT_POST                => 1,
                    CURLOPT_POSTFIELDS          => array(
                      'requestid' => $request_id,
                      'isreceiced' => "true",
                      'attachmentfile' => new CurlFile($file, 'text/csv', $file_db)
                    ),
                    CURLOPT_SSL_VERIFYHOST      => 0,
                    CURLOPT_SSL_VERIFYPEER      => false,
                    CURLOPT_VERBOSE             => 1,
                    CURLOPT_HTTPHEADER          => array(
                       'responseid' => "file" . DATE("YmdHis"),
                       'responsedt' => DATE("YmdHis"),
                    )
                );

                curl_setopt_array($ch, $options);
                $data       = curl_exec($ch);
                $curl_errno = curl_errno($ch);
                $curl_error = curl_error($ch);

                print_r($data);
                // print_r($curl_errno);
                // print_r($curl_error);
                curl_close($ch);

                $sql_success_sp = "UPDATE [t_file_commission] SET fc_process = 7, fc_process_error_id = '".$code."', fc_process_error_message = '".$message."' WHERE fc_id = " . $id_db;
                $sql_success_sp = sqlsrv_prepare($conn, $sql_success_sp);

                if (sqlsrv_execute($sql_success_sp) === false) {
                    echo "failed_sp";
                    die(print_r(sqlsrv_errors(), true));
                }
            }
            sqlsrv_commit($conn);
        } else {
            sqlsrv_rollback($conn);
            $sql_failed_sp = "UPDATE [t_file_commission] SET fc_process = 6, fc_process_error_id = '".$code."', fc_process_error_message = '".$message."' WHERE fc_id = " . $id_db;
            $stmt_failed_sp = sqlsrv_prepare($conn, $sql_failed_sp);

            if (sqlsrv_execute($stmt_failed_sp) === false) {
                echo "failed_sp";
                die(print_r(sqlsrv_errors(), true));
            }
        }
    }else{
        $sql_success = "UPDATE [t_file_commission] SET fc_process = 4 WHERE fc_id = " . $id_db;
		$stmt_success = sqlsrv_prepare($conn, $sql_success);

		if (sqlsrv_execute($stmt_success) === false) {
			echo "tiga";
			die(print_r(sqlsrv_errors(), true));
		}

		sqlsrv_commit($conn);
    }

    rename($file, $success . "\\" . $file_name);
}
